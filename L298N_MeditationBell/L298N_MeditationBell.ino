/*  L298 powered Lego Technic tracked vehicle
 *  by Hoolio Wobbits 2019
 *  based on code from Arduino DC Motor Control - PWM | H-Bridge | L298N  -  Example 01
 *   by Dejan Nedelkovski, www.HowToMechatronics.com
*/

#define outputBenable 3 //orange
#define outputBin1 A3 //red 
#define outputBin2 A5 //brown

void setup() {
  // inputs from microbit (via phone)
  pinMode(outputBenable, OUTPUT);
  pinMode(outputBin1, OUTPUT);
  pinMode(outputBin2, OUTPUT);
  //
  Serial.begin(9600);
  Serial.println("ready..");
  //startupSound();
}
void loop() {
  const int duration = 75;
//  int raw = analogRead(potPin);
//  int duration = map(raw, 0, 1023, 24, 4000);

  const int pwmOutput = 255; // full power!
  //int raw = analogRead(potPin);
  //int pwmOutput = map(raw, 0, 1023, 235, 255);
  
  ding(pwmOutput, duration);

  delay(6000);
}

//forwards
void ding(int pwmOutput, int duration) {
  Serial.print(pwmOutput); Serial.print(","); Serial.println(duration); 
  analogWrite(outputBenable, pwmOutput); // Send PWM signal to L298N Enable pin

  // on
  digitalWrite(outputBin1, HIGH);
  digitalWrite(outputBin2, LOW);
  // delay
  delay(duration);
  // off
  digitalWrite(outputBin1, LOW);
  digitalWrite(outputBin2, LOW);
}
