/*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.
 
  This example code is in the public domain.
 */
 
// pin assignments
int annodes0 = 2;int annodes1 = 3;int annodes2 = 4;int annodes3 = 5;int annodes4 = 6;
//pin 7 is spare
int cathodes0 = 8;int cathodes1 = 9;int cathodes2 = 10;int cathodes3 = 11;int cathodes4 = 12;
int IndicatorLED = 13;
int InputPot = A0;
int C_SpeakerPin = 19; //ie analog5, in use as a digital pin

int mycursor=2;  // set initial position of the cursor

// the setup routine runs once when you press reset:
void setup() {                
  // initialize the pins
  pinMode(annodes0, OUTPUT); pinMode(annodes1, OUTPUT); pinMode(annodes2, OUTPUT); pinMode(annodes3, OUTPUT); pinMode(annodes4, OUTPUT);
  pinMode(cathodes0, OUTPUT); pinMode(cathodes1, OUTPUT); pinMode(cathodes2, OUTPUT); pinMode(cathodes3, OUTPUT); pinMode(cathodes4, OUTPUT);
  pinMode(IndicatorLED, OUTPUT); 
  pinMode(InputPot, INPUT);
 
  Serial.begin(9600);
}

void loop() {
  //space invaders!!! :)
  int numberofrounds = 5; 
  int roundlengthinticks = 16;
  int gameroundtickspeed[]= {300,200,150,120,80};
  
  // count the number of rounds
  for (int i = 0; i < numberofrounds; i++){
    // count the number of ticks in each round
    for (int a = 0; a < roundlengthinticks; a++){
      sound(1);
      mycursor=ControlShip(gameroundtickspeed[i]); //ControlShip(delay)
      delay(gameroundtickspeed[i]);
    }
    delay(1000); //delay between rounds
  }
}

void sound(int soundeffect) {
   switch( soundeffect ) {
     case 1: //grunt
       tone(C_SpeakerPin, 17, 200);
       break;
   }
   //delay(1000);
}

int ControlShip(int mydelay){
  int mypreviouscursor=mycursor;
  int instruction[] = {1, 6, 11, 16, 21}; // planet surface
  //int instruction[] = {1, 1, 1, 1}; // planet surface
  
  // note the last element in the array is arraylen -1 !!!!
  int arraylen = sizeof(instruction)/sizeof(int); int lastarrayelement = arraylen - 1;
  
  // determine the position of the pot and assign a velocity to it
  int velocityX= analogRead(InputPot); int velocity = map(velocityX, 0, 1023, -150, 150);
  
  // set the cursor to increment or decrement depending on the velocity
  // note the deadzone at which nothing happens
  if (velocity > 30) { mycursor++;} 
  if (velocity < -30) {mycursor--;}

  // wrap the cursor around the array
  if (mycursor < 0) { mycursor = lastarrayelement; } 
  if (mycursor > lastarrayelement) { mycursor = 0; }
  
  //Serial.print("mycursor = "); Serial.print(mycursor);  Serial.print(" mydelay = "); Serial.print(mydelay);  Serial.print(" velocity = "); Serial.print(velocity); 
       
  // blink the leds according to the instructions
  lightled(instruction[mycursor]);  delay(mydelay);
  
  // if the cursor has moved, then turn everything off and wait a sec 
  if (mypreviouscursor != mycursor){alloff();} 
    
  return mycursor;
}

void lightled(int LEDid) {
  Serial.print(" led = "); Serial.println(LEDid); //Serial.print("  delay = "); Serial.println(mydelay);
  switch (LEDid) {
    case 0:
      //all off
      digitalWrite(cathodes0, LOW); digitalWrite(annodes0, LOW);
      digitalWrite(cathodes1, LOW); digitalWrite(annodes1, LOW);
      digitalWrite(cathodes2, LOW); digitalWrite(annodes2, LOW);
      digitalWrite(cathodes3, LOW); digitalWrite(annodes3, LOW);
      digitalWrite(cathodes4, LOW); digitalWrite(annodes4, LOW); break;
    case 1: //c0a0
      digitalWrite(cathodes0, HIGH); digitalWrite(annodes0, HIGH); break;
    case 2: //c0a1
      digitalWrite(cathodes0, HIGH); digitalWrite(annodes1, HIGH); break;
    case 3: //c0a2
      digitalWrite(cathodes0, HIGH); digitalWrite(annodes2, HIGH); break;
    case 4: //c0a3
      digitalWrite(cathodes0, HIGH); digitalWrite(annodes3, HIGH); break;      
    case 5: //c0a4
      digitalWrite(cathodes0, HIGH); digitalWrite(annodes4, HIGH); break;
    case 6: //c1a0
      digitalWrite(cathodes1, HIGH); digitalWrite(annodes0, HIGH); break;
    case 7: //c1a1
      digitalWrite(cathodes1, HIGH); digitalWrite(annodes1, HIGH); break;
    case 8: //c1a2
      digitalWrite(cathodes1, HIGH); digitalWrite(annodes2, HIGH); break;
    case 9: //c1a3
      digitalWrite(cathodes1, HIGH); digitalWrite(annodes3, HIGH); break;
    case 10: //c1a4
      digitalWrite(cathodes1, HIGH); digitalWrite(annodes4, HIGH); break;
    case 11: //c2a0
      digitalWrite(cathodes2, HIGH); digitalWrite(annodes0, HIGH); break;
    case 12: //c2a1
      digitalWrite(cathodes2, HIGH); digitalWrite(annodes1, HIGH); break;
    case 13: //c2a2
      digitalWrite(cathodes2, HIGH); digitalWrite(annodes2, HIGH); break;
    case 14: //c2a3
      digitalWrite(cathodes2, HIGH); digitalWrite(annodes3, HIGH); break;
    case 15: //c2a4
      digitalWrite(cathodes2, HIGH); digitalWrite(annodes4, HIGH); break;
    case 16: //c3a0
      digitalWrite(cathodes3, HIGH); digitalWrite(annodes0, HIGH); break;
    case 17: //c3a1
      digitalWrite(cathodes3, HIGH); digitalWrite(annodes1, HIGH); break;
    case 18: //c3a2
      digitalWrite(cathodes3, HIGH); digitalWrite(annodes2, HIGH); break;
    case 19: //c3a3
      digitalWrite(cathodes3, HIGH); digitalWrite(annodes3, HIGH); break;
    case 20: //c3a4
      digitalWrite(cathodes3, HIGH); digitalWrite(annodes4, HIGH); break;
    case 21: //c4a0
      digitalWrite(cathodes4, HIGH); digitalWrite(annodes0, HIGH); break;
    case 22: //c4a1
      digitalWrite(cathodes4, HIGH); digitalWrite(annodes1, HIGH); break;
    case 23: //c4a2
      digitalWrite(cathodes4, HIGH); digitalWrite(annodes2, HIGH); break;
    case 24: //c4a3
      digitalWrite(cathodes4, HIGH); digitalWrite(annodes3, HIGH); break;
    case 25: //c4a3
      digitalWrite(cathodes4, HIGH); digitalWrite(annodes4, HIGH); break;
    case 100:
      //all on
      digitalWrite(cathodes0, HIGH); digitalWrite(annodes0, HIGH);
      digitalWrite(cathodes1, HIGH); digitalWrite(annodes1, HIGH);
      digitalWrite(cathodes2, HIGH); digitalWrite(annodes2, HIGH);
      digitalWrite(cathodes3, HIGH); digitalWrite(annodes3, HIGH);
      digitalWrite(cathodes4, HIGH); digitalWrite(annodes4, HIGH);    
      break;      
    default: // if nothing else matches, do the default
      Serial.print("ERROR LEDid = "); Serial.println(LEDid); alloff(); allon(); //delay(1500);
  }
}

void alloff(){
  // turn everything off for contrast
  digitalWrite(cathodes0, LOW);
  digitalWrite(cathodes1, LOW);
  digitalWrite(cathodes2, LOW);
  digitalWrite(cathodes3, LOW);
  digitalWrite(cathodes4, LOW);
  digitalWrite(annodes0, LOW);
  digitalWrite(annodes1, LOW);
  digitalWrite(annodes2, LOW);
  digitalWrite(annodes3, LOW);
  digitalWrite(annodes4, LOW); 
  digitalWrite(IndicatorLED, LOW);
}

void allon(){
  // turn everything off for contrast
  digitalWrite(cathodes0, HIGH);
  digitalWrite(cathodes1, HIGH);
  digitalWrite(cathodes2, HIGH);
  digitalWrite(cathodes3, HIGH);
  digitalWrite(cathodes4, HIGH);
  digitalWrite(annodes0, HIGH);
  digitalWrite(annodes1, HIGH);
  digitalWrite(annodes2, HIGH);
  digitalWrite(annodes3, HIGH);
  digitalWrite(annodes4, HIGH); 
  digitalWrite(IndicatorLED, HIGH);
}

void GameSimpleVelocityLoop(){
  // define the instructions for the array and the delay for led illumination
  //int instruction[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25}; //test
  int instruction[] = {1, 6, 11, 16, 21, 22, 23, 24, 25, 20, 15, 10, 5, 4, 3, 2, 7, 12, 17, 18, 19, 14, 9, 8, 13};
  
  // and then simply iterate through the instruction array
  int i; int arraylen = sizeof(instruction)/sizeof(int);
  while(1){
    for (i = 0; i < arraylen; i = i + 1) {
        // delay is used for setting both the ON and OFF durations
      int mydelayX= analogRead(InputPot); int mydelay = map(mydelayX, 0, 1023, 4, 200);  
        // blink the leds according to the instructions
      lightled(instruction[i]); delay(mydelay);
        // turn everything off and wait a sec (before re-reading the instructinos and starting again etc)
      alloff(); delay(mydelay/2);
    }
  }
}
