void setup() {
    _wifi.setDebug(true);
    _wifi.begin(9600); // or 115200, no difference
    Serial.println("**** WIFI current mode + AP key *****");
    Serial.println(_wifi.getWifiMode());
    Serial.println(_wifi.getAPParams());
    Serial.println(_wifi.getAPKey());
    Serial.println("**** WIFI reset AP *****");
    _wifi.setWifiMode("AP");
    _wifi.setAPParams("11BGN", "DIGIX", "C1");
    _wifi.setAPKey("OPEN", "NONE", "");
    _wifi.reset();
    Serial.println("**** WIFI reset END *****");
}
