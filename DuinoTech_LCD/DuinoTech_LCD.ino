#include <LiquidCrystal.h>
#include <EEPROM.h>

/*******************************************************
Meditation Timer

2019 Julius Roberts
********************************************************/

// define some values used by the panel and buttons
LiquidCrystal lcd(8, 9, 4, 5, 6, 7); 
int lcd_key     = 0;
int adc_key_in  = 0;
#define btnRIGHT  0
#define btnUP     1
#define btnDOWN   2
#define btnLEFT   3
#define btnSELECT 4
#define btnNONE   5
#define backlightPin 10
int brightness = 62;

// dinger
#define outputBenable 3 //orange
#define outputBin1 A3 //blue
#define outputBin2 A5 //yellow
const int dingerPwmOutput = 255; // full power!
const int dingerDuration = 24;

#define mainDurationEEPROM 0

// define globals using EEPROM values:
// new unused EEPROM locations read as 255.
int soft_start_delay = 1; // seconds
int interval_duration = 60; // seconds
//int main_duration = 5;
int main_duration = EEPROM.read(mainDurationEEPROM);

// read the buttons
int read_LCD_buttons()
{
 adc_key_in = analogRead(0);      // read the value from the sensor
 // "..We make this the 1st option for speed reasons since it will be the most likely result
 if (adc_key_in > 1000) return btnNONE;
 // "..my buttons when read are centered at these valies: 0, 144, 329, 504, 741
 // "..we add approx 50 to those values and check to see if we are close
 if (adc_key_in < 50)   return btnRIGHT;
 if (adc_key_in < 195)  return btnUP;
 if (adc_key_in < 380)  return btnDOWN;
 if (adc_key_in < 555)  return btnLEFT;
 if (adc_key_in < 790)  return btnSELECT;

 return btnNONE;  // when all others fail, return this...
}

void set_main_duration(){
   while (lcd_key != btnSELECT){
    lcd_key = read_LCD_buttons();
     switch (lcd_key){
       case btnUP:{
        main_duration++; delay(300); break;
      }
       case btnDOWN:{
         if (main_duration <= 1) {main_duration = 1; delay(300); break;}
         else {main_duration--; delay(300); break;}       
       }
    }
   lcd.clear(); lcd.setCursor(0,0);
   lcd.print("Main time "); lcd.print(main_duration); lcd.print(" min");
   delay(100);
   EEPROM.write(mainDurationEEPROM, main_duration);
 }
}

void soft_start(){
    for (int i = soft_start_delay; i > 0; i--) {
    lcd.clear(); lcd.setCursor(0,0);
    lcd.print("Warmup in "); lcd.print(i); lcd.print(" secs");
    delay(1000);
  }
  ding(dingerPwmOutput,dingerDuration);
}

void setup()
{
// set pin modes
pinMode(outputBenable, OUTPUT);
pinMode(outputBin1, OUTPUT);
pinMode(outputBin2, OUTPUT);
pinMode(backlightPin, OUTPUT);

// set brightness
analogWrite(backlightPin,brightness);

// init serial and lcd
Serial.begin(9600);
Serial.println("ready.."); 
lcd.begin(16, 2);              // start the library

// setup our vars then soft start
set_main_duration();
soft_start();
}

void loop()
{ 
  // WARMUP
  for (int i = interval_duration; i > 0; i--) {
    lcd.clear(); lcd.setCursor(0,0);
    lcd.print("Main in "); lcd.print(i); lcd.print(" secs");
    delay(1000);
  }
  ding(dingerPwmOutput,dingerDuration);

  // MAIN
  for (int i = main_duration * 60; i > 0; i--) {
    lcd.clear(); lcd.setCursor(0,0);
    lcd.print("End in "); lcd.print(i); lcd.print(" secs");
    delay(1000);
  }
  ding(dingerPwmOutput,dingerDuration);

  // END
  while(true){
    lcd.clear(); lcd.setCursor(0,0);
    lcd.print("    Finished!"); 
    delay(1000);
  }
  ding(dingerPwmOutput,dingerDuration); delay(1000); ding(dingerPwmOutput,dingerDuration);
  
}

void ding(int dingerPwmOutput, int dingerDuration){
  lcd.clear(); lcd.setCursor(0,0);
  lcd.print("    !DING!"); 
  Serial.print("ding("); Serial.print(dingerPwmOutput); Serial.print(","); Serial.print(dingerDuration); Serial.println(")");
  
  analogWrite(outputBenable, dingerPwmOutput); // Send PWM signal to L298N Enable pin
  // on
  digitalWrite(outputBin1, HIGH);
  digitalWrite(outputBin2, LOW);
  // delay
  delay(dingerDuration);
  // off
  digitalWrite(outputBin1, LOW);
  digitalWrite(outputBin2, LOW);

  delay(1000);
}
