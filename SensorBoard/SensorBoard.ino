/*
A Demo sketch for use with my homebrew sensor board.

2016 Julius Roberts
*/

/* SETUP GLOBAL VARIABLES

Below we define the hardware pin configuration.  arduino pins are
of two types (look closely on the board and you'll see); analog (ie able to
read a variable voltage) or digital, able to read if 5v is on or off.  Also
pins can be either set to input mode (ie used to sense voltage) our output, used
to supply voltage to something.*/

const int C_REDIndoorThermistorPin = A5; // one of the green wires.
const int C_LightDependentResistorPin = A2; // one of the green wires.
const int C_BluePotPin = A1; // one of the green wires.

/*You'll notice that we're using the analog pins for sensing analog voltage,
that's the significance of the A in the pin number.  You'll see that
the pins below have no A prefix, so they're digital pins.*/

const int C_RedLED = 7; // one of the blue wires
const int C_OrangeLED = 8; // one of the blue wires
const int C_GreenLED = 9; // one of the blue wires

/*All this const and int stuff is us defining the variables we'll use later.
Variables are simply names given to a location in the memory (RAM) of the
device.   we use them as placeholders, and we can use our code to access,
change and remove their contents. in C (this programming language) all
variables have a type (such as an integer or a string).

eg; int number = 0;  or string name = "bob";

variables have various properties inherited from their type, for example, a
variable of type int must be a number between -2,147,483,647 and
2,147,483,647.  There are many variable types, sometimes it's very important to
carefully choose which one.  you can't assign a string eg "bob" to a variable
of int type.

All variables also have a scope, that is, where in the code they apply to. The
name "Dad" has a very tight scope, it means a different person in each house,
and only the people in that house know who He is.  Wheras the name "Tony Abbot"
has a Global scope; everybody knows exactly who that is. The scope that
applies to a variable is determined at the time the variable is created.  any
variable defined within a function will only work if used within that
function. it's a local variable.  variables defined up the top of your code,
ie; outside any functions are global, and are available everywhere.  this can be
quite important later on.

But what's a function?  yes.. good question.  We'll come back to that. Suffice
to say it's a fragemnt of code with a name and some defined inputs and
outputs.   it does a specific thing, and if we need to do the same thing in
several places in our code, instead of copy and pasting the code everywhere we
can just write a function that contains that operation and  call the function
instead.  saves lots of effort, and has several other advantages.

already we know variabes are typed, and another attribute is that they can be
created as a constant. yes a constant variable is a thing.  a constant is
simply a type of variable where the assigned value cannot be changed when the
code is run.  It's set and forget, like your gender (well..).  But uless
specified otherwise, variables are truly variable, like your age.  you may
note that i've chosen to prefix the variable name with an identifier showing
it's type. it's not required, but can be very useful sometimes.  I could also
have used used whatever variable name i liked; the six varialbes above could
easily be called a,b,c,d,e & f and the code would work. it's just that it then
becomes less readable by humans, and that in an of itself causes other issues.
pro tip, use very descriptive variable names*/

int V_SampleCounter = 0;  //Used for counting the number of samples we take later

/* so we've completed the header section, all our global variables are defined
and  we're ready to move onto defining some functions.  All arduino sketches
MUST have at least a setup() and a loop() function.  This one has another
called Thermistor() that i downloaded from the internet because it suited my
purpose.  If you do that it's nice to leave the authors name and email address
in there if you can.

/*THE SETUP() FUNCTION. IT RUNS ONCE ONLY.*/
void setup() {

  /* This is the first time we've seen a function definition.  It's simply a
  block of   code contained within braces ie {}. Functions have names and have
  predictable   inputs and outputs, so we use them to do some work for us,
  usually repeatedly.   Functions can be something we have   written (such as
  Thermistor() down below), or use the automatic ones that   the authors of
  Arduino have provided for us.  setup() and loop() are mandatory, but there
  are may many others such   as analogRead()   or digitalWrite() or map() and so
  on.  I have absolutley no idea how they work internally   all i   know is how
  to use them, and they work predictably, so i don't need to know   any more
  than that.

  (Libraries are things that are used in this way, software written by someone
  else to do cool stuff for us so we don't have to.They contain examples and
  functions and documentation and all sorts and they are a very good way to
  extend the functionality of your device.check out
  http://www.righto.com/2009/08/multi-protocol-infrared-remote-library.html, i
  use Ken's infra red library to turn my TV on and off with just an infra red
  LED.it's brilliant.)

  The inbuilt setup() function allows us to initialize hardware or make a
  startup sequence or something.Unlike the loop() function (which we'll meet
  later, it runs once only).

  Part of the setup is telling the arduino what each of these pins will do;
  will they be used for taking input? ie determining if a putton is pressed? or
  an output, sending 5v to something? such as an LED.*/

  pinMode(C_RedLED, OUTPUT); // LEDs require voltage, so pins are used as OUTPUT
  pinMode(C_OrangeLED, OUTPUT);
  pinMode(C_GreenLED, OUTPUT);

  /*   Next is some bling, but it does introduce you to one of the control
  structures, the loop.   here we use a for loop to twinkle the lights upon start
  the for loop has 3 parts, it starts by defining an integer i as 0, that
  happens ONCE.  then there is the test by which the for loop determines if the
  code inside (ie within the braces) should be run.  the test here is by doing
  an evaluation if i < 10.  if that's true then the for loop will run the code
  once only.  the final thing is an expression that  modifies one of the inputs
  of the test, so that the for loop will only run a certain number of times.
  i++ is equivalent to i = i + 1.   long story short, in this scenario, it will
  run the code fragment (within the braces) 10 times.*/

  for (int i = 0; i < 10; i++) {
    // this section only executes if the for loop test is true.
    digitalWrite(C_GreenLED, HIGH);  delay(50); digitalWrite(C_GreenLED, LOW);

    /*digitalWrite() is a system function (as in I didn't write it). it's purpose is
    to add or remove 5v from the specified pin (it only makes sense to do that for
    a pin that's already been set in OUTPUT.  it's not compulsory you set the pin
    mode,  but it's a very good idea).  digitalWrite() it accepts two arguments,
    the pin to write to, in this case we're using the variable C_GreenLED (you may
    recall we defined what C_GreenLED was earlier, it's set to 9).  Also if you
    trace the wiring on the board, you'll see the green LED is hardwired to pin 9
    on the shield. the second argument of digitalWrite() is just HIGH or LOW, that
    is effectively on or off respectively. delay() just pauses the operation of the
    whole device for the specified time in miliseconds.*/

    digitalWrite(C_OrangeLED, HIGH); delay(50); digitalWrite(C_OrangeLED, LOW);
    digitalWrite(C_RedLED, HIGH);    delay(50); digitalWrite(C_RedLED, LOW);

    /*while we are here, note that the code inside the braces is indented?  that's
    not compulsory in C (although it is in other programming languages), however
    it's a very nice thing to do because it helps to make your code more human
    readable.   there are several attribues of code that make it more portable (ie
    easier for  other people to read and use):
    1) lots of comments.  explain your brain in plain english
    2) human readable variable names.
    3) indent inside braces.  braces within braces means double indentation.
    4) don't declare global variables where local ones would suffice.
    5) and on and on.. ;)*/
  }

  /*Finally, the last thing in our setup() function is to open serial
  communications and wait for the port to open. By this means we can use the
  Arduino serial montior to output via the serial i/o lines on the Arduino (ie
  via USB).  Without this here, you'll get no serial output.  The device would
  technically run faster because you've given it less work to do.*/
  Serial.begin(9600);
}

/*THE LOOP() FUNCTION.  This is where the main body of your code is*/
void loop() {
  /*the loop() function runs to completion, and then runs again and again infinitely.
  a loop() that contained code to turn a light on, wait a sec, then off, wait a sec
  would result in a light that blinked forever, in theory.  In fact that's what the
  Arduino Blink example actually does.
  */

  /*So we know that the loop will run forever, what if we wanted to count how
  many loops  it had actually done? you may recall we defined V_SampleCounter
  earlier. it's a variable of type int. it has the initial value of 0. Here we
  use some shorthand to increment the variable by one. */

  V_SampleCounter++;

  /*a longer (but probably more readable) equivalent is V_SampleCounter =
  V_SampleCounter + 1; So effectively this is just a count of the number of
  times the loop has been run.After awhile This number will get very
  big.Eventually it wil get so big that the type (int) used to define
  V_SampleCounter will run out of space (Remember each type has a predefined
  maximum length) and will overflow.Overflows can be dangerous, but in this
  case it will just wrap around to the start of the range again, so after the
  2,147,483,648th loop, V_SampleCounter would be -2,147,483,647 ;)*/

  int V_IndoorTemp = Thermistor(analogRead(C_REDIndoorThermistorPin));
  int V_LightLevel = analogRead(C_LightDependentResistorPin);
  int V_BluePot = analogRead(C_BluePotPin);

  /*Here we start doing some useful stuff; actually sample some sensors.  There
  is quite alot going on here, so read it slowly.  Repeatedly :)

  First you'll notice the int V_IndoorTemp.That's defining (creating) a new
  variable of type int.(You can't assign a typed variable a value not of that
  type  so if the Thermistor() function was coded to return a string, we'd get
  a compile* error).
  Second you'll notice we're assigning the V_IndoorTemp variable a value.The
  value is the output of the Thermistor() function.
  Third Thermistor() accepts one argument, and that's the output of the
  analogRead() function,
  Fourth the analogRead() function accepts one argument, the pin it's
  supposed to read from.

  [*] What's the compiler?  it's the syntax checker and magic foo thing
  that converts this human readable code into machine code the computer can
  understand.  It does that when you hit the upload button.  black black black
  magic voodoo.

  OK, so let's now read it again, but from the inside out.
  C_REDIndoorThermistorPin is set toA5, so the code is equivalent to

  int V_IndoorTemp = Thermistor(analogRead(A5));

  The arduino will go ahead and sample the voltage on A5, let's say it's
  234234. So the code (now partially evaluated) logically looks like;

  int V_IndoorTemp = Thermistor(234234);

  Thermister does some stuff to the number 234234 (scroll down below and have
  a look)  and returns a value in degrees C corresponding to that voltage,
  lets' say it's 22.  so now we have;

  int V_IndoorTemp = 22;

  And by that means we now have an integer assigned to the variable
  V_IndoorTemp.

  You can infer the operation for the assignment of the other variables by the
  same means we've discussed above.*/

  /* OK TAKE A DEEP BREATH
  If you've got this far, you're gunna make it.Here's where the cool stuff
  happens and where you can immediately start to see the power of Ardino; the
  speed and flexibility by which we can make iterative fundumental behavioural
  changes to our device.

  We we know already that we have various typed variables in RAM and they
  contain  values correspoinding to the light, the temperature, and the
  position of the potentiometer. As yet, we're not actually doing anything
  with these numbers, they're just being sampled

  We know our board has some LEDs attached, so lets define some logic by which
  these lights might be illuminated.  This is the first time we've used the if
  statement, but it's operation is obvious.  digitalWrite() is not new either, so
  I'm hoping you can work out what will happen when this is up and running.*/

  if (V_LightLevel < 600) {
    digitalWrite(C_GreenLED, HIGH);
  } else {
    digitalWrite(C_GreenLED, LOW);
  }
  /* Feel free to change any of these values, comment sections in or out, add more
  core within the if or else braces, do what you like and let the arduino show you
  what's possible*/

  if (V_IndoorTemp > 18) {
    digitalWrite(C_OrangeLED, HIGH);
  } else {
    digitalWrite(C_OrangeLED, LOW);
  }
  if (V_BluePot > 600) {
    digitalWrite(C_RedLED, HIGH);
  } else {
    digitalWrite(C_RedLED, LOW);
  }

  /* Finally, a quick primer on serial communications.  This is what we use to
  talk back to the computer.  With the USB cable still attached, and the arduino
  running our code, if you open the serial monitor in the arduino software,
  you'll see the output as defined here. this is a fantastic way to see the
  actual numbers of the variables in real time.  Note the Serial.print() vs
  Serial.println(), the latter is how we get a new line*/

  Serial.print(V_SampleCounter);
  Serial.print("   light = "); Serial.print(V_LightLevel);
  Serial.print(": temp = "); Serial.print(V_IndoorTemp);
  Serial.print(": pot = "); Serial.println(V_BluePot);

  /* PS: Imagine if you wanted the whole device to slow down a bit and only take
  it's  samples once per second (instead of as fast as it can).  That's easy,
  but just adding a little delay at the end of the loop() function.  try
  removing the comments (ie the //) at the start of the next line, then upload.
  check the serial monitor, see what's happened?*/

  //delay(1000);

  /* PPS: Performance is a word we can use to describe the speed at   which
  your device does a certain task.  In this sketch a single sample of the
  Thermister is taken once per loop.  The exact sample resolution is a little
  tricky to determine, but you could run the code and check how many samples are
  taken within a given second.  In terms of performance loops execute faster if
  they have less work to do, so optimising code (and reducing the use of delay()
  and serial output (which is slow) can be important.*/
}

float Thermistor(int RawADC) {
  /*
    Inputs ADC Value from Thermistor and outputs Temperature in Celsius
     requires: include <math.h>
    Utilizes the Steinhart-Hart Thermistor Equation:
       Temperature in Kelvin = 1 / {A + B[ln(R)] + C[ln(R)]3}
       where A = 0.001129148, B = 0.000234125 and C = 8.76741E-08

    These coefficients seem to work fairly universally, which is a bit of a
    surprise.

    Schematic:
      [Ground] -- [10k-pad-resistor] -- | -- [thermistor] --[Vcc (5 or 3.3v)]
                                        |
                                   Analog Pin 0

    In case it isn't obvious (as it wasn't to me until I thought about it), the analog ports
    measure the voltage between 0v -> Vcc which for an Arduino is a nominal 5v, but for (say)
    a JeeNode, is a nominal 3.3v.

    The resistance calculation uses the ratio of the two resistors, so the voltage
    specified above is really only required for the debugging that is commented out below

    Resistance = (1024 * PadResistance/ADC) - PadResistor

    I have used this successfully with some CH Pipe Sensors (http://www.atcsemitec.co.uk/pdfdocs/ch.pdf)
    which be obtained from http://www.rapidonline.co.uk.

  */

  //float vcc = 4.91;         // only used for display purposes, if used set to the measured Vcc.
  float pad = 9790;         // balance/pad resistor value, set this to the measured resistance of your pad resistor
  //float thermr = 4700;      // thermistor nominal resistance
  float thermr = 10000;      // thermistor nominal resistance
  long Resistance;
  float Temp;  // Dual-Purpose variable to save space.

  Resistance = ((1024 * pad / RawADC) - pad);
  Temp = log(Resistance); // Saving the Log(resistance) so not to calculate  it 4 times later
  Temp = 1 / (0.001129148 + (0.000234125 * Temp) + (0.0000000876741 * Temp * Temp * Temp));
  Temp = Temp - 273.15;  // Convert Kelvin to Celsius

  //Temp = Temp - 20; // Hoolios unforgivably bad conversion factor when using the 4.k thermistor
  Temp = Temp - 3; // Hoolios unforgivably bad conversion factor when using the 10k thermistor.

  return Temp;   // Return the Temperature
}
