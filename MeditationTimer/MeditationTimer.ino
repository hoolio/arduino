#include <LiquidCrystal.h>
#include <EEPROM.h>

/*******************************************************
Meditation Timer

2019 Julius Roberts
********************************************************/

// define some values used by the panel and buttons
LiquidCrystal lcd(8, 9, 4, 5, 6, 7);
int brightness = 62; 
int lcd_key     = 0;
int adc_key_in  = 0;
#define btnRIGHT  0
#define btnUP     1
#define btnDOWN   2
#define btnLEFT   3
#define btnSELECT 4
#define btnNONE   5
#define backlightPin 10

// dinger
#define outputBenable 3 //orange
#define outputBin1 A3 //red
#define outputBin2 A5 //brown

// EEPROM LOCATIONS (these are set using calibrate_bell())
// define globals using EEPROM values:
// new unused EEPROM locations read as 255.
#define mainDurationEEPROM 0
#define dingerDurationEEPROM 2
#define dingerDutyCycleEEPROM 1
#define intervalDurationEEPROM 3
#define leadInDurationEEPROM 4

// these are all configurable via the menus
// and then permanently stored in the EEPROM
int lead_in_duration = EEPROM.read(leadInDurationEEPROM);
int dingerDuration = EEPROM.read(dingerDurationEEPROM);
int dingerDutyCycle = EEPROM.read(dingerDutyCycleEEPROM);
int main_duration = EEPROM.read(mainDurationEEPROM);
int interval_duration = EEPROM.read(intervalDurationEEPROM);

// read the buttons
int read_LCD_buttons(){
 adc_key_in = analogRead(0);      // read the value from the sensor
 // "..We make this the 1st option for speed reasons since it will be the most likely result
 if (adc_key_in > 1000) return btnNONE;
 // "..my buttons when read are centered at these valies: 0, 144, 329, 504, 741
 // "..we add approx 50 to those values and check to see if we are close
 if (adc_key_in < 50)   return btnRIGHT;
 if (adc_key_in < 195)  return btnUP;
 if (adc_key_in < 380)  return btnDOWN;
 if (adc_key_in < 555)  return btnLEFT;
 if (adc_key_in < 790)  return btnSELECT;

 return btnNONE;  // when all others fail, return this...
}

void ding(int dingerDutyCycle, int dingerDuration){
  lcd.clear(); lcd.setCursor(0,0);
  lcd.print("      ding");
  // Serial.print("dingerDutyCycle: "); Serial.print(dingerDutyCycle);
  // Serial.print(" dingerDuration: "); Serial.println(dingerDuration);

  //convert duty cycle to pwmoutput
  int dingerPwmOutput = map(dingerDutyCycle, 0, 100, 0, 254);

  analogWrite(outputBenable, dingerPwmOutput); // Send PWM signal to L298N Enable pin
  // on
  digitalWrite(outputBin1, HIGH);
  digitalWrite(outputBin2, LOW);
  // delay
  delay(dingerDuration);
  // off
  digitalWrite(outputBin1, LOW);
  digitalWrite(outputBin2, LOW);

  //delay(1000);
}

void calibrate_bell(){
   while (true){
    // update display
     lcd.clear();
     lcd.setCursor(0,0);
     lcd.print("Duration: ");  lcd.println(dingerDuration);
     lcd.setCursor(0,1);
     lcd.print("DutyCycle: ");  lcd.println(dingerDutyCycle);
     delay(150);

     // detect input
     lcd_key = read_LCD_buttons();
     switch (lcd_key){
       case btnUP:{   dingerDuration++; break;}
       case btnDOWN:{ dingerDuration--; break;}
       case btnLEFT:{ dingerDutyCycle--; break;}
       case btnRIGHT:{
        if(dingerDutyCycle >= 100){dingerDutyCycle = 100;
          }else{dingerDutyCycle++;}
        break;}
       case btnSELECT:{
           ding(dingerDutyCycle,dingerDuration);
           delay(1000);
           //write values to EEPROM
           EEPROM.write(dingerDurationEEPROM, dingerDuration);
           EEPROM.write(dingerDutyCycleEEPROM, dingerDutyCycle);
           break;}
       }
   }
}

void set_durations(){
  int repeat_delay = 200;
   while (lcd_key != btnSELECT){
     lcd_key = read_LCD_buttons();
     switch (lcd_key){
       case btnLEFT:{set_interval_duration();}
       case btnUP:{main_duration++; delay(repeat_delay); break;}
       case btnDOWN:{if (main_duration <= 1) {main_duration = 1; delay(repeat_delay); break;}
         else {main_duration--; delay(repeat_delay); break;}}
     }
     lcd.clear(); lcd.setCursor(0,0);
     lcd.print("main sit "); lcd.print(main_duration); lcd.print(" mins");
     delay(repeat_delay);
     EEPROM.write(mainDurationEEPROM, main_duration);
   }
}

void set_interval_duration(){
   delay(300); // prevent firing btnLEFT straight away
   
   int repeat_delay = 100;
   while (lcd_key != btnSELECT){
     lcd_key = read_LCD_buttons();
     switch (lcd_key){
       case btnLEFT:{set_lead_in_duration();}
       case btnRIGHT:{set_durations();}
       case btnUP:{interval_duration++; delay(repeat_delay); break;}
       case btnDOWN:{if (interval_duration <= 1) {interval_duration = 1; delay(repeat_delay); break;}
         else {interval_duration--; delay(repeat_delay); break;}}
     }
     lcd.clear(); lcd.setCursor(0,0);
     lcd.print("interval "); lcd.print(interval_duration); lcd.print(" secs");
     delay(repeat_delay);
     EEPROM.write(intervalDurationEEPROM, interval_duration);
   }
}

void set_lead_in_duration(){
   delay(300); // prevent firing btnLEFT straight away

   int repeat_delay = 100;
   while (lcd_key != btnSELECT){
     lcd_key = read_LCD_buttons();
     switch (lcd_key){
       case btnRIGHT:{set_interval_duration();}
       case btnUP:{lead_in_duration++; delay(repeat_delay); break;}
       case btnDOWN:{if (lead_in_duration <= 1) {lead_in_duration = 1; delay(repeat_delay); break;}
         else {lead_in_duration--; delay(repeat_delay); break;}}
     }
     lcd.clear(); lcd.setCursor(0,0);
     lcd.print("lead in "); lcd.print(lead_in_duration); lcd.print(" secs");
     delay(repeat_delay);
     EEPROM.write(leadInDurationEEPROM, lead_in_duration);
   }
}

void lead_in(){
  for (int i = lead_in_duration; i > 0; i--) {
    lcd.clear(); lcd.setCursor(0,0);
    lcd.print("warmup in "); lcd.print(i); lcd.print(" secs");
    delay(1000);
  }
  ding(dingerDutyCycle,dingerDuration);
}

void welcome(){
  lcd.clear();
  lcd.setCursor(0,0);  lcd.print("       mu");

  // this just loops for 3000ms and 
  // starts calibrate if SELECT is pressed
  for (int i = 1; i < 3000; i++) {
     lcd_key = read_LCD_buttons();
     switch (lcd_key){case btnSELECT:{calibrate_bell();}}
     delay(1);
   }  
}

void setup(){
  // init serial and lcd
  //  Serial.begin(9600);
  //  Serial.println("ready..");
  lcd.begin(16, 2);
  
  // set pin modes
  pinMode(outputBenable, OUTPUT);
  pinMode(outputBin1, OUTPUT);
  pinMode(outputBin2, OUTPUT);
  pinMode(backlightPin, OUTPUT);

  // set brightness
  analogWrite(backlightPin,brightness);

  //
  welcome();
  set_durations();

  //start
  lead_in();
}

void(* resetFunc) (void) = 0;

void loop(){
  // WARMUP
  for (int i = interval_duration; i > 0; i--) {
    lcd.clear(); lcd.setCursor(0,0);
    lcd.print(" main in "); lcd.print(i); lcd.print(" secs");
    delay(1000);
  }
  ding(dingerDutyCycle,dingerDuration);

  // MAIN
  for (int i = main_duration * 60; i > 0; i--) {
    lcd.clear(); lcd.setCursor(0,0);
    lcd.print(" end in "); lcd.print(i); lcd.print(" secs");
    delay(1000);
  }
  ding(dingerDutyCycle,dingerDuration);
  delay(7000);
  ding(dingerDutyCycle,dingerDuration);

  // finished
  lcd.clear(); lcd.setCursor(0,0);
  lcd.print("    finished!");
  delay(5000);

  // reset the device for the next session
  resetFunc();  //call reset
}
