/* == wattsCleverTest ==
 * 
 * A simple sketch to send HIGH to the pins connected 
 * by some means to a wattsClever remote.
*/

const int PIN_RELAY_ONE_on = A1; // set this HIGH to turn on relay 1
const int PIN_RELAY_ONE_off = A2; // set this HIGH to turn off relay 1
const int PIN_RELAY_TWO_on = 11; // set this HIGH to turn ON relay 2
const int PIN_RELAY_TWO_off = 12; // set this HIGH to turn OFF relay 2

const int BUTTON_PRESS_DURATION = 1200; // > 40, longer = more reliable.
const int CYCLE_DURATION = 2000;


void setup()
{  
	pinMode(PIN_RELAY_ONE_on, OUTPUT); 
	pinMode(PIN_RELAY_ONE_off, OUTPUT); 
	pinMode(PIN_RELAY_TWO_on, OUTPUT); 
	pinMode(PIN_RELAY_TWO_off, OUTPUT); 
	
	digitalWrite(PIN_RELAY_ONE_on, LOW);
	digitalWrite(PIN_RELAY_ONE_off, LOW);
	digitalWrite(PIN_RELAY_TWO_on, LOW);
	digitalWrite(PIN_RELAY_TWO_off, LOW);

    Serial.begin(9600);  
}

void loop()
{
	// === RELAY ONE ===
	//
	//set on
	digitalWrite(13, HIGH);
	digitalWrite(PIN_RELAY_ONE_on, HIGH);
	delay(BUTTON_PRESS_DURATION);
	digitalWrite(PIN_RELAY_ONE_on, LOW);
	//
	delay(CYCLE_DURATION);
	//
	//set off
	digitalWrite(13, LOW);
	digitalWrite(PIN_RELAY_ONE_off, HIGH);
	delay(BUTTON_PRESS_DURATION);
	digitalWrite(PIN_RELAY_ONE_off, LOW);
	//
	delay(CYCLE_DURATION/2);

	// === RELAY TWO ===
	//
	//set on
	digitalWrite(13, HIGH);
	digitalWrite(PIN_RELAY_TWO_on, HIGH);
	delay(BUTTON_PRESS_DURATION);
	digitalWrite(PIN_RELAY_TWO_on, LOW);
	//
	delay(CYCLE_DURATION);
	//
	//set off
	digitalWrite(13, LOW);
	digitalWrite(PIN_RELAY_TWO_off, HIGH);
	delay(BUTTON_PRESS_DURATION);
	digitalWrite(PIN_RELAY_TWO_off, LOW);
	//
	delay(CYCLE_DURATION/2);

}

