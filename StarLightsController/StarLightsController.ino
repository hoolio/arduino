// Include libraries
#include <SoftI2C.h> // RTC
#include <DS3232RTC.h> // RTC
#include <LiquidCrystal.h> // LCD

// === PIN ASSIGNMENTS ===
//
const char PIN_RTC_SDA = A4; // realtime clock data
const char PIN_RTC_SCL = A5; // realtime clock clock

LiquidCrystal lcd( 8, 9, 4, 5, 6, 7 );

const int PIN_STARS_on = A1; // set this HIGH to turn ON relay 1
const int PIN_STARS_off = A2; // set this HIGH to turn OFF relay 1
//const int PIN_STARS_on = 11; // set this HIGH to turn ON relay 2
//const int PIN_STARS_off = 12; // set this HIGH to turn OFF relay 2

// === CLOCK ===
//Setup Real Time Clock (RTC)
SoftI2C i2c(PIN_RTC_SDA, PIN_RTC_SCL); // assign pins to SDA and SCL
DS3232RTC rtc(i2c);
RTCTime time;

// == RELAY STUFF ==
//
// c_MINIMUM_STARS_RETRIGGER_INTERVAL how often to TRIGGER/RETRIGGER relay state.
//   we can retrigger the relay with the current state to be sure the
//     intial presses actually worked.  it also serves to stop flapping relays
//const int c_MINIMUM_STARS_RETRIGGER_INTERVAL = 60;  // seconds

// c_BUTTON_PRESS_DURATION how long to press the button on the remote
//   must be > 40, the longer equals more predictable.
const int c_BUTTON_PRESS_DURATION = 600; // miliseconds

//setup some vars
uint32_t epochTime = 0;
uint32_t countdown = 0;
uint32_t onInEpochTime = 0;
uint32_t Scheduled = 0;

// == FUNCTION PROTOTYPES == WHY??
/*void clearLCD();
void turnHisLightsOn(String);
void turnHisLightsOn(String);*/

void setup() {
	// setup lcd & serial
	lcd.begin(16, 2);
	Serial.begin(9600);

	// === STARTUP SPLASH ===
	//
	clearLCD();
	lcd.setCursor( 0, 0 );  lcd.print(" = Starzmatic = ");
	lcd.setCursor( 0, 1 );  lcd.print(" =  v2.00000  = "); 
	delay(600); //ms 

	// setup relay control pins
	pinMode(PIN_STARS_on, OUTPUT); 
	pinMode(PIN_STARS_off, OUTPUT); 
	digitalWrite(PIN_STARS_on, LOW);
	digitalWrite(PIN_STARS_off, LOW);

	//turn on lights to start with
	turnHisLightsOn("    Startup!    ");
	delay(600);
}

void loop() {
	// This code assumes the lights will be on to start with.
	// For whatever reason we're using delays instead of RTC timers.
	// i'd like to not have to do 3 if statements, instead to a loop around the warnings
	//
	/*
	// Timings:             off event,    off message, off duration,  on message    
	String warningOne[4] = {"10:14:55","  Reading time  ","5000",        ""};
	String warningTwo[4] = {"10:15:50","    Up in bed   ","10000",       ""};
	String bedtime[2] =    {"10:17:00","   Goodnight!   "};
	*/
	// Timings:             off event,    off message, off duration,  on message    
	String warningOne[4] = {"19:29:55","  Reading time  ","5000",        ""};
	String warningTwo[4] = {"19:47:50","    Up in bed   ","10000",       ""};
	String bedtime[2] =    {"19:50:00","   Goodnight!   "};

	// Update LCD
	lcd.setCursor( 0, 0); lcd.print(" NOW: "); lcd.print(getHumanTimeHMS());
	lcd.setCursor( 0, 1); lcd.print(" BED: "); lcd.print(bedtime[0]);

	// Execute time based actionss
	if (getHumanTimeHMS() == warningOne[0]) {
		turnHisLightsOff(warningOne[1]); delay(warningOne[2].toInt());
		turnHisLightsOn(warningOne[3]);}

	if (getHumanTimeHMS() == warningTwo[0]) {
		turnHisLightsOff(warningTwo[1]); delay(warningTwo[2].toInt());
		turnHisLightsOn(warningTwo[3]);}

	if (getHumanTimeHMS() == bedtime[0]) {
		turnHisLightsOff(bedtime[1]);}

	// Delay to decrease load on clock
	delay(900);

	/* How to an RTC timer instead of delay.
	Scheduled = getEpochTime() + 5;  // seconds
	while (getEpochTime() != Scheduled){
		countdown = Scheduled - getEpochTime();
		lcd.setCursor( 0, 1); lcd.print("     on in "); lcd.print(countdown);
		delay(999);
	}*/
}

void turnHisLightsOn(String message){
	clearLCD();

	lcd.setCursor( 0, 0 ); lcd.print(message);
	Serial.print(getHumanTime()); Serial.print(": ON  -"); Serial.println(message);

	digitalWrite(PIN_STARS_on, HIGH);
	delay(c_BUTTON_PRESS_DURATION);
	digitalWrite(PIN_STARS_on, LOW);
}

void turnHisLightsOff(String message){
	clearLCD();

	lcd.setCursor( 0, 0 ); lcd.print(message);
	Serial.print(getHumanTime()); Serial.print(": OFF -"); Serial.println(message);

	digitalWrite(PIN_STARS_off, HIGH);
	delay(c_BUTTON_PRESS_DURATION);
	digitalWrite(PIN_STARS_off, LOW);
}

String getHumanTimeHMS(){
	RTCTime time; rtc.readTime(&time);

	String theTimeDeconstructed;

	theTimeDeconstructed =  printDec2(time.hour);
	theTimeDeconstructed += ':';
	theTimeDeconstructed += printDec2(time.minute);
	theTimeDeconstructed += ':';
	theTimeDeconstructed += printDec2(time.second);

	return theTimeDeconstructed;
}

String getHumanTime(){
	RTCTime time; rtc.readTime(&time);
	RTCDate date; rtc.readDate(&date);

	String theTimeDeconstructed;

	theTimeDeconstructed =  printDec2(time.hour);
	theTimeDeconstructed += ':';
	theTimeDeconstructed += printDec2(time.minute);
	theTimeDeconstructed += ':';
	theTimeDeconstructed += printDec2(time.second);
	theTimeDeconstructed += ' ';

	//theTimeDeconstructed += date.day;
	//theTimeDeconstructed += '/';
	//theTimeDeconstructed += date.month;
	//theTimeDeconstructed += "/'";
	//theTimeDeconstructed += date.year-2000;

	return theTimeDeconstructed;
}

String printDec2(int value)
{
    String stringOne;
    stringOne =  (char)('0' + (value / 10)); // this is the 10's of the time unit
    stringOne += (char)('0' + (value % 10)); // this is the 1's

    return stringOne;
}

uint32_t getEpochTime(){
    // assemble time elements into time_t 

    #define LEAP_YEAR(Y)     ( ((1970+Y)>0) && !((1970+Y)%4) && ( ((1970+Y)%100) || !((1970+Y)%400) ) )
    #define SECS_PER_DAY 86400
    #define SECS_PER_HOUR 3600
    #define SECS_PER_MIN 60

    static const uint8_t monthDays[]={31,28,31,30,31,30,31,31,30,31,30,31}; // API starts months from 1, this array starts from 0

    RTCTime time;
    RTCDate date;
    rtc.readTime(&time);
    rtc.readDate(&date);

    unsigned long currentYear = date.year;
    unsigned long currentMonth = date.month;
    unsigned long currentDay = date.day;
    unsigned long currentHour = time.hour;
    unsigned long currentMin = time.minute;
    unsigned long currentSecond = time.second;

    /*unsigned long currentYear = 2016;
    unsigned long currentMonth = 6;
    unsigned long currentDay = 6;
    unsigned long currentHour = 9;
    unsigned long currentMin = 31;
    unsigned long currentSecond = 11;*/

    unsigned long i = 0; 
    unsigned long secsFromYears = 0;
    unsigned long secsFromMonths = 0;
    unsigned long secsFromDays = 0;
    unsigned long secsFromHours = 0;
    unsigned long secsFromMins = 0;
    unsigned long secs = 0;
    unsigned long totalSecs = 0;

    // seconds from 1970 till 1 jan 00:00:00 of the given year
    secsFromYears = (currentYear-1970)*(SECS_PER_DAY * 365);

    for (i = 1971; i < currentYear; i++) {
        if (LEAP_YEAR(i)) {
            secsFromYears += SECS_PER_DAY;   // add extra days for leap years           
        }
    }

    // add days for this year, months start from 1
    for (i = 1; i < currentMonth; i++) {
        if ( (i == 2) && LEAP_YEAR(currentYear)) { 
            secsFromMonths = (secsFromMonths + (SECS_PER_DAY * 29));
        } else {
            secsFromMonths = (secsFromMonths + (SECS_PER_DAY * monthDays[i-1]));  //monthDay array starts from 0
        }
    }

    secsFromDays  = currentDay * SECS_PER_DAY;  
    secsFromHours = currentHour * SECS_PER_HOUR;
    secsFromMins  = currentMin * SECS_PER_MIN; 
    secs          = currentSecond; 

    totalSecs = secsFromYears + secsFromMonths + secsFromDays + secsFromHours + secsFromMins + secs;

    if (0) { 
    	Serial.print("currentYear:    "); Serial.println(currentYear); 
    	Serial.print("currentMonth:   "); Serial.println(currentMonth); 
    	Serial.print("currentDay:     "); Serial.println(currentDay); 
    	Serial.print("currentHour:    "); Serial.println(currentHour); 
    	Serial.print("currentMin:     "); Serial.println(currentMin); 
    	Serial.print("currentSecond:  "); Serial.println(currentSecond); 
    	Serial.print("secsFromYears:  "); Serial.println(secsFromYears); 
    	Serial.print("secsFromMonths: "); Serial.println(secsFromMonths);
    	Serial.print("secsFromDays:   "); Serial.println(secsFromDays);
    	Serial.print("secsFromHours:  "); Serial.println(secsFromHours);
    	Serial.print("secsFromMins:   "); Serial.println(secsFromMins);
    	Serial.print("secs:           "); Serial.println(secs);
    	Serial.print("totalSecs:      "); Serial.println(totalSecs);
    	Serial.println();
    }    

    return totalSecs; 
}

void clearLCD(){
    // write nothing all over the LCD to clear it.

    lcd.setCursor(0, 0); lcd.print("                ");
    lcd.setCursor(0, 1); lcd.print("                ");
}
