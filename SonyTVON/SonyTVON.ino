// This sketch will send out a canon D50 trigger signal (probably works with most canons)
// See the full tutorial at http://www.ladyada.net/learn/sensors/ir.html
// this code is public domain, please enjoy!

int IRledPin = 12; // LED connected to digital pin 12

// The setup() method runs once, when the sketch starts

void setup() {
  // initialize the IR digital pin as an output:
  pinMode(IRledPin, OUTPUT);
  
  Serial.begin(9600);
}

void loop()
{
  //SendSonyOnCode();
  pulseIR(10000);
  delay(5000);
     
  //while(1){};
}

// This procedure sends a 38KHz pulse to the IRledPin
// for a certain # of microseconds. We'll use this whenever we need to send codes
void pulseIR(long microsecs) {
  // we'll count down from the number of microseconds we are told to wait
  
  cli(); // this turns off any background interrupts
  
  while (microsecs > 0) {
    // 38 kHz is about 13 microseconds high and 13 microseconds low
   digitalWrite(IRledPin, HIGH); // this takes about 3 microseconds to happen
   delayMicroseconds(10); // hang out for 10 microseconds
   digitalWrite(IRledPin, LOW); // this also takes about 3 microseconds
   delayMicroseconds(10); // hang out for 10 microseconds

   // so 26 microseconds altogether
   microsecs -= 26;
  }
  
  sei(); // this turns them back on
}

void SendSonyOnCode() {
  // This is the code for my particular Canon, for others use the tutorial
  // to 'grab' the proper code from the remote
  
  int IRsignal[] = {
// ON, OFF (in 10's of microseconds)
	238, 58,
	118, 56,
	60, 58,
	116, 58,
	58, 58,
	118, 58,
	58, 58,
	58, 58,
	118, 58,
	58, 58,
	58, 58,
	60, 56,
	58, 2522,
	236, 56,
	118, 58,
	58, 58,
	116, 60,
	58, 58,
	116, 58,
	58, 60,
	58, 58,
	116, 58,
	58, 58,
	60, 56,
	60, 58,
	58, 2520,
	234, 58,
	118, 56,
	60, 58,
	116, 58,
	58, 58,
	118, 58,
	58, 58,
	58, 58,
	118, 58,
	58, 58,
	58, 58,
	58, 60,
	58, 2520,
	234, 58,
	118, 56,
	60, 58,
	116, 58,
	58, 58,
	118, 58,
	58, 58,
	58, 58,
	118, 58,
	58, 58,
	58, 58,
	58, 58,
	58, 2522,
	234, 58,
	116, 58,
	60, 56,
	118, 58,
	58, 58,
	118, 58,
	58, 58,
	58, 58,
	118, 58,
	58, 58,
	58, 58,
	58, 58,
	60, 2520,
	234, 58,
	116, 58,
	60, 58,
	116, 58,
	58, 58,
	118, 58,
	58, 58,
	58, 58,
	118, 58,
	58, 58,
	58, 58,
	58, 58,
	60, 2520,
	234, 58,
	118, 56,
	60, 56,
	118, 58,
	58, 58,
	118, 58,
	58, 58,
	58, 58,
	118, 58,
	58, 58,
	58, 58,
	58, 58,
	58, 0};

  //determine array length
  int ArrayLen=91;
  
  //Serial.print("Array length "); Serial.println(ArrayLen);
  
  // iterate through the array
  for(int i=0; i < ArrayLen; i++){
    int onPulseTime=IRsignal[i];
    int offPulseTime=IRsignal[i+1];
  
      Serial.print(i); Serial.print(" On  "); Serial.print(onPulseTime);
      //pulseIR(onPulseTime);
     
      Serial.print(i+1); Serial.print(" Off "); Serial.println(offPulseTime);
      //delayMicroseconds(offPulseTime);
  }  
  
  delay(3000);
}
