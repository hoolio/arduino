/*
  biketempled
  lights RGB led blue, green through red proportional to an integer temp.
	int temp defined by potentiometer and analog input or irtemp module
 */
 
#include "IRTemp.h"
 
// pin assignments
int c_potPin = 0;  // pot used to control brightness
int c_dataPin = 2;  // IRTemp module
int c_clockPin = 3;  // IRTemp module - this must be either pin 2 or pin 3
int c_acquirePin = 4;  // IRTemp module
int c_redPin	 = 10; // RGB LED Red
int c_greenPin	 = 5;  // RGB LED Green
int c_bluePin	 = 6;  // RGB LED Blue

// veriable declarations
float v_tempC;					
float v_pot; 
int v_blueTemp= 0; int v_greenTemp= 0; int v_redTemp= 0;

// setup the irtemp module/library
IRTemp irTemp(c_acquirePin, c_clockPin, c_dataPin);
static const TempUnit SCALE=CELSIUS;  // Options are CELSIUS, FAHRENHEIT

// to illuminate the three leds, Red, Green and Blue, we need need to 
//  determine how much of each colour we should illuminate at a given
//  temperature.  instead of hardcoding the values for illumination 
//  we use percentages of a tempceiling; that being the highest value
//  we could reasonably recieve.  for example then, we may decide to set
//  the highest possible amount of red for if the temp equals, the temp
//  ceiling.  and perhaps the higest green when we're only at 50% etc.

int c_tempceiling = 100;  // the highest value we will reasonably recieve.
int c_tempfloor = 0;	 
int v_brightnessRAW = 0;  // this controlled by pot, hence RAW
int v_bluetemp_low = c_tempfloor;
int v_bluetemp_high = c_tempceiling * 0.45; // ie blue will be off at this value
int v_greentemp_low = c_tempceiling * 0.10;
int v_greentemp_mid = c_tempceiling * 0.5;
int v_greentemp_high = c_tempceiling * 0.90; // ie green will be off at this value
int v_redtemp_low = c_tempceiling * 0.65;
int v_redtemp_high = c_tempceiling;

// enable or disable debug 
int c_DEBUG = 1;

void setup()
{
	if(c_DEBUG){Serial.begin(9600);} //opens serial port, sets data rate to 9600 bps
}

void loop()
{	
	// get the current temp from the IR temp sensor
	v_tempC = getIRTemp();			

	// then illuminate the correct leds according to the lookup logic
	// red led will blink on out of range (error) on v_tempC
	illuminateRGB(v_tempC);
		
	//delay(10); //wait X ms before sending new data	
}

int getIRTemp(void){
    float irTemperature = irTemp.getIRTemperature(SCALE);
    //float ambientTemperature = irTemp.getAmbientTemperature(SCALE);
    //float diff = irTemperature - ambientTemperature;
    
    //Serial.print(" AMBTEMP= "); Serial.print(ambientTemperature);
	//Serial.print(" DIFF= "); Serial.println(diff);    
    
    if (isnan(irTemperature)) {
		Serial.print("IRTEMP= FAIL "); Serial.print(irTemperature);				
	} else {        
		if(c_DEBUG){Serial.print("IRTEMP= "); Serial.print(irTemperature);}		
	}	
	
	return irTemperature;
}

int illuminateRGB(int v_tempC) {		
	// this function is really quite ugly.  very happy to optimise this :)
	
	//  it accepts the current temp and a brightness.  it maps the temp
	//  to various blends of the R G and B LEDS to make a colour
	//  range BLUE > GREEN > RED as temp increases.  brightness constrains
	//  the upper limit of the brighness for all the LEDs.	
	
	// obtain and map the brightness input down to the correct range
	v_brightnessRAW = analogRead(c_potPin); 	
	int v_brightness = map(v_brightnessRAW, 0, 1024, 0, 255);
	
	// do some debug output
	if(c_DEBUG){Serial.print(" TEMP=");  Serial.print(v_tempC); }
	if(c_DEBUG){Serial.print(" RED=");  Serial.print(v_redTemp); }
	if(c_DEBUG){Serial.print(" GREEN= ");  Serial.print(v_greenTemp); }
	if(c_DEBUG){Serial.print(" BLUE="); 	Serial.print(v_blueTemp); }	
	if(c_DEBUG){Serial.print(" BRIGHT= "); Serial.println(v_brightness);}
		
	// find the correct set of illumination for the current temp
	// if current temp is 99999 then there was an error getting a temp
	// so we just show a blinking red led;	
	/*
	if (v_tempC < 1000){
		// set all leds off then blink red light
		Serial.print("IRTEMP= FAIL "); Serial.println(v_tempC);	
		analogWrite(c_redPin, 0); delay(300);	
		v_tempC = v_brightness; // set red hot for error display
	}*/
	
	// blue led
	if(v_tempC < v_bluetemp_low){
		analogWrite(c_bluePin, v_brightness);} 	
	else if(v_tempC > v_bluetemp_low  && v_tempC <= v_bluetemp_high){
		v_blueTemp= map(v_tempC, v_bluetemp_low, v_bluetemp_high, v_brightness, 0); 
		analogWrite(c_bluePin, v_blueTemp);}
	else if(v_tempC > v_bluetemp_high){
		analogWrite(c_bluePin, v_bluetemp_low);}
	
	// green pin
	if(v_tempC < v_greentemp_low){
		analogWrite(c_greenPin, 0);}
	else if(v_tempC > v_greentemp_low && v_tempC <= v_greentemp_mid){
		v_greenTemp = map(v_tempC, v_greentemp_low, v_greentemp_mid, 1, v_brightness); 
		analogWrite(c_greenPin, v_greenTemp);}
	else if(v_tempC > v_greentemp_mid && v_tempC <= v_greentemp_high){
		v_greenTemp = map(v_tempC, v_greentemp_mid, v_greentemp_high, v_brightness, 0); 
		analogWrite(c_greenPin, v_greenTemp);}
	
	// red pin
	else if(v_tempC > v_greentemp_high){
		analogWrite(c_greenPin, 0);}
	if(v_tempC < v_redtemp_low){
		analogWrite(c_redPin, 0);}
	else if(v_tempC >= v_redtemp_low){
		v_redTemp= map(v_tempC, v_redtemp_low, c_tempceiling, 1, v_brightness);
		analogWrite(c_redPin, v_redTemp);}
	else if(v_tempC > c_tempceiling){
		analogWrite(c_redPin, v_brightness);}
}


