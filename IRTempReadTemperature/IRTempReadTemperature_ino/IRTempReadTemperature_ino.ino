/*
 * File:    readTemperature.ino
 * Version: 1.0
 * Author:  Andy Gelme (@geekscape)
 * License: GPLv3
 *
 * For more information see www.freetronics.com/irtemp
 *
 * IRTemp library uses an Arduino interrupt:
 *   If PIN_CLOCK = 2, then Arduino interrupt 0 is used
 *   If PIN_CLOCK = 3, then Arduino interrupt 1 is used
 */

#include "IRTemp.h"

static const byte PIN_DATA    = 2;
static const byte PIN_CLOCK   = 3;  // Must be either pin 2 or pin 3
static const byte PIN_ACQUIRE = 4;

static const TempUnit SCALE=CELSIUS;  // Options are CELSIUS, FAHRENHEIT

IRTemp irTemp(PIN_ACQUIRE, PIN_CLOCK, PIN_DATA);

void setup(void) {
  Serial.begin(115200);
  Serial.println("IRTemp example");
  Serial.println("~~~~~~~~~~~~~~");
}

void loop(void) {
  float irTemperature = irTemp.getIRTemperature(SCALE);
  float ambientTemperature = irTemp.getAmbientTemperature(SCALE);
  float diff = irTemperature - ambientTemperature;
  
  if (isnan(irTemperature)) {
    Serial.println("Failed");
  }
  
  Serial.print("IRed= "); Serial.print(irTemperature);
  Serial.print("  Amb= "); Serial.print(ambientTemperature);
  Serial.print("  Diff= "); Serial.println(diff);

  //delay(500);
}
