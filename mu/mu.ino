/* mu:   A meditation timer for arduino.
Author:  Hoolio Wobbits, 2019
Parts:   Arduino, L298 motor driver, DFRobot LCD Sheild,
          a 12v power source.  I'm using an NBN UPS battery.
          A bell, I've hacked an old fire bell, it dings on a
          12v pulse of around 30ms.  
Wiring:  As per pin assignments below.  Wiring for the 
          L298 is as per it's doco.
Notes:   I'm not using the 5v from the L298 for the arduino,
          I found that it's not enough power for the LCD.
Usage:   Start the device, it says "mu"
          hit select.
          meditate.
Details: The device has 4 timers.  
         They count in seconds, seconds, mins, mins.
         Values are read from EEPROM so on a new device they are wrong 
         Initial use, start the device, it says "mu".
         Press the down arrow until you find factory_reset() and hit it.
         That will set the timers to 16, 60, 10, 0 and rest the device.
         Hit select to meditate using default times.  Or
         Hit the down arrow again and use left/right arrow to change time(s)
         Note that down near the bottom you can also see calibrate().
         Use left/right for dutyCycle, up/down for dingerDuration, select to ding.
         When you're happy with calibrate(), hit the reset button.
*/

#include <Arduino.h>
#include <LiquidCrystal.h>
#include <EEPROM.h>

// Pin mapping for the display:
LiquidCrystal lcd(8, 9, 4, 5, 6, 7);

// dinger
#define outputBenable 3 //orange
#define outputBin1 A3 //red
#define outputBin2 A5 //brown

// vars for buttons
const int lcd_buttons_pin = 0;
int lcd_key     = 0;
int adc_key_in  = 0;
#define btnRIGHT     0
#define btnUP        1
#define btnDOWN      2
#define btnLEFT      3
#define btnSELECT    4
#define btnNONE      5
#define backlightPin 10

//memory
#define dingerDurationEEPROM  0
#define dingerDutyCycleEEPROM 1
#define timer1DurationEEPROM  2
#define timer2DurationEEPROM  3
#define timer3DurationEEPROM  4
#define timer4DurationEEPROM  5
#define backlightDutyEEPROM   6

// note; for new devices, the default EEPROM values
// can be set to sane defaults by running factory_reset()
int timer1duration  = EEPROM.read(timer1DurationEEPROM);
int timer2duration  = EEPROM.read(timer2DurationEEPROM);
int timer3duration  = EEPROM.read(timer3DurationEEPROM);
int timer4duration  = EEPROM.read(timer4DurationEEPROM);
int backlightDuty   = EEPROM.read(backlightDutyEEPROM);
int dingerDuration  = EEPROM.read(dingerDurationEEPROM);
int dingerDutyCycle = EEPROM.read(dingerDutyCycleEEPROM);

// initial startup "screen".  
int menu = 2;
int menu_delay = 300;

// this displays the main menu according to the
// global "menu" variable (see above).  as that 
// gets incremented/decremented, subsequent iterations
// of updateMenu() will display a new menu "screen"

void updateMenu() {
  switch (menu) {
    case 0:
      menu = 1;
      break;
    case 1:
      lcd.clear();
      lcd.print("> t1 secs: "); lcd.print(timer1duration);
      lcd.setCursor(0, 1);
      lcd.print("  t2 secs: "); lcd.print(timer2duration);
      break;
    case 2:
      lcd.clear();
      lcd.print("  t1 secs: "); lcd.print(timer1duration);
      lcd.setCursor(0, 1);
      lcd.print("> t2 secs: "); lcd.print(timer2duration);
      break;
    case 3:
      lcd.clear();
      lcd.print("> t3 mins: "); lcd.print(timer3duration);
      lcd.setCursor(0, 1);
      lcd.print("  t4 mins: "); lcd.print(timer4duration);
      break;
    case 4:
      lcd.clear();
      lcd.print("  t3 mins: "); lcd.print(timer3duration);
      lcd.setCursor(0, 1);
      lcd.print("> t4 mins: "); lcd.print(timer4duration);
      break;
    case 5:
      lcd.clear();
      lcd.print("> backlight: ");  lcd.print(backlightDuty);
      lcd.setCursor(0, 1);
      lcd.print("  calibrate");
      break;
    case 6:
      lcd.clear();
      lcd.print("  backlight: ");  lcd.print(backlightDuty);
      lcd.setCursor(0, 1);
      lcd.print("> calibrate");
      break;
    case 7:
      lcd.clear();
      lcd.print("> factory reset");
      break;
    case 8:
      menu = 7;
      break;
  }
}

// this taken from other sketches.  it only works
// with specific arduino lcd sheilds that return
// button presses as various analog values on the
// appropriate pin.  the dfrobot lcd sheild is a good
// example.

int read_LCD_buttons(){
 adc_key_in = analogRead(lcd_buttons_pin);      // read the value from the sensor
 // "..We make this the 1st option for speed reasons since it will be the most likely result
 if (adc_key_in > 1000) return btnNONE;
 // "..my buttons when read are centered at these valies: 0, 144, 329, 504, 741
 // "..we add approx 50 to those values and check to see if we are close
 if (adc_key_in < 50)   return btnRIGHT;
 if (adc_key_in < 195)  return btnUP;
 if (adc_key_in < 380)  return btnDOWN;
 if (adc_key_in < 555)  return btnLEFT;
 if (adc_key_in < 790)  return btnSELECT;

 return btnNONE;  // when all others fail, return this...
}

// this is used to determine appropriate values
// to trigger the bell.  two determining factors are
// pwm duty cycle and duration.  the default values
// are 100% duty cycle for 30ms (see factory_reset())

void calibrate_bell(){
   while (true){
    // update display
     lcd.clear();
     lcd.setCursor(0,0);
     lcd.print("duration:  ");  lcd.println(dingerDuration);
     lcd.setCursor(0,1);
     lcd.print("dutyCycle: ");  lcd.println(dingerDutyCycle);
     delay(150);

     // detect input
     lcd_key = read_LCD_buttons();
     switch (lcd_key){
       case btnUP:{   dingerDuration++; break;}
       case btnDOWN:{ dingerDuration--; break;}
       case btnLEFT:{ dingerDutyCycle--; break;}
       case btnRIGHT:{
        if(dingerDutyCycle >= 100){dingerDutyCycle = 100;
          }else{dingerDutyCycle++;}
        break;}
       case btnSELECT:{
           ding(dingerDutyCycle,dingerDuration);
           delay(1000);
           //write values to EEPROM
           EEPROM.write(dingerDurationEEPROM, dingerDuration);
           EEPROM.write(dingerDutyCycleEEPROM, dingerDutyCycle);
           break;}
       }
   }
}

// actually ring the bell.  this is called by
// calibrate() and meditate() and others.

void ding(int dingerDutyCycle, int dingerDuration){
  lcd.clear(); lcd.setCursor(0,0);
  lcd.print("      ding");

  //convert duty cycle to pwmoutput
  int dingerPwmOutput = map(dingerDutyCycle, 0, 100, 0, 254);

  analogWrite(outputBenable, dingerPwmOutput); // Send PWM signal to L298N Enable pin
  // on
  digitalWrite(outputBin1, HIGH);
  digitalWrite(outputBin2, LOW);
  // delay
  delay(dingerDuration);
  // off
  digitalWrite(outputBin1, LOW);
  digitalWrite(outputBin2, LOW);
}

// init pin modes, init lcd and serial etc.
// also run mu to display the minimal screen.
// note that when setup finishes, the main loop()
// begins, and that repeately polls for input

void setup() {
  // init LCD etc
  Serial.begin(9600);
  lcd.begin(16, 2);

  // set pin modes
  pinMode(outputBenable, OUTPUT);
  pinMode(outputBin1, OUTPUT);
  pinMode(outputBin2, OUTPUT);
  pinMode(backlightPin, OUTPUT);
  pinMode(lcd_buttons_pin, INPUT);

  // set brightness
  setBrightness(backlightDuty);

  // welcome screen
  menu = 2;
  mu();
}

// any time the menu is X then increment the value
// according to X.  the variable "menu" is used everywhere
// and it controls what is displayed and what the 
// buttons do.  see updateMenu().
// the same logic is used for decreaseValue() and select().

void increaseValue() {
  switch (menu) {
    case 1:
      timer1duration+=2;
      EEPROM.write(timer1DurationEEPROM, timer1duration);
      break;
    case 2:
      timer2duration+=2;
      EEPROM.write(timer2DurationEEPROM, timer2duration);
      break;
    case 3:
      timer3duration++;
      EEPROM.write(timer3DurationEEPROM, timer3duration);
      break;
    case 4:
      timer4duration++;
      EEPROM.write(timer4DurationEEPROM, timer4duration);
      break;
    case 5:
      backlightDuty+= 5;
      EEPROM.write(backlightDutyEEPROM,backlightDuty);
      setBrightness(backlightDuty);
      break;
  }
}

// sends the appropriate PWM signal to the backlightPin.
// note that the storing of the value to the EEPROM
// is handled by the calling function.

void setBrightness(int backlightDuty) {
  int pwmValue = map(backlightDuty,0,100,0,255);
  analogWrite(backlightPin,pwmValue);
}

// as per increaseValue()

void decreaseValue() {
  switch (menu) {
    case 1:
      timer1duration-=2;
      EEPROM.write(timer1DurationEEPROM, timer1duration);
      break;
    case 2:
      timer2duration-=2;
      EEPROM.write(timer2DurationEEPROM, timer2duration);
      break;
    case 3:
      timer3duration--;
      EEPROM.write(timer3DurationEEPROM, timer3duration);
      break;
    case 4:
      timer4duration--;
      EEPROM.write(timer4DurationEEPROM, timer4duration);
      break;
    case 5:
      backlightDuty-= 5;
      EEPROM.write(backlightDutyEEPROM,backlightDuty);
      setBrightness(backlightDuty);
      break;
  }
}

// a weird special function i googled.  allows the 
// arduino to reset itself via software.  

void(* resetFunc) (void) = 0;

// restore all EEPROM values to these hardcoded ones.
// useful for initilising a new device, or if the kids
// have been playing with the timer etc.

void factory_reset() {
  for (int i = 0; i < 3; i++) {
    lcd.setCursor(0,0);  lcd.print("   resetting!   ");
    delay(600);
    lcd.clear();
    delay(600);
  }

  EEPROM.write(timer1DurationEEPROM, 16);
  EEPROM.write(timer2DurationEEPROM, 60);
  EEPROM.write(timer3DurationEEPROM, 10);
  EEPROM.write(timer4DurationEEPROM, 0);
  EEPROM.write(backlightDutyEEPROM, 60);
  EEPROM.write(dingerDurationEEPROM, 30);
  EEPROM.write(dingerDutyCycleEEPROM, 100);

  delay(2000);
  resetFunc();
}

  // the idea here is show mu, nice and clean
  // hitting buttons will have other stuff
  // happen, but for now, nice.  clean. like space..

void mu(){
  lcd.clear();
  lcd.setCursor(0,0);  lcd.print("       mu");
}

// use all the variables previously defined 
// or read from EEPROM and ring the bell at the 
// appropriate intervals.

void meditate(){
  // timer1
  if(timer1duration > 0){
    for (int i = timer1duration; i > 0; i--) {
      lcd.clear(); lcd.setCursor(0,0);
      lcd.print("       "); lcd.print(i);
      delay(1000);
    }
    ding(dingerDutyCycle,dingerDuration);
  }

  // timer2
  if(timer2duration > 0){
    for (int i = timer2duration; i > 0; i--) {
      lcd.clear(); lcd.setCursor(0,0);
      lcd.print("       "); lcd.print(i);
      delay(1000);
    }
    ding(dingerDutyCycle,dingerDuration);
  }

  // timer3
  if(timer3duration > 0){
    for (int i = timer3duration * 60; i > 0; i--) {
      lcd.clear(); lcd.setCursor(0,0);
      lcd.print("       "); lcd.print(i);
      delay(1000);
    }
    ding(dingerDutyCycle,dingerDuration);
  }

  // timer4
  if(timer4duration > 0){
    for (int i = timer4duration * 60; i > 0; i--) {
      lcd.clear(); lcd.setCursor(0,0);
      lcd.print("       "); lcd.print(i);
      delay(1000);
    }
    ding(dingerDutyCycle,dingerDuration);
  }

  // finish
  ding(dingerDutyCycle,dingerDuration);
  delay(7000);
  ding(dingerDutyCycle,dingerDuration);

  // finished
  lcd.clear(); lcd.setCursor(0,0);
  lcd.print("    finished!");
  delay(5000);

  // prepare for next session
  mu();  
}

// as per increaseValue()

void select(){
  switch (menu){
    case 1:{ meditate(); break;}
    case 2:{ meditate(); break;}
    case 3:{ meditate(); break;}
    case 4:{ meditate(); break;}
    case 5:{ meditate(); break;}
    case 6:{ calibrate_bell(); break; }
    case 7:{ factory_reset(); break; }
  }
}

// this repeately polls for user input.  which is
// almost always except if we're in the middle
// of meditate() which just has a series of
// delay() functions

void loop() {
  lcd_key = read_LCD_buttons();
  switch (lcd_key){
     case btnUP:{  
       menu--;
       updateMenu();
       delay(menu_delay);
       break;
      }
     case btnDOWN:{ 
       menu++;
       updateMenu();
       delay(menu_delay);
       break;
      }
     case btnLEFT:{
       decreaseValue();
       updateMenu();
       delay(menu_delay);
       break;
      }
     case btnRIGHT:{ 
       increaseValue();
       updateMenu();
       delay(menu_delay);
       break;
     }
     case btnSELECT:{ 
        select();
        break;
       }
     }
}
