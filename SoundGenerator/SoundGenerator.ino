/*
  AnalogReadSerial
  Reads an analog input on pin 0, prints the result to the serial monitor.
  Attach the center pin of a potentiometer to pin A0, and the outside pins to +5V and ground.
 
 This example code is in the public domain.
 */

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
}

// the loop routine runs over and over again forever:
void loop() {
  int C_bottom = 0;

  
  // read the input on analog pin 0:
  int V_ceiling = map(analogRead(A3),0,1023,C_bottom,15000);
  int V_StartFreq = map(analogRead(A1),0,1023,C_bottom,V_ceiling);
  int V_EndFreq = map(analogRead(A2),0,1023,C_bottom,V_ceiling);
  int V_Randomosity = map(analogRead(A4),0,1023,0, 20);
  int V_Duration = map(analogRead(A5),0,1023,0,4000);
  int V_tempo = map(analogRead(A0),0,1023,0,200);

  // print out the value you read:
  //Serial.print(" startfreq: ");  Serial.print(V_StartFreq);
  //Serial.print(" endfreq: ");  Serial.print(V_EndFreq);
  //Serial.print(" ceiling:"); Serial.println(V_ceiling);
  //Serial.print(" randomosity: ");  Serial.print(V_Randomosity); 
  //Serial.print(" duration:"); Serial.print(V_Duration);
  //Serial.print(" tempo:"); Serial.print(V_tempo);
    
  int V_PlayLen=0;
  int V_PlayFreq=0;
  
  // determine if the freq is ascending or descending
  if(V_StartFreq<V_EndFreq){ //ascending
      int V_FrequencyRange=V_EndFreq-V_StartFreq;
      
      for (int V_PlayFreq=V_StartFreq; V_PlayFreq < V_EndFreq; V_PlayFreq++){
        // so if V_FrequencyRange is large, the sound takes ages to play.
        // if V_duration is less than the V_FrequencyRange then we need to throw away some freqs
        // in order to make it take less time to play.  we can do that by simply skipping a number of
        // iterations of this for loop.  the difference between V_FrequencyRange and V_duration
        // is the number of foor loop iterations we need to skip
        /*V_Duration = map(analogRead(A5),0,1023,0,4000);
        int V_Skips=V_FrequencyRange-V_Duration;
        Serial.print(" V_Skips: "); Serial.print(V_Skips);  Serial.print(" V_PlayFreq: "); Serial.println(V_PlayFreq); 
        if (V_PlayFreq % V_Skips == 0) { Serial.print(" skipping ");break; }*/
        int V_tempo = map(analogRead(A0),0,1023,2,1000);
        if(V_Randomosity>1){ V_PlayFreq=random(V_PlayFreq-V_Randomosity,V_PlayFreq+V_Randomosity); }
        if(V_tempo>1){ delay(V_tempo); }  
        tone(8,V_PlayFreq);  
      }   
} else { // descending
      for (int V_PlayFreq=V_StartFreq; V_PlayFreq > V_EndFreq; V_PlayFreq--){
        int V_tempo = map(analogRead(A0),0,1023,2,1000);
        if(V_Randomosity>1){ V_PlayFreq=random(V_PlayFreq-V_Randomosity,V_PlayFreq+V_Randomosity); }
        if(V_tempo>1){ delay(V_tempo); }
        tone(8,V_PlayFreq);  
      } 
    } 
}
