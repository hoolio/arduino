// Example for using Adafruit Bluefruit App to send/receive controller data

// BLE Serial code Copyright (c) Sandeep Mistry. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

#include <Adafruit_Microbit.h>

Adafruit_Microbit microbit;

// function prototypes over in packetparser.cpp
uint8_t readPacket(Stream *ble, uint16_t timeout);
float parsefloat(uint8_t *buffer);
void printHex(const uint8_t * data, const uint32_t numBytes);
// the packet buffer
extern uint8_t packetbuffer[];

#define BLE_READPACKET_TIMEOUT 1000

const int leftFord = 1; //green
const int leftBack = 0; //blue
const int rightFord = 2; //white
const int rightBack = 11; //orange
const int otherA = 15; //yellow
const int otherB = 8; //red

void setup() {
  Serial.begin(9600);

  Serial.println("Controller ready!");

  // custom services and characteristics can be added as well
  microbit.BTLESerial.begin();
  microbit.BTLESerial.setLocalName("microbit");

  // Start LED matrix driver after radio (required)
  microbit.begin();

  //set pin modes
  pinMode(leftFord, OUTPUT); digitalWrite(leftFord, LOW);
  pinMode(leftBack, OUTPUT); digitalWrite(leftBack, LOW);
  pinMode(rightFord, OUTPUT); digitalWrite(rightFord, LOW);
  pinMode(rightBack, OUTPUT); digitalWrite(rightBack, LOW);
  pinMode(otherA, OUTPUT); digitalWrite(otherA, LOW);
  pinMode(otherB, OUTPUT); digitalWrite(otherB, LOW);
}

void loop(void)
{
  /* Wait for new data to arrive */
  uint8_t len = readPacket(&(microbit.BTLESerial), BLE_READPACKET_TIMEOUT);
  if (len == 0) return;

  /* Got a packet! */
  // printHex(packetbuffer, len);

  // Buttons
  if (packetbuffer[1] == 'B') {
    uint8_t buttnum = packetbuffer[2] - '0';
    boolean pressed = packetbuffer[3] - '0';
    Serial.print ("Button "); Serial.print(buttnum);
    if (pressed) {
      Serial.println(" pressed");
      if (buttnum == 1) digitalWrite(leftFord, HIGH);
      if (buttnum == 3) digitalWrite(leftBack, HIGH);
      if (buttnum == 2) digitalWrite(rightFord, HIGH);
      if (buttnum == 4) digitalWrite(rightBack, HIGH);
      if (buttnum == 5) digitalWrite(otherA, HIGH);
      if (buttnum == 6) digitalWrite(otherB, HIGH);
    } else {
      Serial.println(" released");
      if (buttnum == 1) digitalWrite(leftFord, LOW);
      if (buttnum == 3)digitalWrite(leftBack, LOW);
      if (buttnum == 2) digitalWrite(rightFord, LOW);
      if (buttnum == 4) digitalWrite(rightBack, LOW);
      if (buttnum == 5) digitalWrite(otherA, LOW);
      if (buttnum == 6) digitalWrite(otherB, LOW);
    }
  }
}
