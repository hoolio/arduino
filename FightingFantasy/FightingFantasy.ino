/*
  Xmas Countdown
  Julius Roberts 2015
  Released under the terms of the GPLv3  
  
  the hardware for this programme is entirely custom.
  there exists for it, no schematic at all.  this is bespoke.
 */
 
// pin assignments
int annodes0 = 2;int annodes1 = 3;int annodes2 = 4;int annodes3 = 5;int annodes4 = 6;
//pin 7 is spare
int cathodes0 = 8;int cathodes1 = 9;int cathodes2 = 10;int cathodes3 = 11;int cathodes4 = 12;
int IndicatorLED = 13;
int InputPot = A0;
int C_ButtonPin = 18; //ie analog4, in use as a digital pin
int C_SpeakerPin = 19; //ie analog5, in use as a digital pin

// global variables
int cursor = 10;
int buttonState = LOW;

// play variables
int mySkill = 0;
int enemySkill = 0;

// the setup routine runs once when you press reset:
void setup() {                
  // initialize the pins
  pinMode(annodes0, OUTPUT); pinMode(annodes1, OUTPUT); pinMode(annodes2, OUTPUT); pinMode(annodes3, OUTPUT); pinMode(annodes4, OUTPUT);
  pinMode(cathodes0, OUTPUT); pinMode(cathodes1, OUTPUT); pinMode(cathodes2, OUTPUT); pinMode(cathodes3, OUTPUT); pinMode(cathodes4, OUTPUT);
  pinMode(IndicatorLED, OUTPUT); 
  pinMode(C_ButtonPin, INPUT);  pinMode(InputPot, INPUT);
   
  //Serial.begin(9600);

  // if analog input pin 0 is unconnected, random analog
  // noise will cause the call to randomSeed() to generate
  // different seed numbers each time the sketch runs.
  // randomSeed() will then shuffle the random function.
  randomSeed(analogRead(0));
}

void loop() {


	//determine value for mySkill (if it's not set yet)
	while ((mySkill == 0) && (buttonState == LOW)) {
		int userSelectedNumberRaw = analogRead(InputPot); 
		cursor = map(userSelectedNumberRaw, 0, 1023, 12, 8);
		if (mySkill != cursor) { 
		  sound(1);  //grunt
		  mySkill = cursor;
		}
		displayNumber(mySkill,1);
		buttonState = digitalRead(C_ButtonPin); 				
	}

	/*if ((mySkill > 0) && (enemySkill = 0)){
		displayNumber(mySkill,10);
		displayNumber(enemySkill,10);	
	}*/

	//if ((mySkill > 0) && ((buttonState == HIGH)) {


	//int DoubleDieRoll = random(2,12);
	//int SingleDieRoll = random(1,6);



	/*
	if (buttonState == HIGH){
		//determine value for enemySkill
		int userSelectedNumberRaw = analogRead(InputPot); 
		cursor = map(userSelectedNumberRaw, 0, 1023, 12, 8);
		if (enemySkill != cursor) { 
		  sound(1);  //grunt
		  enemySkill = cursor;
		}
		displayNumber(enemySkill,5);
	}*/
	
	/*if (buttonState == HIGH) {
		sound(1);
		delay(200);

		for(int i=2;i<=DoubleDieRoll;i++) { 
		  sound(1); //grunt
		  displayNumber(i,i * 1.5);
		}
		displayNumber(DoubleDieRoll,90);
		//delay(8000);
	}*/


	/*	
  ////  we first sample the pot and determine which Number to display
  int userSelectedNumberRaw = analogRead(InputPot); 
  int userSelectedNumber = map(userSelectedNumberRaw, 0, 1023, 19, 0);
    
  //// then we check to see if the number changed 
  if (userSelectedNumber != cursor) { 
    sound(1);  //grunt
    cursor = userSelectedNumber;
    // else we display the current number.  
  } else {
    displayNumber(userSelectedNumber,1);
    // sample the button to instigate countdown to song!!
    int buttonState = digitalRead(C_ButtonPin); 
    if (buttonState == HIGH) { 
      // sound(1); // select item noise?
      // for loop to display and decrement displayNumber
      for(int i=userSelectedNumber;i>-1;i--) { 
        sound(1); //grunt
        displayNumber(i,i * 1.5);
      }
      //// then finally play xmas song.
      sound(2); //xmas


    } // end if button HIGH

  } // end else.  
  */
}

int displayNumber(int chosenNumber, int loops) {
  // chosenNumber = the Number to display on the matrix

  // loops = the number of times we iterate through numbersArray[].  
  //   the higher the loops, the longer a Number will appear on the screen.
  
  // first define all the possible numbers we could display
  //   columns is the total possible number of LEDs in a given Number
  const int columns=18;  
  
  // rows = is the total number of Numbers we understand.  ideally 19.
  //  more than 19 rows will require us to display the number 20 which
  //  may be quite hard on a 5x5 LED array.   this number must match
  //  the number of rows you define in the array below.
  const int rows=20;
  
  // then for each row (starting with row zero) map out the LEDs
  //   required to display that Number.  We conveniently store the map
  //   for Number 0 in numbersArray[0], Number 1 in numbersArray[1] etc.
  //   -1 means ignore, don't display anything.  
  const int numbersArray[rows][columns] = {
    {16,11, 6, 7, 8, 9,10,17,18,19,20,15,-1,-1,-1,-1,-1,-1}, // 0
    {17,11,12,13,14,15,20,10,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1}, //1
    {17,11, 7,13,19,20,15,10,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1}, //2 yuk
    {16,11, 7,13, 9,15,20,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1}, //3 
    { 6, 7, 8, 9,10,14,19,18,12, 6,-1,-1,-1,-1,-1,-1,-1,-1}, //4
    { 6,11,16,17,18,13, 8, 9,10,15,20,-1,-1,-1,-1,-1,-1,-1}, //5
    { 6,11,16,17,18,13, 8, 9,10,15,20,19,-1,-1,-1,-1,-1,-1}, //6
    {16,11, 6, 7, 8, 9,10,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1}, //7
    { 6,11,16,17,18,13, 8, 9,10,15,20,19, 7,-1,-1,-1,-1,-1}, //8
    {16,11, 6, 7, 8, 9,10,17,18,13,-1,-1,-1,-1,-1,-1,-1,-1}, //9
    {21,22,23,24,25,11, 6, 1, 2, 3, 4, 5,10,15,14,13,12,-1}, //10
    {16,17,18,19,20, 6, 7, 8, 9,10,-1,-1,-1,-1,-1,-1,-1,-1}, //11
    {21,22,23,24,25,12, 6, 2, 8,14,15,10, 5,-1,-1,-1,-1,-1}, //12 yuk
    {21,22,23,24,25,11, 6, 2, 8, 4,10,15,-1,-1,-1,-1,-1,-1}, //13
    {21,22,23,24,25, 1, 2, 3, 4, 5, 7,13,-1,14, 9,-1,-1,-1}, //14
    {21,22,23,24,25,11, 6, 1,12,13, 8, 3, 4, 5,10,15,-1,-1}, //15 
    {21,22,23,24,25, 1,-1, 3, 4, 5,11,12,13,14,15, 6, 8,10}, //16   
    {21,22,23,24,25, 1, 2, 3, 4, 5, 6,11,-1,-1,-1,-1,-1,-1}, //17 
    {21,22,23,24,25, 1, 2, 3, 4, 5,11,12,13,14,15, 6, 8,10}, //18
    {21,22,23,24,25, 1, 2, 3, 4, 5,11,12,13,-1,-1, 6, 8,-1}  //19
  };

  // NOTE; if you have forgotten about how flicker free multiplexing works,
  // have a read of the primer at the start of basicMultiplexing();

  // in order to minimise apparent flicker, we need to illuminate a
  // given set of LEDs at as higher frequency as possible.  the more 
  // processing/delays involved, the more apparent the flicker.
  // illuminating a given LED takes processing time, 
  // thusly the more LEDs lit, the more flicker. 
   
  // now we iterate N times through the chosen numbersArray[] and illuminate each led.   
  
  // ondelayms = LED on duration in microseconds.  higher values make LEDs "brighter" but introduces more flicker.
  //  flicker is a product of the fact that we rely on persistence of vision to make all those LEDs appear as though
  //  they are on at the same time, when in fact we just illuminate them all indivdually in turn, very quickly.  the
  //  duration of which each led is ON affects how bright it is.  but the longer we wait, the more chance that we
  //  will notice a delay before the next light comes on; hence flicker.  try it and see. 
  // an ideal value will depend on the number of LEDs in the given Number.  a Number comprising more LEDs will take
  //  longer to process, so we might choose a lower value for ondelayms because there are more LEDs to deal with.  
  // Assuming we will have several Numbers to display, we have to compromise and choose a good overall value.
  //  loop through all Numbers, increase ondelayms until you see flicker, then decrease it until it goes away.
  //  further more, and finally, differing ambient light levels will effect your perception of flicker, so do your
  //  loop testing in differing light levels too.
  const int ondelayms = 1200; 

  // finally loop through the instructions[] derived from numbersArray[] derived from int chosenNumber ;)
  for (int loopcounter = 0; loopcounter < loops ; loopcounter ++) {
      // iterate through the array, starting at element 0
      for (int i = 0; i < columns; i++){
        lightled(numbersArray[chosenNumber][i],ondelayms);
      }
   }
}

void  basicMultiplexing(){
  // in order to minimise apparent flicker, we need to illuminate a
  // given set of LEDs at as higher frequency as possible.  the more 
  // processing/delays involved, the more apparent the flicker.
  // illuminating a given LED takes processing time, 
  // thusly the more LEDs lit, the more flicker.  
  //
  // the two for loops here differ only because the top one senses 
  // the input once.  the other does it every iteration of the for loop.
  // given that input sensing adds processing time,
  // the second example runs much more slowly, and thusly 
  // results in the increase in apparent flicker.
  //
  // comment out the unused section.
  //
  // // sensing input once only;
  int ondelayX = analogRead(InputPot);   
  int ondelay = ondelayX;
  for (int i = 1; i <= 25; i++){lightled(i,ondelay);}
  
  // sensing input repeatedly == more delay == more apparent flicker.
  /*
  for (int i = 1; i <= 25; i++){
  int ondelayX = analogRead(InputPot); 
  int ondelay = ondelayX;  
  lightled(i,ondelay);
  } 
  */
}

void software_Reset() { 
   // Restarts program from beginning but does not reset the peripherals and registers
   // taken from http://www.arduino.cc/cgi-bin/yabb2/YaBB.pl?num=1246541553
   asm volatile ("  jmp 0");  
}

void sound(int soundeffect) {
   switch( soundeffect ) {
      case 1: {//grunt
         tone(C_SpeakerPin, 17, 250);
      } break;
      case 2: {// xmas!!
        // the song to play at the truimphant conclusion :)
        const char *song = "Happy Birthday Song:d=4,o=5,b=125:16c,32p,32c,32p,8d,32p,8c,32p,8f,32p,e,16p,16c,32p,32c,32p,8d,32p,8c,32p,8g,32p,f,8p,16c,32p,32c,32p,8c6,32p,8a,32p,8f,32p,8e,32p,8d,32p,16a#,32p,32a#,32p,8a,32p,8f,32p,8g,32p,f";
        //const char *song = "JingleBells:d=4,o=5,b=355:2b,2b,2b,2p,2b,2b,2b,2p,2b,2d6,2g,2a,1b,2c6,2c6,1c6,2b,2b,1b,2b,2a,2a,2b,1a,2d6";
        //const char *song = "DingDongMerrilyonHigh:d=4,o=5,b=230:g6,g6,8a6,8g6,8f#6,8e6,2d.6,d6,e6,g6,g6,f#6,2g6,2g6,g6,g6,8a6,8g6,8f#6,8e6,2d.6,d6,e6,g6,g6,f#6,2g6,2g6,d.7,8c7,8b6,8c7,8d7,8b6,c.7,8b6,8a6,8b6,8c7,8a6,b.6,8a6,8g6,8a6,8b6,8g6,a.6,8g6,f#6,d6,e6,g6,g6,f#6,2g6,2g6";
        //const char *song = "FirstDayofChristmas: d=4,o=5,b=160: 8c, 8c, f, 8f, 8f, f, 8f, 8f, 8g, 8a, 8a#, 8g, a., 8p, c, 8g, 8a, 8a#, 8g, 8c, 8c, 8g, 8a, 8a#, 8g, c, d, c., 8p, 8c, 8a#, 8a, 8g, f, 8g, 8a, a#, d, d, c, 8f, 8g, 8a, 8a#, a, g, 2f";
        //const char *song = "Onthe12thDayofChristmas:d=8,o=5,b=150:d,d,4g,g,g,4g,g,g,a,b,c6,a,4b.,p,4d6,a,b,c6,a,d6,d6,a,b,c6,a,4d6,4e6,4d.6,p,d6,c6,b,a,4g,a,b,4c6,4e,4e,4d,g,a,b,c6,4b,4a,2g";
        //const char *song = "WeWishYouAMerryChristmas:d=8,o=5,b=140:4d,4g,g,a,g,f#,4e,4c,4e,4a,a,b,a,g,4f#,4d,4f#,4b,b,c6,b,a,4g,4e,4d,4e,4a,4f#,2g";    
        //const char *song = "aadams:d=4,o=5,b=160:8c,f,8a,f,8c,b4,2g,8f,e,8g,e,8e4,a4,2f,8c,f,8a,f,8c,b4,2g,8f,e,8c,d,8e,1f,8c,8d,8e,8f,1p,8d,8e,8f#,8g,1p,8d,8e,8f#,8g,p,8d,8e,8f#,8g,p,8c,8d,8e,8f";
        play_rtttl(song);   
      } break;
   }
}

void lightled(int LEDid, int ondelay) {
  //Serial.print(" led = "); Serial.print(LEDid); Serial.print("  ondelay = "); Serial.println(ondelay);
  switch (LEDid) {
    case 0:
      //all off
      digitalWrite(cathodes0, LOW); digitalWrite(annodes0, LOW); digitalWrite(cathodes1, LOW); digitalWrite(annodes1, LOW);
      digitalWrite(cathodes2, LOW); digitalWrite(annodes2, LOW); digitalWrite(cathodes3, LOW); digitalWrite(annodes3, LOW);
      digitalWrite(cathodes4, LOW); digitalWrite(annodes4, LOW); break;
    case 1: //c0a0
      digitalWrite(cathodes0, HIGH); digitalWrite(annodes0, HIGH); break;
    case 2: //c0a1
      digitalWrite(cathodes0, HIGH); digitalWrite(annodes1, HIGH); break;
    case 3: //c0a2
      digitalWrite(cathodes0, HIGH); digitalWrite(annodes2, HIGH); break;
    case 4: //c0a3
      digitalWrite(cathodes0, HIGH); digitalWrite(annodes3, HIGH); break;      
    case 5: //c0a4
      digitalWrite(cathodes0, HIGH); digitalWrite(annodes4, HIGH); break;
    case 6: //c1a0
      digitalWrite(cathodes1, HIGH); digitalWrite(annodes0, HIGH); break;
    case 7: //c1a1
      digitalWrite(cathodes1, HIGH); digitalWrite(annodes1, HIGH); break;
    case 8: //c1a2
      digitalWrite(cathodes1, HIGH); digitalWrite(annodes2, HIGH); break;
    case 9: //c1a3
      digitalWrite(cathodes1, HIGH); digitalWrite(annodes3, HIGH); break;
    case 10: //c1a4
      digitalWrite(cathodes1, HIGH); digitalWrite(annodes4, HIGH); break;
    case 11: //c2a0
      digitalWrite(cathodes2, HIGH); digitalWrite(annodes0, HIGH); break;
    case 12: //c2a1
      digitalWrite(cathodes2, HIGH); digitalWrite(annodes1, HIGH); break;
    case 13: //c2a2
      digitalWrite(cathodes2, HIGH); digitalWrite(annodes2, HIGH); break;
    case 14: //c2a3
      digitalWrite(cathodes2, HIGH); digitalWrite(annodes3, HIGH); break;
    case 15: //c2a4
      digitalWrite(cathodes2, HIGH); digitalWrite(annodes4, HIGH); break;
    case 16: //c3a0
      digitalWrite(cathodes3, HIGH); digitalWrite(annodes0, HIGH); break;
    case 17: //c3a1
      digitalWrite(cathodes3, HIGH); digitalWrite(annodes1, HIGH); break;
    case 18: //c3a2
      digitalWrite(cathodes3, HIGH); digitalWrite(annodes2, HIGH); break;
    case 19: //c3a3
      digitalWrite(cathodes3, HIGH); digitalWrite(annodes3, HIGH); break;
    case 20: //c3a4
      digitalWrite(cathodes3, HIGH); digitalWrite(annodes4, HIGH); break;
    case 21: //c4a0
      digitalWrite(cathodes4, HIGH); digitalWrite(annodes0, HIGH); break;
    case 22: //c4a1
      digitalWrite(cathodes4, HIGH); digitalWrite(annodes1, HIGH); break;
    case 23: //c4a2
      digitalWrite(cathodes4, HIGH); digitalWrite(annodes2, HIGH); break;
    case 24: //c4a3
      digitalWrite(cathodes4, HIGH); digitalWrite(annodes3, HIGH); break;
    case 25: //c4a3
      digitalWrite(cathodes4, HIGH); digitalWrite(annodes4, HIGH); break;
    case 88: //do nothing.
      break;  
    case 100:
      //all on
      digitalWrite(cathodes0, HIGH); digitalWrite(annodes0, HIGH); digitalWrite(cathodes1, HIGH); digitalWrite(annodes1, HIGH);
      digitalWrite(cathodes2, HIGH); digitalWrite(annodes2, HIGH); digitalWrite(cathodes3, HIGH); digitalWrite(annodes3, HIGH);
      digitalWrite(cathodes4, HIGH); digitalWrite(annodes4, HIGH);    
      break;      
    default: // if nothing else matches, do the default
      //Serial.print("ERROR LEDid = "); Serial.println(LEDid); alloff(); allon(); delay(1500);
      break;
  }
  //delay a sec before resuming
  //delay(ondelay);  
  delayMicroseconds(ondelay);
  alloff(); 
}

void alloff(){
  // turn everything off for contrast
  digitalWrite(cathodes0, LOW);  digitalWrite(cathodes1, LOW);  digitalWrite(cathodes2, LOW);  digitalWrite(cathodes3, LOW);  digitalWrite(cathodes4, LOW);
  digitalWrite(annodes0, LOW);  digitalWrite(annodes1, LOW);  digitalWrite(annodes2, LOW);  digitalWrite(annodes3, LOW);  digitalWrite(annodes4, LOW); 
  digitalWrite(IndicatorLED, LOW);
}

void allon(){
  // turn everything off for contrast
  digitalWrite(cathodes0, HIGH);  digitalWrite(cathodes1, HIGH);  digitalWrite(cathodes2, HIGH);  digitalWrite(cathodes3, HIGH);  digitalWrite(cathodes4, HIGH);
  digitalWrite(annodes0, HIGH);  digitalWrite(annodes1, HIGH);  digitalWrite(annodes2, HIGH);  digitalWrite(annodes3, HIGH);  digitalWrite(annodes4, HIGH); 
  digitalWrite(IndicatorLED, HIGH);
}

void play_rtttl(const char *p) {
  // this function based on the LCA 2012 leo stick tunes collaborative effort at https://gist.github.com/1800871
  //  some more rttl songs at http://ez4mobile.com/nokiatone/rtttf.htm

  //char *song = "WeWishYouAMerryChristmas:d=8,o=5,b=140:4d,4g,g,a,g,f#,4e,4c,4e,4a,a,b,a,g,4f#,4d,4f#,4b,b,c6,b,a,4g,4e,4d,4e,4a,4f#,2g";
      
  #define isdigit(n) (n >= '0' && n <= '9')
   
  #define NOTE_B0 31
  #define NOTE_C1 33
  #define NOTE_CS1 35
  #define NOTE_D1 37
  #define NOTE_DS1 39
  #define NOTE_E1 41
  #define NOTE_F1 44
  #define NOTE_FS1 46
  #define NOTE_G1 49
  #define NOTE_GS1 52
  #define NOTE_A1 55
  #define NOTE_AS1 58
  #define NOTE_B1 62
  #define NOTE_C2 65
  #define NOTE_CS2 69
  #define NOTE_D2 73
  #define NOTE_DS2 78
  #define NOTE_E2 82
  #define NOTE_F2 87
  #define NOTE_FS2 93
  #define NOTE_G2 98
  #define NOTE_GS2 104
  #define NOTE_A2 110
  #define NOTE_AS2 117
  #define NOTE_B2 123
  #define NOTE_C3 131
  #define NOTE_CS3 139
  #define NOTE_D3 147
  #define NOTE_DS3 156
  #define NOTE_E3 165
  #define NOTE_F3 175
  #define NOTE_FS3 185
  #define NOTE_G3 196
  #define NOTE_GS3 208
  #define NOTE_A3 220
  #define NOTE_AS3 233
  #define NOTE_B3 247
  #define NOTE_C4 262
  #define NOTE_CS4 277
  #define NOTE_D4 294
  #define NOTE_DS4 311
  #define NOTE_E4 330
  #define NOTE_F4 349
  #define NOTE_FS4 370
  #define NOTE_G4 392
  #define NOTE_GS4 415
  #define NOTE_A4 440
  #define NOTE_AS4 466
  #define NOTE_B4 494
  #define NOTE_C5 523
  #define NOTE_CS5 554
  #define NOTE_D5 587
  #define NOTE_DS5 622
  #define NOTE_E5 659
  #define NOTE_F5 698
  #define NOTE_FS5 740
  #define NOTE_G5 784
  #define NOTE_GS5 831
  #define NOTE_A5 880
  #define NOTE_AS5 932
  #define NOTE_B5 988
  #define NOTE_C6 1047
  #define NOTE_CS6 1109
  #define NOTE_D6 1175
  #define NOTE_DS6 1245
  #define NOTE_E6 1319
  #define NOTE_F6 1397
  #define NOTE_FS6 1480
  #define NOTE_G6 1568
  #define NOTE_GS6 1661
  #define NOTE_A6 1760
  #define NOTE_AS6 1865
  #define NOTE_B6 1976
  #define NOTE_C7 2093
  #define NOTE_CS7 2217
  #define NOTE_D7 2349
  #define NOTE_DS7 2489
  #define NOTE_E7 2637
  #define NOTE_F7 2794
  #define NOTE_FS7 2960
  #define NOTE_G7 3136
  #define NOTE_GS7 3322
  #define NOTE_A7 3520
  #define NOTE_AS7 3729
  #define NOTE_B7 3951
  #define NOTE_C8 4186
  #define NOTE_CS8 4435
  #define NOTE_D8 4699
  #define NOTE_DS8 4978  
  #define OCTAVE_OFFSET 0
  
  int notes[] = { 0,
  NOTE_C4, NOTE_CS4, NOTE_D4, NOTE_DS4, NOTE_E4, NOTE_F4, NOTE_FS4, NOTE_G4, NOTE_GS4, NOTE_A4, NOTE_AS4, NOTE_B4,
  NOTE_C5, NOTE_CS5, NOTE_D5, NOTE_DS5, NOTE_E5, NOTE_F5, NOTE_FS5, NOTE_G5, NOTE_GS5, NOTE_A5, NOTE_AS5, NOTE_B5,
  NOTE_C6, NOTE_CS6, NOTE_D6, NOTE_DS6, NOTE_E6, NOTE_F6, NOTE_FS6, NOTE_G6, NOTE_GS6, NOTE_A6, NOTE_AS6, NOTE_B6,
  NOTE_C7, NOTE_CS7, NOTE_D7, NOTE_DS7, NOTE_E7, NOTE_F7, NOTE_FS7, NOTE_G7, NOTE_GS7, NOTE_A7, NOTE_AS7, NOTE_B7
  };
    // Absolutely no error checking in here
  
    byte default_dur = 4;
    byte default_oct = 6;
    int bpm = 63;
    int num;
    long wholenote;
    long duration;
    byte note;
    byte scale;
  
    // format: d=N,o=N,b=NNN:
    // find the start (skip name, etc)
  
    while(*p != ':') p++; // ignore name
    p++; // skip ':'
  
    // get default duration
    if(*p == 'd'){
      p++; p++; // skip "d="
      num = 0;
      while(isdigit(*p)){
        num = (num * 10) + (*p++ - '0');
      }
      if(num > 0) default_dur = num;
      p++; // skip comma
    }
  
  
    // get default octave
    if(*p == 'o'){
      p++; p++; // skip "o="
      num = *p++ - '0';
      if(num >= 3 && num <=7) default_oct = num;
      p++; // skip comma
    }
  
    // get BPM
    if(*p == 'b'){
      p++; p++; // skip "b="
      num = 0;
      while(isdigit(*p)){
        num = (num * 10) + (*p++ - '0');
      }
      bpm = num;
      p++; // skip colon
    }
    
    // BPM usually expresses the number of quarter notes per minute
    wholenote = (60 * 1000L / bpm) * 4; // this is the time for whole note (in milliseconds)
    
    // now begin note loop
    while(*p){
      // first, get note duration, if available
      num = 0;
      while(isdigit(*p)){
        num = (num * 10) + (*p++ - '0');
      }
      
      if(num) duration = wholenote / num;
      else duration = wholenote / default_dur; // we will need to check if we are a dotted note after
  
      // now get the note
      note = 0;
      
      switch(*p) {
        case 'c':
          note = 1;
          break;
        case 'd':
          note = 3;
          break;
        case 'e':
          note = 5;
          break;
        case 'f':
          note = 6;
          break;
        case 'g':
          note = 8;
          break;
        case 'a':
          note = 10;
          break;
        case 'b':
          note = 12;
          break;
        case 'p':
        default:
          note = 0;
      }
      p++;
  
      // now, get optional '#' sharp
      if(*p == '#'){
        note++;
        p++;
      }
  
      // now, get optional '.' dotted note
      if(*p == '.'){
        duration += duration/2;
        p++;
      }
    
      // now, get scale
      if(isdigit(*p)){
        scale = *p - '0';
        p++;
      }
      else{
        scale = default_oct;
      }
  
      scale += OCTAVE_OFFSET;
  
      if(*p == ',')
        p++; // skip comma for next note (or we may be at the end)
  
      // now play the note
  
      if(note){
        digitalWrite(C_SpeakerPin, HIGH);
        int danFreq;
        float danDur;
        danFreq = notes[(scale - 4) * 12 + note];
        danDur = 1000000 / danFreq;
        unsigned long start = millis();
        while (millis() - start <= duration) {
            digitalWrite(C_SpeakerPin, HIGH);
            delayMicroseconds(danDur);
            digitalWrite(C_SpeakerPin, LOW);
            delayMicroseconds(danDur);
        }
        digitalWrite(C_SpeakerPin, LOW);
      }
      else{
        delay(duration);
      }
    }
}