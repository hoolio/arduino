#include <LiquidCrystal.h>

LiquidCrystal lcd( 8, 9, 4, 5, 6, 7 );

int counter = 0;

void setup()
{
  lcd.begin(16, 2);
  lcd.print("Starting..");
  delay(1000);
}

void loop()
{
  lcd.setCursor( 0, 0 );   //top left
  lcd.print("counter = ");
  lcd.println(counter);
  
  delay(300);
  counter = counter + 1;
  
}
