/*
  AnalogReadSerial
  Reads an analog input on pin 0, prints the result to the serial monitor.
  Attach the center pin of a potentiometer to pin A0, and the outside pins to +5V and ground.
 
 This example code is in the public domain.
 */

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
}

// the loop routine runs over and over again forever:
void loop() {
  int bottom = 0;
  int ceiling = map(analogRead(A5),0,1023,bottom,15000);
  int tempo = map(analogRead(A0),0,1023,2,1000);
  
  // read the input on analog pin 0:
  int sensorValue1 = map(analogRead(A1),0,1023,bottom,ceiling);
  int sensorValue2 = map(analogRead(A2),0,1023,bottom,ceiling);
  int sensorValue3 = map(analogRead(A3),0,1023,bottom,ceiling);
  int sensorValue4 = map(analogRead(A4),0,1023,bottom,ceiling);

  // print out the value you read:
  Serial.print("tempo:"); Serial.print(tempo); Serial.print(" ");
  Serial.print("ceiling:"); Serial.print(ceiling); Serial.print("   ");
  Serial.print(sensorValue1);  Serial.print("   ");
  Serial.print(sensorValue2);  Serial.print("   ");
  Serial.print(sensorValue3);  Serial.print("   ");
  Serial.println(sensorValue4); 

  //delay(100);        // delay in between reads for stability
  tone(8,sensorValue1,tempo);  delay(tempo);
  tone(8,sensorValue2,tempo);  delay(tempo);
  tone(8,sensorValue3,tempo);  delay(tempo);
  tone(8,sensorValue4,tempo);  delay(tempo);
}
