const int dividerAnalogInputPin = A0;

float getVoltage(float rawRead){
  // this corrects the 10-bit rawRead to a lowVoltage (0-5v)
  float lowVoltage = mapf(rawRead,0,1023,0,5);
  
  // using our divider, 4.062v (vOut) should equate to 12v (vSupply)
  // where 4.062 was calculated using measured resister values and
  // http://www.ohmslawcalculator.com/voltage-divider-calculator
  //
  //float highVoltage = mapf(lowVoltage,0,4.062,0,12);

  // or we plug those same values into our own formula calcVOut():
  float R1 = 2390; // ohms (actual measured)
  float R2 = 1500; // ohms (actual measured)
  float vSupply = 13; // volts  
  float vOut = calcVOut(vSupply,R1, R2);
  
  float highVoltage = mapf(lowVoltage,0,vOut,0,vSupply);
  
  return highVoltage;
}

float calcVOut(float vSupply, float R1, float R2){
  // vOut = vSupply * (R2 / (R1 + R2))
  // vSupply = vOut / (R2 / (R1 + R2))
  float vOut = vSupply * (R2 / (R1 + R2));

  return vOut;
}

// the standard map() function returns int values only :(
float mapf(float x, float in_min, float in_max, float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

float generateAverageRawRead(int numberOfSamples){
  float totalRawReads = 0;
  for (int i = 0; i < numberOfSamples; i++) {
    totalRawReads = totalRawReads + analogRead(dividerAnalogInputPin);;
    delay(20);
  }
  float averageRawRead = totalRawReads / numberOfSamples;

  return averageRawRead;
}

void setup() {
  Serial.begin(9600);
}

void loop() {
  //// initial method
  //  float rawRead = analogRead(dividerAnalogInputPin);
  //  Serial.print(rawRead);
  //  Serial.print(", ");
  //  Serial.println(getVoltage(rawRead));
  //  delay(500);

  ////  averaging method
  const int numberOfSamples = 30;
  Serial.println(getVoltage(generateAverageRawRead(numberOfSamples)));
}
