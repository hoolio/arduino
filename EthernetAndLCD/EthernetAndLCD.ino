#include <LiquidCrystal.h>
#include <SPI.h>
#include <Ethernet.h>

LiquidCrystal lcd( 8, 9, 4, 5, 6, 7 );
//Ethernet shield attached to pins 10, 11, 12, 13

// Enter a MAC address for your controller below.
// Newer Ethernet shields have a MAC address printed on a sticker on the shield
byte mac[] = { 0x00, 0xAA, 0xBB, 0xCC, 0xDE, 0x02 };
// Initialize the Ethernet client library
// with the IP address and port of the server 
// that you want to connect to (port 80 is default for HTTP):

EthernetClient client;

void setup()
{
  // allow some time (50 ms) after powerup/start, so the 
  // Wiznet W5100 Reset IC to release and come out of reset.
  delay( 50 );   

  // start the serial and ethernet libraries
  lcd.begin(16, 2);
  //Serial.begin(9600);
  
  // start the Ethernet connection:
  if (Ethernet.begin(mac) == 0) {
    lcd.print("DHCP noworky :( "); while(1){}; // ie die.
  } else {
    printIp2LCD();     // print your local IP address:
    delay(5000);
  }
}

void loop()
{
  //lcd.setCursor( 0, 0 );   //top left
  //lcd.print("counter = ");
}

void printIp2LCD(){
  lcd.setCursor( 0, 0 );
  lcd.print("My IP address: ");
  lcd.setCursor( 0, 1 );
  for (byte thisByte = 0; thisByte < 4; thisByte++) {
    // print the value of each byte of the IP address:
    lcd.print(Ethernet.localIP()[thisByte], DEC);
    lcd.print("."); 
   }
}
