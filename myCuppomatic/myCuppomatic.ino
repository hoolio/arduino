/*
  myCuppomatic by Julius Roberts. June 2012.
  based variously on AnalogReadSignal, leosticktunes, blink, Thermister.. etc
  
  purpose is to control a relay to which is attached the kettle.  we want to stop the kettle
  before it boils so we can have a nice cup of green tea.  we control target temp with a pot.
  
  reads potentiometer which sets a target temperature  
  reads thermister which sets current temperature
  outputs target and current temps to an LCD display
  blink orange heating led proportional to remaining heating time and heat kettle*
  when target temp is reached, illuminate red LED and sound an alarm.
  cut power to the kettle*
  stop processing.

  * currently there is no support for an actual relay, so the orange heating led acts as the kettle.
*/
#include <math.h>
#include <LiquidCrystal.h>

// analog pins
char C_InputPot = A0;
int C_ThermistorPIN = A1;

// digital pins
LiquidCrystal lcd( 8, 9, 4, 5, 6, 7 );
int C_HeatingLED = 10;  char C_HeatingLEDState = HIGH;
int C_Speaker = 11;  //note we use two pins for more fancy speaker action
int C_Speaker2 = 12; //note we use two pins for more fancy speaker action ;)
int C_OverTempLED = 13;
int C_StartStopButton = 2; char C_StartStopButtonState = 0;
int C_ProgramModeToggle = 19; char C_ProgramModeToggleState = 0;  //using analog pin5 as a digital input, 13+5=19

// the song to play at the truimphant conclusion :)
char *song = "20thCenFoxShort:d=16,o=5,b=140:b,8p,b,b,2b,p,c6,32p,b,32p,c6,32p,b,32p,c6,32p,b,8p,b,b,b,32p,b,32p,b,32p,b,32p,b,32p,b,32p,b,32p,g#,32p,a,32p,b,8p,b,b,2b,4p";

int V_targetTemp = 100;
//char V_ProgramMode = "BLAH";

void setup() {  
  //initialise the various i/o pins we're using
  pinMode(C_InputPot, INPUT);
  pinMode(C_ThermistorPIN, INPUT);  
  pinMode(C_StartStopButton, INPUT);
  pinMode(C_ProgramModeToggle, INPUT);
      
  pinMode(C_Speaker, OUTPUT);
  pinMode(C_Speaker2, OUTPUT);
  pinMode(C_HeatingLED, OUTPUT);
  pinMode(C_OverTempLED, OUTPUT);
  
  lcd.begin(16, 2);
  
  // display splash screen and instructions
  lcd.setCursor(0, 0); lcd.print(" Cuppomatic; a  ");
  lcd.setCursor(0, 1); lcd.print("robotic teamaid!");
  delay(5000);
  
  // perform initial config
  show_instructions_to_LCD();
  //lcd.setCursor(0, 0); lcd.print("Toggle tea/coffee");
  //lcd.setCursor(0, 1); lcd.print("mode with switch ");
  //delay(1000); clearLCD();
  
  /*
  // first determine if we're in tea or coffee mode
  while (C_StartStopButtonState != HIGH){
    C_ProgramModeToggleState = digitalRead(C_ProgramModeToggle);

    if (C_ProgramModeToggleState != HIGH){
        lcd.setCursor(0, 0); lcd.print("  Coffee Mode  "); 
        lcd.setCursor(0, 1); lcd.print("button to enter");
        V_ProgramMode="COFFEE";
    } else {
        lcd.setCursor(0, 0); lcd.print("    Tea Mode   "); 
        lcd.setCursor(0, 1); lcd.print("button to enter");
        V_ProgramMode="TEA";
    }
    C_StartStopButtonState = digitalRead(C_StartStopButton);
      
  }  */
  
  //ok, so if program mode is tea, we do all the fancy stuff
  //if(V_ProgramMode=="TEA"){
    lcd.setCursor(0, 0); lcd.print("Turn temp knob,");
    lcd.setCursor(0, 1); lcd.print("button to start");
    delay(5000); clearLCD();
    
    // allow the user to use the knob to set a target temp
    // then press the button to start operations
    int V_currentTemp = Thermistor(analogRead(C_ThermistorPIN));
    while (C_StartStopButtonState != HIGH){
      // read the desired target temp, then map that value to something between and 99.
      int V_targetTempX = analogRead(C_InputPot);
      V_targetTemp = map(V_targetTempX, 0, 1023, 0, 99); 
      lcd.setCursor(1, 0); lcd.print("set:");  lcd.print(V_targetTemp); lcd.print("  now:"); lcd.print(V_currentTemp);
      lcd.setCursor(0, 1); lcd.print("button to start");
      
      // sanity check that the target temp isn't lower than the current temp
      V_currentTemp = Thermistor(analogRead(C_ThermistorPIN));
        
      if (V_targetTemp < V_currentTemp){
        clearLCD();
        lcd.setCursor(0, 0); lcd.print("Um cant set temp");
        lcd.setCursor(0, 1); lcd.print("  lower than "); lcd.setCursor(13, 1);lcd.print(V_currentTemp);
        delay(1000); clearLCD();
      } else {
        // check to see if they have pressed the start/stop button, if so we exit this loop
        C_StartStopButtonState = digitalRead(C_StartStopButton);
      }
    }
    
  // so this is coffee mode, simply set the target temp to something massive (ie; allow kettle to boil)  
  //} else {
  //  V_targetTemp = 100:
  //}
  
  // initiate countdown to kettle on  
  start_kettle_countdown(3);
  
  Serial.begin(9600);
}

void loop() {  
  // read the temps
  int V_currentTemp = Thermistor(analogRead(C_ThermistorPIN));
  
  // note we already had the user define V_targetTemp in setup() but 
  // having V_targetTemp here again allows us to calibrate and/or fix setup mistakes etc
  int V_targetTempX = analogRead(C_InputPot);
  V_targetTemp = map(V_targetTempX, 0, 1023, 0, 99);

  // determine the current/target delta
  int V_tempDelta = V_targetTemp - V_currentTemp;
  if (V_tempDelta < 0) { V_tempDelta = 0;}  // if this < 0 then we create delay(-N) which crashes the leo. 
  
  // output to serial
  Serial.print("V_targetTemp: ");  Serial.print(V_targetTemp);  Serial.print(" V_currentTemp: ");  Serial.print(V_currentTemp);    Serial.print(" V_tempDelta: ");  Serial.println(V_tempDelta);
  // output to LCD
  lcd.setCursor(1, 0); lcd.print("set:");  lcd.print(V_targetTemp); lcd.print("  now:"); lcd.print(V_currentTemp);

  // check to see if the kettle is up to temp.  we check this *before* turning on the relay
  // lest we trip the relay when the water is already up to temp or sommit.
  if (V_currentTemp >= V_targetTemp) {
    // here is where we'd set a relay LOW if we had one
    //somerelay(LOW);
    
    //illuminate the led and put some txt out to the screen
    Serial.println("target reached! ");
    digitalWrite(C_OverTempLED, HIGH); 
    
    lcd.setCursor(0, 0); lcd.print("   Finished:    ");
    lcd.setCursor(0, 1); lcd.print(" It's tea time! ");

    // make a sound for awhile..    
    //beep(12,200,5000,200);
    play_rtttl(song);
        
    // kettle to temp, stop forever (until device reset)
    while(1) { }; //simply start a while loop that never finishes..   
     
  } else { // the kettle isn't over temp
    
    // we'll blink the heating led and say "kettle on" on the LCD
    // and we'll do it proportional to tempdelta, ie faster when we're closer to target temp
    // NB: delayMilliSeconds(the timer used, the delay in milliseconds)
    int V_HeatLEDDElay = V_tempDelta * 10;
    if (delayMilliSeconds(1,V_HeatLEDDElay)){
      if(C_HeatingLEDState==HIGH){
        C_HeatingLEDState=LOW;
        lcd.setCursor(0, 1); lcd.print("   Kettle On!   ");
        digitalWrite(C_HeatingLED,C_HeatingLEDState);
      } else{
        C_HeatingLEDState=HIGH;
        lcd.setCursor(0, 1); lcd.print(">> Kettle On! <<");
        digitalWrite(C_HeatingLED,C_HeatingLEDState);
      }
    }
    
    // here is where we'd set a relay HIGH if we had one
    //somerelay(HIGH);
  }
  
  // delay in between reads for stability
  delay(100);
}

void start_kettle_countdown(int number_of_seconds_to_countdown){
  lcd.setCursor(0, 0); lcd.print("starting kettle ");
  lcd.setCursor(0, 1); lcd.print("                ");
  for (int i = number_of_seconds_to_countdown; i > -1; i--) {
    lcd.setCursor(0, 1); lcd.print(" in "); lcd.setCursor(4, 1); lcd.print(i); lcd.setCursor(5, 1); lcd.print(" seconds! ");
    delay(1000);
  }
  clearLCD();
}

void show_instructions_to_LCD(){
  lcd.setCursor(0, 0); lcd.print("Use knob to set ");
  lcd.setCursor(0, 1); lcd.print("  desired temp  ");
  delay(4000); clearLCD();
  /*lcd.setCursor(0, 0); lcd.print("Then press the  ");
  lcd.setCursor(0, 1); lcd.print(" button to start");
  delay(4000); clearLCD();*/
  lcd.setCursor(0, 0); lcd.print("  When temp OK  ");
  lcd.setCursor(0, 1); lcd.print("kettle will stop");
  delay(4000); clearLCD();
}

void clearLCD(){
  lcd.setCursor(0, 0); lcd.print("                ");
  lcd.setCursor(0, 1); lcd.print("                ");
}

// global functional definition(s):
void beep(int V_repeats, int V_beep_duration, int V_frequency, int V_silence_duration){
   for (int x = 0; x <= V_repeats; x++){
      //int V_beep_duration = 200; // OK
      //int V_frequency = 5000 ;// OK, somewhere between 4k and 7k seems good.
      int C_danDur = 1000000 / V_frequency; 
      unsigned long start = millis();
      while (millis() - start <= V_beep_duration) {
        digitalWrite(C_Speaker, HIGH);
        delayMicroseconds(C_danDur);
        digitalWrite(C_Speaker, LOW);
        delayMicroseconds(C_danDur);      
       }
       delay(V_silence_duration);
    }
}

void Terminate() {
    lcd.setCursor(0, 0); lcd.print("Program aborted ");
    lcd.setCursor(0, 1); lcd.print("  reset device  ");
    
    Serial.println("Program aborted, reset device.");

    while(1) { }; //simply start a while loop that never finishes..
}
  
unsigned long timer[10];
byte timerState[10];
int delayMilliSeconds(int timerNumber,unsigned long delaytime){
  unsigned long timeTaken;
  if (timerState[timerNumber]==0){    //If the timer has been reset (which means timer (state ==0) then save millis() to the same number timer,
    timer[timerNumber]=millis();
    timerState[timerNumber]=1;      //now we want mark this timer "not reset" so that next time through it doesn't get changed.
  }
  if (millis()> timer[timerNumber]){
    timeTaken=millis()+1-timer[timerNumber];    //here we see how much time has passed
  }
  else{
    timeTaken=millis()+2+(4294967295-timer[timerNumber]);    //if the timer rolled over (more than 48 days passed)then this line accounts for that
  }
  if (timeTaken>=delaytime) {          //here we make it easy to wrap the code we want to time in an "IF" statement, if not then it isn't and so doesn't get run.
     timerState[timerNumber]=0;  //once enough time has passed the timer is marked reset.
     return 1;                          //if enough time has passed the "IF" statement is true
  }
  else {                               //if enough time has not passed then the "if" statement will not be true.
    return 0;
  }
}

float Thermistor(int RawADC) {
  //float vcc = 4.91;         // only used for display purposes, if used set to the measured Vcc.
  float pad = 9790;         // balance/pad resistor value, set this to the measured resistance of your pad resistor
  //float thermr = 4700;      // thermistor nominal resistance
  float thermr = 10000;      // thermistor nominal resistance
  long Resistance;  
  float Temp;  // Dual-Purpose variable to save space.

  Resistance=((1024 * pad / RawADC) - pad); 
  Temp = log(Resistance); // Saving the Log(resistance) so not to calculate  it 4 times later
  Temp = 1 / (0.001129148 + (0.000234125 * Temp) + (0.0000000876741 * Temp * Temp * Temp));
  Temp = Temp - 273.15;  // Convert Kelvin to Celsius                      

  //Temp = Temp - 20; // Hoolios unforgivably bad conversion factor when using the 4.k thermistor
  Temp = Temp - 3; // Hoolios unforgivably bad conversion factor when using the 10k thermistor.

  return Temp;   // Return the Temperature
}

void play_rtttl(char *p) {
  // this function based on the LCA 2012 leo stick tunes collaborative effort at https://gist.github.com/1800871
    
  //char *song = "Muppets:d=4,o=5,b=250:c6,c6,a,b,8a,b,g,p,c6,c6,a,8b,8a,8p,g.,p,e,e,g,f,8e,f,8c6,8c,8d,e,8e,8e,8p,8e,g,2p,c6,c6,a,b,8a,b,g,p,c6,c6,a,8b,a,g.,p,e,e,g,f,8e,f,8c6,8c,8d,e,8e,d,8d,c";
  //char *song = "Looney:d=4,o=5,b=140:32p,c6,8f6,8e6,8d6,8c6,a.,8c6,8f6,8e6,8d6,8d#6,e.6,8e6,8e6,8c6,8d6,8c6,8e6,8c6,8d6,8a,8c6,8g,8a#,8a,8f";
  //char *song = "20thCenFox:d=16,o=5,b=140:b,8p,b,b,2b,p,c6,32p,b,32p,c6,32p,b,32p,c6,32p,b,8p,b,b,b,32p,b,32p,b,32p,b,32p,b,32p,b,32p,b,32p,g#,32p,a,32p,b,8p,b,b,2b,4p,8e,8g#,8b,1c#6,8f#,8a,8c#6,1e6,8a,8c#6,8e6,1e6,8b,8g#,8a,2b";
  //char *song = "Canon:d=4,o=5,b=80:8d,8f#,8a,8d6,8c#,8e,8a,8c#6,8d,8f#,8b,8d6,8a,8c#,8f#,8a,8b,8d,8g,8b,8a,8d,8f#,8a,8b,8f#,8g,8b,8c#,8e,8a,8c#6,f#6,8f#,8a,e6,8e,8a,d6,8f#,8a,c#6,8c#,8e,b,8d,8g,a,8f#,8d,b,8d,8g,c#.6";
  //char *song = "StarWars:d=4,o=5,b=45:32p,32f#,32f#,32f#,8b.,8f#.6,32e6,32d#6,32c#6,8b.6,16f#.6,32e6,32d#6,32c#6,8b.6,16f#.6,32e6,32d#6,32e6,8c#.6,32f#,32f#,32f#,8b.,8f#.6,32e6,32d#6,32c#6,8b.6,16f#.6,32e6,32d#6,32c#6,8b.6,16f#.6,32e6,32d#6,32e6,8c#6";
  //char *song = "Gadget:d=16,o=5,b=50:32d#,32f,32f#,32g#,a#,f#,a,f,g#,f#,32d#,32f,32f#,32g#,a#,d#6,4d6,32d#,32f,32f#,32g#,a#,f#,a,f,g#,f#,8d#";
  //char *song = "Smurfs:d=32,o=5,b=200:4c#6,16p,4f#6,p,16c#6,p,8d#6,p,8b,p,4g#,16p,4c#6,p,16a#,p,8f#,p,8a#,p,4g#,4p,g#,p,a#,p,b,p,c6,p,4c#6,16p,4f#6,p,16c#6,p,8d#6,p,8b,p,4g#,16p,4c#6,p,16a#,p,8b,p,8f,p,4f#";
  //char *song = "LeisureSuit:d=16,o=6,b=56:f.5,f#.5,g.5,g#5,32a#5,f5,g#.5,a#.5,32f5,g#5,32a#5,g#5,8c#.,a#5,32c#,a5,a#.5,c#.,32a5,a#5,32c#,d#,8e,c#.,f.,f.,f.,f.,f,32e,d#,8d,a#.5,e,32f,e,32f,c#,d#.,c#";
    
  #define isdigit(n) (n >= '0' && n <= '9')
   
  #define NOTE_B0 31
  #define NOTE_C1 33
  #define NOTE_CS1 35
  #define NOTE_D1 37
  #define NOTE_DS1 39
  #define NOTE_E1 41
  #define NOTE_F1 44
  #define NOTE_FS1 46
  #define NOTE_G1 49
  #define NOTE_GS1 52
  #define NOTE_A1 55
  #define NOTE_AS1 58
  #define NOTE_B1 62
  #define NOTE_C2 65
  #define NOTE_CS2 69
  #define NOTE_D2 73
  #define NOTE_DS2 78
  #define NOTE_E2 82
  #define NOTE_F2 87
  #define NOTE_FS2 93
  #define NOTE_G2 98
  #define NOTE_GS2 104
  #define NOTE_A2 110
  #define NOTE_AS2 117
  #define NOTE_B2 123
  #define NOTE_C3 131
  #define NOTE_CS3 139
  #define NOTE_D3 147
  #define NOTE_DS3 156
  #define NOTE_E3 165
  #define NOTE_F3 175
  #define NOTE_FS3 185
  #define NOTE_G3 196
  #define NOTE_GS3 208
  #define NOTE_A3 220
  #define NOTE_AS3 233
  #define NOTE_B3 247
  #define NOTE_C4 262
  #define NOTE_CS4 277
  #define NOTE_D4 294
  #define NOTE_DS4 311
  #define NOTE_E4 330
  #define NOTE_F4 349
  #define NOTE_FS4 370
  #define NOTE_G4 392
  #define NOTE_GS4 415
  #define NOTE_A4 440
  #define NOTE_AS4 466
  #define NOTE_B4 494
  #define NOTE_C5 523
  #define NOTE_CS5 554
  #define NOTE_D5 587
  #define NOTE_DS5 622
  #define NOTE_E5 659
  #define NOTE_F5 698
  #define NOTE_FS5 740
  #define NOTE_G5 784
  #define NOTE_GS5 831
  #define NOTE_A5 880
  #define NOTE_AS5 932
  #define NOTE_B5 988
  #define NOTE_C6 1047
  #define NOTE_CS6 1109
  #define NOTE_D6 1175
  #define NOTE_DS6 1245
  #define NOTE_E6 1319
  #define NOTE_F6 1397
  #define NOTE_FS6 1480
  #define NOTE_G6 1568
  #define NOTE_GS6 1661
  #define NOTE_A6 1760
  #define NOTE_AS6 1865
  #define NOTE_B6 1976
  #define NOTE_C7 2093
  #define NOTE_CS7 2217
  #define NOTE_D7 2349
  #define NOTE_DS7 2489
  #define NOTE_E7 2637
  #define NOTE_F7 2794
  #define NOTE_FS7 2960
  #define NOTE_G7 3136
  #define NOTE_GS7 3322
  #define NOTE_A7 3520
  #define NOTE_AS7 3729
  #define NOTE_B7 3951
  #define NOTE_C8 4186
  #define NOTE_CS8 4435
  #define NOTE_D8 4699
  #define NOTE_DS8 4978  
  #define OCTAVE_OFFSET 0
  
  int notes[] = { 0,
  NOTE_C4, NOTE_CS4, NOTE_D4, NOTE_DS4, NOTE_E4, NOTE_F4, NOTE_FS4, NOTE_G4, NOTE_GS4, NOTE_A4, NOTE_AS4, NOTE_B4,
  NOTE_C5, NOTE_CS5, NOTE_D5, NOTE_DS5, NOTE_E5, NOTE_F5, NOTE_FS5, NOTE_G5, NOTE_GS5, NOTE_A5, NOTE_AS5, NOTE_B5,
  NOTE_C6, NOTE_CS6, NOTE_D6, NOTE_DS6, NOTE_E6, NOTE_F6, NOTE_FS6, NOTE_G6, NOTE_GS6, NOTE_A6, NOTE_AS6, NOTE_B6,
  NOTE_C7, NOTE_CS7, NOTE_D7, NOTE_DS7, NOTE_E7, NOTE_F7, NOTE_FS7, NOTE_G7, NOTE_GS7, NOTE_A7, NOTE_AS7, NOTE_B7
  };
    // Absolutely no error checking in here
  
    byte default_dur = 4;
    byte default_oct = 6;
    int bpm = 63;
    int num;
    long wholenote;
    long duration;
    byte note;
    byte scale;
  
    // format: d=N,o=N,b=NNN:
    // find the start (skip name, etc)
  
    while(*p != ':') p++; // ignore name
    p++; // skip ':'
  
    // get default duration
    if(*p == 'd'){
      p++; p++; // skip "d="
      num = 0;
      while(isdigit(*p)){
        num = (num * 10) + (*p++ - '0');
      }
      if(num > 0) default_dur = num;
      p++; // skip comma
    }
  
  
    // get default octave
    if(*p == 'o'){
      p++; p++; // skip "o="
      num = *p++ - '0';
      if(num >= 3 && num <=7) default_oct = num;
      p++; // skip comma
    }
  
    // get BPM
    if(*p == 'b'){
      p++; p++; // skip "b="
      num = 0;
      while(isdigit(*p)){
        num = (num * 10) + (*p++ - '0');
      }
      bpm = num;
      p++; // skip colon
    }
    
    // BPM usually expresses the number of quarter notes per minute
    wholenote = (60 * 1000L / bpm) * 4; // this is the time for whole note (in milliseconds)
    
    // now begin note loop
    while(*p){
      // first, get note duration, if available
      num = 0;
      while(isdigit(*p)){
        num = (num * 10) + (*p++ - '0');
      }
      
      if(num) duration = wholenote / num;
      else duration = wholenote / default_dur; // we will need to check if we are a dotted note after
  
      // now get the note
      note = 0;
      
      switch(*p) {
        case 'c':
          note = 1;
          break;
        case 'd':
          note = 3;
          break;
        case 'e':
          note = 5;
          break;
        case 'f':
          note = 6;
          break;
        case 'g':
          note = 8;
          break;
        case 'a':
          note = 10;
          break;
        case 'b':
          note = 12;
          break;
        case 'p':
        default:
          note = 0;
      }
      p++;
  
      // now, get optional '#' sharp
      if(*p == '#'){
        note++;
        p++;
      }
  
      // now, get optional '.' dotted note
      if(*p == '.'){
        duration += duration/2;
        p++;
      }
    
      // now, get scale
      if(isdigit(*p)){
        scale = *p - '0';
        p++;
      }
      else{
        scale = default_oct;
      }
  
      scale += OCTAVE_OFFSET;
  
      if(*p == ',')
        p++; // skip comma for next note (or we may be at the end)
  
      // now play the note
  
      if(note){
        digitalWrite(C_Speaker2, HIGH);
        int danFreq;
        float danDur;
        danFreq = notes[(scale - 4) * 12 + note];
        danDur = 1000000 / danFreq;
        unsigned long start = millis();
        while (millis() - start <= duration) {
            digitalWrite(C_Speaker, HIGH);
            delayMicroseconds(danDur);
            digitalWrite(C_Speaker, LOW);
            delayMicroseconds(danDur);
        }
        digitalWrite(C_Speaker2, LOW);
      }
      else{
        delay(duration);
      }
    }
}
