#!/usr/bin/python

# output header stuff
print "#include <Canbus.h>"
print "#include <defaults.h>"
print "#include <global.h>"
print "#include <mcp2515.h>"
print "#include <mcp2515_defs.h>"
print ""
print "void setup() {"
print "  Serial.begin(9600);"
print ""
print "  if(Canbus.init(CANSPEED_500))"
print '    { Serial.println("CAN Init ok..");}'
print "  else"
print '    {Serial.println("Cant init CAN!!");}'
print "}"
print ""
print "void loop()"
print "{"
print '  Serial.println("Spamming CAN..");'
print ""
print "  tCAN message;"

# open dump file
f = open('focus.can.dump', 'r')

for line in f:
    splitline = line.split(' ')

    print "  message.id = 0x" + splitline[1] + ";"
    print "  message.header.rtr = 0;"
    print "  message.header.length = 8;"
    print "  message.data[0] = 0x" + splitline[3].rstrip() + ";"
    print "  message.data[1] = 0x" + splitline[4].rstrip() + ";"

    if len(splitline) >= 6:
        print "  message.data[2] = 0x" + splitline[5].rstrip() + ";"

    if len(splitline) >= 7:
        print "  message.data[3] = 0x" + splitline[6].rstrip() + ";"

    if len(splitline) >= 8:
        print "  message.data[4] = 0x" + splitline[7].rstrip() + ";"

    if len(splitline) >= 9:
        print "  message.data[5] = 0x" + splitline[8].rstrip() + ";"

    if len(splitline) >= 10:
        print "  message.data[6] = 0x" + splitline[9].rstrip() + ";"

    if len(splitline) >= 11:
        print "  message.data[7] = 0x" + splitline[10].rstrip() + ";"

    if len(splitline) >= 12:
        print "  message.data[8] = 0x" + splitline[11].rstrip() + ";"

    if len(splitline) >= 13:
        print "  message.data[9] = 0x" + splitline[12].rstrip() + ";"

    print "  mcp2515_bit_modify(CANCTRL, (1<<REQOP2)|(1<<REQOP1)|(1<<REQOP0), 0);"
    print "  mcp2515_send_message(&message);"
    # print "  message.data[0] = 0x0;"
    # print "  message.data[1] = 0x0;"
    # print "  message.data[2] = 0x0;"
    # print "  message.data[3] = 0x0;"
    # print "  message.data[4] = 0x0;"
    # print "  message.data[5] = 0x0;"
    # print "  message.data[6] = 0x0;"
    # print "  message.data[7] = 0x0;"
    # print "  message.data[8] = 0x0;"
    # print "  message.data[9] = 0x0;"
    print "  delay(50);"
    print ""
    del splitline

print "}"
