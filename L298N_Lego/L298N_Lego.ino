/*  L298 powered Lego Technic tracked vehicle
 *  by Hoolio Wobbits 2019
 *  based on code from Arduino DC Motor Control - PWM | H-Bridge | L298N  -  Example 01
 *   by Dejan Nedelkovski, www.HowToMechatronics.com
*/

#define outputMotorAenable 5
#define outputMotorAin1 8
#define outputMotorAin2 9
#define outputMotorBenable 6
#define outputMotorBin1 10
#define outputMotorBin2 11
#define outputBuzzer 3
//#define potPin A0
#define inputLeftFord A0 // green
#define inputLeftBack A1 // blue
#define inputRightFord A3 // white
#define inputRightBack A2 // orange
#define inputBuzzer A4 // yellow

void setup() {
  // inputs from microbit (via phone)
  pinMode(inputLeftFord, INPUT);
  pinMode(inputLeftBack, INPUT);
  pinMode(inputRightFord, INPUT);
  pinMode(inputRightBack, INPUT);
  pinMode(inputBuzzer, INPUT);
  // outputs to motor driver board etc
  pinMode(outputMotorAenable, OUTPUT);
  pinMode(outputMotorAin1, OUTPUT);
  pinMode(outputMotorAin2, OUTPUT);
  pinMode(outputMotorBenable, OUTPUT);
  pinMode(outputMotorBin1, OUTPUT);
  pinMode(outputMotorBin2, OUTPUT);
  pinMode(outputBuzzer, OUTPUT);
  //
  Serial.begin(9600);
  Serial.println("bot ready..");
  //startupSound();
}
void loop() {
  const int pwmOutput = 255;
  const int moveThreshold = 125; // how much of a signal before we trigger

  // left
  if (analogRead(inputLeftFord) > moveThreshold){
    moveinputLeftFord(pwmOutput,inputLeftFord);
  } else if (analogRead(inputLeftBack) > moveThreshold){
    moveinputLeftBack(pwmOutput,inputLeftBack);
  } else stopLeft();

  // right
  if (analogRead(inputRightFord) > moveThreshold){
    moveinputRightFord(pwmOutput,inputRightFord);
  } else if (analogRead(inputRightBack) > moveThreshold){
    moveinputRightBack(pwmOutput,inputRightBack);
  } else stopRight();

}

//forwards
void moveinputLeftFord(int pwmOutput, int inputAmount) {
  Serial.print("moveLeftForwards(");
  Serial.print(pwmOutput); Serial.print(",");
  Serial.print(inputAmount); Serial.println(")");
  analogWrite(outputMotorAenable, pwmOutput); // Send PWM signal to L298N Enable pin
  digitalWrite(outputMotorAin1, HIGH);
  digitalWrite(outputMotorAin2, LOW);
  // pin frequency duration
  tone(outputBuzzer, 18, 250);
}

void moveinputRightFord(int pwmOutput, int inputAmount) {
  Serial.print("moveRightForwards(");
  Serial.print(pwmOutput); Serial.print(",");
  Serial.print(inputAmount); Serial.println(")");
  analogWrite(outputMotorBenable, pwmOutput); // Send PWM signal to L298N Enable pin
  digitalWrite(outputMotorBin1, HIGH);
  digitalWrite(outputMotorBin2, LOW);
  // pin frequency duration
  tone(outputBuzzer, 19, 250);
}

// back
void moveinputLeftBack(int pwmOutput, int inputAmount) {
  Serial.print("moveinputLeftBack(");
  Serial.print(pwmOutput); Serial.print(",");
  Serial.print(inputAmount); Serial.println(")");
  analogWrite(outputMotorAenable, pwmOutput); // Send PWM signal to L298N Enable pin
  digitalWrite(outputMotorAin1, LOW);
  digitalWrite(outputMotorAin2, HIGH);
  // pin frequency duration
  tone(outputBuzzer, 22, 250);
}

void moveinputRightBack (int pwmOutput, int inputAmount){
  Serial.print("moveinputRightBack(");
  Serial.print(pwmOutput); Serial.print(",");
  Serial.print(inputAmount); Serial.println(")");
  analogWrite(outputMotorBenable, pwmOutput); // Send PWM signal to L298N Enable pin
  digitalWrite(outputMotorBin1, LOW);
  digitalWrite(outputMotorBin2, HIGH);
  // pin frequency duration
  tone(outputBuzzer, 21, 250);
}

// stop
void stopLeft(){
  //Serial.println("stopLeft()");
  analogWrite(outputMotorAenable, HIGH); // Send PWM signal to L298N Enable pin
  digitalWrite(outputMotorAin1, LOW);
  digitalWrite(outputMotorAin2, LOW);
}

void stopRight(){
  //Serial.println("stopRight()");
  analogWrite(outputMotorBenable, HIGH); // Send PWM signal to L298N Enable pin
  digitalWrite(outputMotorBin1, LOW);
  digitalWrite(outputMotorBin2, LOW);
}

void startupSound(){
   //basically random-ish noise ascending in frequency
   int freqbase=180; // where to start
   for(int i=0; i < 30; i++){ // how many individual noises in the effect
      freqbase = freqbase + 15; // how much lower should each noise go?
      int frequency = random(freqbase-30,freqbase); //what exact freqency to plot
      int mydelay = random(30,150); // how long should each noise go for
      tone(outputBuzzer, frequency, mydelay);
      Serial.print("frequency = "); Serial.print(frequency);  Serial.print(" mydelay = "); Serial.println(mydelay);
   }
   tone(outputBuzzer, 100, 15);     delay(250);
   for (int i = 0; i < 2; i++) {
      for (int i = 0; i < 4; i++) {
         tone(outputBuzzer, 690, 100);     delay(20);
         tone(outputBuzzer, 640, 100);     delay(20);
      }
      tone(outputBuzzer, 100, 15);     delay(100);
   }
}
