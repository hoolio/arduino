/*
 * LiquidMenu library - buttons_menu.ino
 *
 */

#include <LiquidCrystal.h>
#include <LiquidMenu.h>

// Pin mapping for the display:
LiquidCrystal lcd(8, 9, 4, 5, 6, 7);
const int lcd_buttons_pin = 0;
int lcd_key     = 0;
int adc_key_in  = 0;
#define btnRIGHT  0
#define btnUP     1
#define btnDOWN   2
#define btnLEFT   3
#define btnSELECT 4
#define btnNONE   5
#define backlightPin 10

//memory 
int timer1_duration = EEPROM.read(0);
int timer2_duration = EEPROM.read(1);
int timer3_duration = EEPROM.read(2);
int timer4_duration = EEPROM.read(3);
int backlight_duty  = EEPROM.read(4);

int read_LCD_buttons(){
 adc_key_in = analogRead(lcd_buttons_pin);      // read the value from the sensor
 // "..We make this the 1st option for speed reasons since it will be the most likely result
 if (adc_key_in > 1000) return btnNONE;
 // "..my buttons when read are centered at these valies: 0, 144, 329, 504, 741
 // "..we add approx 50 to those values and check to see if we are close
 if (adc_key_in < 50)   return btnRIGHT;
 if (adc_key_in < 195)  return btnUP;
 if (adc_key_in < 380)  return btnDOWN;
 if (adc_key_in < 555)  return btnLEFT;
 if (adc_key_in < 790)  return btnSELECT;

 return btnNONE;  // when all others fail, return this...
}

byte pwmLevel = 100;
byte volLevel = 5;

LiquidLine pwm_line(0, 0, "PWM level: ", pwmLevel);
LiquidScreen pwm_screen(pwm_line);

LiquidLine vol_line(0, 0, "vol level: ", volLevel);
LiquidScreen vol_screen(vol_line);

LiquidMenu menu(lcd);

// Function to be attached to the pwm_line object.
/*
 * This function is used for incrementing the PWM level on the
 * 'pwmPin'. It increments the value of 'pwmLevel' and then
 * writes it to the pin.
 */
void pwm_up() {
	if (pwmLevel < 225) {
		pwmLevel += 25;
	} else {
		pwmLevel = 250;
	}
}

void pwm_down() {
	if (pwmLevel > 25) {
		pwmLevel -= 25;
	} else {
		pwmLevel = 0;
	}
}

void vol_down() {
    volLevel --;
}

void vol_up() {
    volLevel ++;
}

void setup() {
	Serial.begin(9600);

	//pinMode(pwmPin, OUTPUT);
  pinMode(lcd_buttons_pin, INPUT);

	lcd.begin(16, 2);

	// Function to attach functions to LiquidLine objects.
  // we'll reuse up to mean increase and id them with 1
	//pwm_line.attach_function(1, pwm_up);
	//pwm_line.attach_function(2, pwm_down);

  vol_line.attach_function(1, vol_up);
  vol_line.attach_function(2, vol_down);

	menu.add_screen(vol_screen);
	//menu.add_screen(pwm_screen);
  menu.update();
}

void loop() {

  lcd_key = read_LCD_buttons();
  if(lcd_key != btnNONE){Serial.println(lcd_key);}
  //menu.switch_focus();

   switch (lcd_key){
     case btnUP:{   
      menu.switch_focus();
      menu.call_function(1);
      break;
      }
     case btnDOWN:{ 
      menu.call_function(2);
      break;
      }
     case btnLEFT:{ 
      menu.previous_screen();
      break;
      }
     case btnRIGHT:{ 
      menu.next_screen();
      break;
     }
     case btnSELECT:{ 
      menu.switch_focus();}
      break;
   }

  //menu.update();
  delay(200);
}
