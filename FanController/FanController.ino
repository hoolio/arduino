/*
 * Arduino Tachometer (for beginners)
 * uC Hardware: Arduino Uno R3 & DFR0009 LCD Shield 
 * Sensor: Delta AFB0712HHB tach signal (green)
 * Code Attribution: T.K.Hareendran(Codrey Electronics)
  */

// NOTES on re: tach signal under pwm
// https://www.analog.com/en/analog-dialogue/articles/how-to-control-fan-speed.html#
 
 #include <LiquidCrystal.h>
LiquidCrystal lcd(8, 9, 4, 5, 6, 7); // Pins assigned for DFR0009
// NOTE THAT PIN 10 IS USED FOR BACKLIGHT CONTROL.
//      and a0 is used to read the push buttons.

// pin assignments
const byte fan1interruptPin = 2;
const byte fan2interruptPin = 3;
const byte fan1pwmPin = 11;
const byte fan2pwmPin = 12;

// variable assignments
volatile uint32_t rev1;
volatile uint32_t freq1;
volatile uint32_t rev2;
volatile uint32_t freq2;
volatile uint32_t rpm1;
volatile uint32_t rpm2;
volatile uint32_t duty = 1;
unsigned long measureTime = 0;

void setDuty(int duty, int duration){
  int dutyPWM = map(duty, 0,100,0,254);

  for (int i=0; i <= duration * 2; i++) {
    delay(500);
    analogWrite(fan1pwmPin, dutyPWM);
    analogWrite(fan2pwmPin, dutyPWM);

    calculateTacho();

    // update display
    lcd.clear(); 
    lcd.setCursor(0,0); lcd.print("1: "); lcd.print(duty); lcd.print("% "); lcd.print(rpm1); lcd.print(" rpm"); 
    lcd.setCursor(0,1); lcd.print("2: "); lcd.print(duty); lcd.print("% "); lcd.print(rpm2); lcd.print(" rpm"); 
  }
}

void addRevolutionFan1() { rev1++; }
void addRevolutionFan2() { rev2++; }

void calculateTacho() {
  noInterrupts(); // disable interrupts while we do the math
  
  // calculate rpms based on the formula
  // rpm = (revs/2) * 60
  rpm1 = (rev1/2) * 60000 / (millis() - measureTime);
  rpm2 = (rev2/2) * 60000 / (millis() - measureTime);
  
  freq1 = rev1; rev1 = 0;
  freq2 = rev2; rev2 = 0;

  // sets a timepoint to accurately determine loop(duration)
  measureTime = millis();

  interrupts();  // reenable interrupts again
}

void setup() {
  // setup pins and then use them for the tach signal interrupts
  attachInterrupt(digitalPinToInterrupt(fan1interruptPin), addRevolutionFan1, FALLING);
  attachInterrupt(digitalPinToInterrupt(fan2interruptPin), addRevolutionFan2, FALLING);

  // setup lcd
  lcd.begin(16, 2);
  lcd.setCursor(0,0); lcd.print("   FANTESTIC! "); 
  delay(300);
  lcd.setCursor(0,1); lcd.print("  running 100% "); 

  // update display
  lcd.setCursor(0,1); lcd.print("ramp mode start");
}

void loop() {
  // cycle duty from 0 through 100, pause at 100 
  switch (duty) {
    case 1: // startup only
      setDuty(100, 10); // full speed, and show rpm
      duty = 0;
      break;
    case 0:
      setDuty(duty, 15); // alow fans to stop, and show rpm
      duty = duty + 5;
      break;
    case 100:
      setDuty(duty, 5); // full speed, and show rpm
      duty = 0;
      break;
    default:
      duty = duty + 5;
      setDuty(duty, 1);
      break;
  }

  // this pauses the main loop to gather data (via interrupts)
  // delay can vary because calculateTaco() compensates.
  //delay(1000);   
}
