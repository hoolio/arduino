/*
 * Copyright (C) 2012 Southern Storm Software, Pty Ltd.
 */

/*

*/

#include <SoftI2C.h>
#include <DS3232RTC.h>
#include <LiquidCrystal.h> // LCD

SoftI2C i2c(A4, A5);
DS3232RTC rtc(i2c);

LiquidCrystal lcd( 8, 9, 4, 5, 6, 7 );

void setup() {

    // setup lcd
    lcd.begin(16, 2);
    Serial.begin(9600);
}

void loop() {

    uint32_t epoch = time_t();

    delay(2000);

    uint32_t epoch2 = time_t();

    int result = epoch2 - epoch;


    //Serial.println(epoch);
    lcd.setCursor( 0, 0 );  lcd.print("  "); lcd.print(result); lcd.print("  "); 

    delay(1000);
}

uint32_t time_t(){
    // assemble time elements into time_t 

    #define LEAP_YEAR(Y)     ( ((1970+Y)>0) && !((1970+Y)%4) && ( ((1970+Y)%100) || !((1970+Y)%400) ) )
    #define SECS_PER_DAY 86400
    #define SECS_PER_HOUR 3600
    #define SECS_PER_MIN 60

    static const uint8_t monthDays[]={31,28,31,30,31,30,31,31,30,31,30,31}; // API starts months from 1, this array starts from 0

    RTCTime time;
    RTCDate date;
    rtc.readTime(&time);
    rtc.readDate(&date);

    int i;
    uint32_t seconds;

    // seconds from 1970 till 1 jan 00:00:00 of the given year
    seconds = (date.year-1970)*(SECS_PER_DAY * 365);

    for (i = 1971; i < date.year; i++) {
        if (LEAP_YEAR(i)) {
            seconds += SECS_PER_DAY;   // add extra days for leap years
            
        }
    }
    // add days for this year, months start from 1
    for (i = 1; i < date.month; i++) {
        if ( (i == 2) && LEAP_YEAR(date.year)) { 
            seconds += SECS_PER_DAY * 29;
        } else {
            seconds += SECS_PER_DAY * monthDays[i];  //monthDay array starts from 0
        }
    }

    seconds+= (date.day-1) * SECS_PER_DAY;  
    seconds+= time.hour * SECS_PER_HOUR;  
    seconds+= time.minute * SECS_PER_MIN; 
    seconds+= time.second;                

    return seconds; 
}

