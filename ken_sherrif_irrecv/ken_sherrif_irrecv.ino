#include <IRremote.h>

int C_IRReceiver = 2;
int C_ShutdownEventLED = 12;
int C_IRCommsGreenLED = 11;
int C_IRCommsOrangeLED = 10;

int V_DepressCount = 1; int V_DepressTime = millis(); int V_LastDepressTime=0;

IRrecv irrecv(C_IRReceiver);
decode_results results;

// define the various signals that come from the MCE remote
#define OFF_CODE1 0xA9EFCC78 // PC on MCE remote
#define OFF_CODE2 0x800F840C // PC on MCE remote
#define OFF_CODE3 0x4B790694 // PC on MCE remote
#define OFF_CODE4 0x800F040C // PC on MCE remote

void setup()
{
  pinMode(C_ShutdownEventLED, OUTPUT);
  pinMode(C_IRCommsGreenLED, OUTPUT);
  pinMode(C_IRCommsOrangeLED, OUTPUT);
  
  Serial.begin(9600);
  irrecv.enableIRIn(); // Start the receiver
  
  Serial.println("Ready to decode remote...");
}

void loop() {
  // if theres something coming from the remote, lets decode it
  if (irrecv.decode(&results)) {   
    digitalWrite(C_IRCommsGreenLED, HIGH);
    digitalWrite(C_IRCommsOrangeLED, HIGH);
    
    // if the remote was the MCE PC ON/OFF button:
    if ((results.value == OFF_CODE1) || (results.value == OFF_CODE2) || (results.value == OFF_CODE3) || (results.value == OFF_CODE4)) {
      
      // lets record when this button press happened, so we can compare it later
      V_LastDepressTime = V_DepressTime;
      V_DepressTime = millis();
      
      // if this next press came within ms of the last one assume it's part of some loong button press event
      // next we want to compare the time of this depress against the time of the last one 
      int V_DepressTimeDelta = V_DepressTime - V_LastDepressTime;
      
      if (V_DepressTimeDelta < 200){
        V_DepressCount++;

        Serial.print(results.value, HEX); Serial.print(" = Off ");
        Serial.print(" V_DepressCount ");Serial.print(V_DepressCount);
        Serial.print(" V_DepressTimeDelta ");Serial.println(V_DepressTimeDelta);
        
        // ok so if this long depress event lasts longer than X then actually trigger the DO SOMETHING
        if (V_DepressCount >= 20) {
          V_DepressCount=1; //so we dont trigger upon the on/off button event
          Serial.println("OK, trigger(ing) shutdown/startup");
          digitalWrite(C_ShutdownEventLED, HIGH);
          Serial.println(".. delaying awhile ..");
          delay(5000); 
          Serial.println(".. now listening again ..");
           digitalWrite(C_ShutdownEventLED, LOW);
        } 
      // else ie V_DepressTimeDelta > 200 they did press the same button as before, but released it
      // so we dont count it as part of a single event.  also we restart the timer, and the counter
      } else {
        Serial.print(results.value, HEX); Serial.print(" = Off X "); 
        Serial.print(" V_DepressTimeDelta ");Serial.println(V_DepressTimeDelta);
        V_DepressCount=1;        
      }
      
    // else if the button wasn't the MCE ON/OFF one
    } else {
       Serial.println(results.value, HEX);
    }
    irrecv.resume(); // Receive the next value
    digitalWrite(C_IRCommsGreenLED, LOW);
    digitalWrite(C_IRCommsOrangeLED, LOW);
  }
}
