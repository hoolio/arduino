/* == Grogometer ==
 * 
 * A thing to monitor and and eventually control the temp of a beer fridge
 *
 * What it does:
 * x) Monitor internal fridge temp
 * x) Output metrics min/current/max/avg temp to LCD and serial
 * x) if temp > (ci_BeerTargetTemp + ci_BeerAcceptableTempVariance) then fridge_on();
 * x) if temp < (ci_BeerTargetTemp - ci_BeerAcceptableTempVariance) then fridge_off();
 * 
 * TO DO
 * x) enable a heater
 * x) get UNIX time working.  then use time math to determine when to trigger
 * x) temp sensor input smoothing function with https://www.arduino.cc/en/Tutorial/Smoothing
 * x) take (smoothed) data for the average only when triggering relays
 *
 * NOTES:
 * I removed the code which handled negative numbers on the external
 *  temp sensor in order to save a + sign on the UI.  might turn out
 *  to be silly, but if i have frozen beer, i think i have bigger issues.
*/

// include the libraries
#include <Wire.h> // FT_TEMP external temp sensor
#include <LiquidCrystal.h> // LCD
//#include <pitches.h> // buzzer
#include <SoftI2C.h> // RTC
#include <DS3232RTC.h> // RTC

 // === PIN ASSIGNMENTS ===
 //
 LiquidCrystal lcd( 8, 9, 4, 5, 6, 7 );
 const char PIN_LCD_READ_BUTTON = A0;

 const char PIN_RTC_SDA = A4; // realtime clock data
 const char PIN_RTC_SCL = A5; // realtime clock clock

 const int PIN_FT_TEMP = 2; // external temp sensor
 const int PIN_BUZZER = 10; // buzzer

 const int PIN_FRIDGE_on = A1; // set this HIGH to turn ON relay 1
 const int PIN_FRIDGE_off = A2; // set this HIGH to turn OFF relay 1
 const int PIN_HEATER_on = 11; // set this HIGH to turn ON relay 2
 const int PIN_HEATER_off = 12; // set this HIGH to turn OFF relay 2

 const int PIN_ANALOG_POT_INPUT = A3; 

 // === SENSOR AND CLOCK
 //
 // stuff for FT_TEMP external temp sensor 
 //void getCurrentTemp( int *sign, int *whole, int *fract);
 //char internalTemp[6];
 //Setup Real Time Clock (RTC)
 SoftI2C i2c(PIN_RTC_SDA, PIN_RTC_SCL); // assign pins to SDA and SCL
 DS3232RTC rtc(i2c);
 RTCTime time;

// === NVRAM MANAGEMENT AND MEMORY MAP ===
// 
// ideally The NVRAM is used to hold brew attribues through a powercycle.
//  here we map which memory locations are used for what.
//  
#define writeMinimumFridgeAirTemp(Y); rtc.writeByte(0, Y) // WTF.  needed the ; to compile..??
#define readMinimumFridgeAirTemp rtc.readByte(0)

#define writeMaximumFridgeAirTemp(Z); rtc.writeByte(1, Z)
#define readMaximumFridgeAirTemp rtc.readByte(1)

#define writeAverageFridgeAirTemp(X); rtc.writeByte(2, X)
#define readAverageFridgeAirTemp rtc.readByte(2)

#define writePreviousSmoothedFridgeAirTemp(A); rtc.writeByte(10, A)
#define readPreviousSmoothedFridgeAirTemp rtc.readByte(10)

#define writeLastFridgePowerOnTime(B); rtc.readByte(50,B)
#define readLastFridgePowerOnTime rtc.readByte(50)

// === BEER STUFF ===
//
const float ci_BeerTargetTemp = 18; 
const float ci_BeerAcceptableTempVariance = 1; // determines accceptable temp tolerance +/- of ci_BeerTargetTemp

// === STATS ===
//
// how often do we sample the brew to determine the total average temp?
const int ci_sampleRateForBrewAverageInSeconds = 5;
uint32_t vi32_mostRecentFridgeAirTempAverageReadingTime = 0;
float vf_frigeAverageAirTemp = 0;
unsigned long vi_numberOfSamplesTakenForFridgeAirTempAverage = 0;
unsigned long ul_sumOfAllFridgeAirTempSamples = 0;

// ==== FT_TEMP SENSOR or frigeAirTemp ===
//
// the FT_TEMP sensor sometimes returns odd values, so we do some smoothing
const int ci_numberOfFridgeAirTempReadings = 60;
unsigned int ACCEPTABLE_SAMPLE_TOLERANCE_PERCENT = 10;
float vf_fridgeAirTempSmoothingArray[ci_numberOfFridgeAirTempReadings];
int readIndex = 0;  // the index of the current reading

// == RELAY STUFF ==
//
// c_MINIMUM_FRIDGE_REPOWER_INTERVAL how often to TRIGGER/RETRIGGER relay state.
//   we assume that we should retrigger the relay with the current
//   state as a precaution.  it also serves to stop flapping relays
const int c_MINIMUM_FRIDGE_REPOWER_INTERVAL = 300;  // seconds
const int BUTTON_PRESS_DURATION = 600; // miliseconds, must be > 40, longer = predictable.
// internal variables
uint32_t v_LAST_FRIDGE_ACTIVATION = 0;

void setup()
{  
    // setup lcd
    lcd.begin(16, 2);

    // === STARTUP SPLASH ===
    //
    clearLCD();
    lcd.setCursor( 0, 0 );  lcd.print(" = Grogomatic = ");
    lcd.setCursor( 0, 1 );  lcd.print(" =  v2.00001  = ");  
    delay(750); 
    clearLCD();
    lcd.setCursor( 0, 0 );  lcd.print("  reticulating  ");
    lcd.setCursor( 0, 1 );  lcd.print("    splines ..  ");

    // === SETUP THE STUFF ==
    //
    // setup external temp sensor
    digitalWrite(PIN_FT_TEMP, LOW);
    pinMode(PIN_FT_TEMP, INPUT);      // sets the digital pin as input (logic 1)
    pinMode(15, INPUT);

    // populate the TEMP smoothing array
    for (int thisReading = 0; thisReading < ci_numberOfFridgeAirTempReadings; thisReading++) {
      vf_fridgeAirTempSmoothingArray[thisReading] = getCurrentFridgeTemp();
      delay(100);
    }

    // setup relay control pins
    pinMode(PIN_FRIDGE_on, OUTPUT); 
    pinMode(PIN_FRIDGE_off, OUTPUT); 
    digitalWrite(PIN_FRIDGE_on, LOW);
    digitalWrite(PIN_FRIDGE_off, LOW);

    // startup sound
    //startupSound();
    clearLCD();

    // unfortunately because the user has no way of clearing these
    // we have to "clear" them here.  thusly a powercyle resets min/max/avg "memory"
    float vf_currentFridgeAirTemp = smoothedFridgeTemp();

    //rtc.writeByte(0, vf_currentFridgeAirTemp); // minimum
    writeMinimumFridgeAirTemp(vf_currentFridgeAirTemp);
    //rtc.writeByte(1, vf_currentFridgeAirTemp); //max
    writeMaximumFridgeAirTemp(vf_currentFridgeAirTemp);
    //rtc.writeByte(2, vf_currentFridgeAirTemp); // avg
    writeAverageFridgeAirTemp(vf_currentFridgeAirTemp);
    

    Serial.begin(9600);  
}

void loop()
{
    // === TALK TO SENSORS ===
    //
    // Read onboard temp from the RTC module
    // float current_RTC_temp = rtc.readTemperature() / 4; // temperature in Celsius stored in temp	
    //
    // read internal temp.   
    float vf_currentFridgeAirTemp = smoothedFridgeTemp();
    
    // === ACTIVATE FRIDGE IF REQUIRED ===
    //
    // we don't want to power the fridge more frequently than every X seconds
    // check the time now, so we can compare with when we last activated the fridge
    uint32_t now = time_t();
    
    if ((now - v_LAST_FRIDGE_ACTIVATION) > c_MINIMUM_FRIDGE_REPOWER_INTERVAL) {
        v_LAST_FRIDGE_ACTIVATION = now;  // store now in memory.  Shuld  (NVRAM!)
        if (vf_currentFridgeAirTemp >= (ci_BeerTargetTemp + ci_BeerAcceptableTempVariance)){
            fridgeOn();
        } else { 
            fridgeOff(); 
        }
    }
    
    // === CALCULATE STATS === 
    //
    float vf_minimumFridgeAirTemp = calculateMinimumFridgeAirTemp(vf_currentFridgeAirTemp);
    float vf_maximumFridgeAirTemp = calculateMaximumFridgeAirTemp(vf_currentFridgeAirTemp);
    if ((now - vi32_mostRecentFridgeAirTempAverageReadingTime) > ci_sampleRateForBrewAverageInSeconds){
        float vf_entireBrewAverageTemp = calculateAverageFridgeAirTemp(now,vf_currentFridgeAirTemp);
    }
    // deviance; is the brew within the acceptable range?
    //  Tolerance = (Measured Value - Expected Value)/Expected Value. 
    float vf_howFarDoesAverageFridgeAirTempDeviateFromTargetTemp = ci_BeerTargetTemp * abs((vf_frigeAverageAirTemp - ci_BeerTargetTemp)/ci_BeerTargetTemp);

    // === DISPLAY OUTPUTS ===
    // 
    // LCD first line
    lcd.setCursor( 0, 0 );  
    lcd.print("Av:");lcd.print(round(vf_frigeAverageAirTemp)); // average
    //lcd.print(" "); lcd.print(round(rtc.readByte(0))); // min
    lcd.print(" "); lcd.print(round(readMinimumFridgeAirTemp)); // min

    lcd.print("<"); lcd.print(vf_currentFridgeAirTemp); // current
    lcd.setCursor( 13, 0 ); // move cursor to truncate 2nd decimal of temp
    //lcd.print("<"); lcd.print(round(a)); // max
    lcd.print("<"); lcd.print(round(readMaximumFridgeAirTemp)); // max


    // LCD second line
    lcd.setCursor( 0, 1 ); 
    lcd.print("Tg:"); lcd.print(round(ci_BeerTargetTemp)); lcd.print("+/-"); lcd.print(round(ci_BeerAcceptableTempVariance));
    if ( vf_howFarDoesAverageFridgeAirTempDeviateFromTargetTemp > ci_BeerAcceptableTempVariance) { lcd.print(" :("); }
    else { lcd.print(" :)"); }

    // == TO SERIAL ==
    //
    //Serial.print("vf_howFarDoesAverageFridgeAirTempDeviateFromTargetTemp: "); Serial.println(vf_howFarDoesAverageFridgeAirTempDeviateFromTargetTemp); 
    //Serial.print("ul_sumOfAllFridgeAirTempSamples:");  Serial.println(ul_sumOfAllFridgeAirTempSamples); 
    //Serial.print("vf_frigeAverageAirTemp:");  Serial.println(vf_frigeAverageAirTemp);
}

float calculateAverageFridgeAirTemp(int vi32_mostRecentFridgeAirTempAverageReadingTime, int vf_currentFridgeAirTemp){
    //

    vi_numberOfSamplesTakenForFridgeAirTempAverage++;
    ul_sumOfAllFridgeAirTempSamples = ul_sumOfAllFridgeAirTempSamples + vf_currentFridgeAirTemp;  
    vf_frigeAverageAirTemp = ul_sumOfAllFridgeAirTempSamples / vi_numberOfSamplesTakenForFridgeAirTempAverage;
    writeAverageFridgeAirTemp(vf_frigeAverageAirTemp); 

    Serial.print("temp:"); Serial.print(vf_currentFridgeAirTemp);
    Serial.print(" samples:"); Serial.print(vi_numberOfSamplesTakenForFridgeAirTempAverage);
    Serial.print(" sum:");  Serial.print(ul_sumOfAllFridgeAirTempSamples); 
    Serial.print(" average:");  Serial.println(vf_frigeAverageAirTemp);

    return vf_frigeAverageAirTemp;
}

void fridgeOff() {
    lcd.setCursor( 0, 1 );  lcd.print("STOPPING FRIDGE!");
    // SEND ON TO THE FRIDGE
    digitalWrite(13, LOW);
    digitalWrite(PIN_FRIDGE_off, HIGH);
    delay(BUTTON_PRESS_DURATION);
    digitalWrite(PIN_FRIDGE_off, LOW);
    delay(300);

    clearLCD();lcd.setCursor( 13, 1 ); lcd.print("OFF");  
}

void fridgeOn() {
    lcd.setCursor( 0, 1 );  lcd.print("POWERING FRIDGE!");
    // SEND ON TO THE FRIDGE
    digitalWrite(13, HIGH);
    digitalWrite(PIN_FRIDGE_on, HIGH);
    delay(BUTTON_PRESS_DURATION);
    digitalWrite(PIN_FRIDGE_on, LOW);
    delay(300);

    clearLCD();lcd.setCursor( 13, 1 ); lcd.print("ON"); 
}

float smoothedFridgeTemp(){
    // this function does two things.  it calculates an average of 
    //  the temps, and calls that smoothedFridgeTemp.  new values are 
    //  compared to the smoothedFridgeTemp, and if it deviates more than
    //  an acceptable tolerance, then are discarded.

    // determine previous smoothed temp, so we can use it to compare
    //  to the current fridge temp.
    float f_previousSmoothedFridgeAirTemp = readPreviousSmoothedFridgeAirTemp;
    float vf_currentFridgeAirTemp = getCurrentFridgeTemp();

    // TOLERANCE - Usually provide as a percentage of the expected value. 
    //  It can be plus or minus. 
    //  Tolerance = (Measured Value - Expected Value)/Expected Value. 
    if (abs(((vf_currentFridgeAirTemp - f_previousSmoothedFridgeAirTemp) / f_previousSmoothedFridgeAirTemp)) < ACCEPTABLE_SAMPLE_TOLERANCE_PERCENT ) {
        // then this sample is ok to keep, and add a new reading to the array
        vf_fridgeAirTempSmoothingArray[readIndex] = getCurrentFridgeTemp();
    } else {
        // this sample is too wrong, don't add it.
    }
    
    // Now we have an array of values we consider "acceptable" we
    // iterate through the array to calculate the average:
    float total = 0;
    for (int i = 0; i < ci_numberOfFridgeAirTempReadings; i++) {
      total = total + vf_fridgeAirTempSmoothingArray[i];
    }
    float smoothedFridgeTemp = total / ci_numberOfFridgeAirTempReadings;

    // advance to the next position in the array for next time
    // or if we're at the end of the array, wrap around
    readIndex = readIndex + 1;
    if (readIndex >= ci_numberOfFridgeAirTempReadings) { readIndex = 0; }
    
    // output 
    writePreviousSmoothedFridgeAirTemp(smoothedFridgeTemp);
    return smoothedFridgeTemp;
}

float calculateMinimumFridgeAirTemp(float vf_currentFridgeAirTemp){
    float vf_minimumFridgeAirTemp = readMinimumFridgeAirTemp;

    if ( round(vf_currentFridgeAirTemp) < round(vf_minimumFridgeAirTemp) ) {
        vf_minimumFridgeAirTemp = vf_currentFridgeAirTemp
        writeMinimumFridgeAirTemp(vf_minimumFridgeAirTemp);
        return vf_currentFridgeAirTemp;
    } else {
        return vf_minimumFridgeAirTemp;
    }
}

float calculateMaximumFridgeAirTemp(float vf_currentFridgeAirTemp){
    float vf_maximumFridgeAirTemp = readMaximumFridgeAirTemp;

    if ( round(vf_currentFridgeAirTemp) > round(vf_maximumFridgeAirTemp) ) {
        vf_maximumFridgeAirTemp = vf_currentFridgeAirTemp
        writeMaximumFridgeAirTemp(vf_maximumFridgeAirTemp);
        return vf_currentFridgeAirTemp;
    } else {
        return vf_maximumFridgeAirTemp;
    }
}

void clearLCD(){
    // write nothing all over the LCD to clear it.

    lcd.setCursor(0, 0); lcd.print("                ");
    lcd.setCursor(0, 1); lcd.print("                ");
}

/*
void startupSound(){
    // test sound
    int melody[] = { NOTE_C4, NOTE_G3, NOTE_G3, NOTE_A3, NOTE_G3, 0, NOTE_B3, NOTE_C4};
    // note durations: 4 = quarter note, 8 = eighth note, etc.:
    int noteDurations[] = { 4, 8, 8, 4, 4, 4, 4, 4};

    // iterate over the notes of the melody:
    for (int thisNote = 0; thisNote < 8; thisNote++) {
        // to calculate the note duration, take one second
        // divided by the note type.
        //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
        int noteDuration = 1000 / noteDurations[thisNote];
        //tone(PIN_BUZZER, melody[thisNote], noteDuration);

        // to distinguish the notes, set a minimum time between them.
        // the note's duration + 30% seems to work well:
        int pauseBetweenNotes = noteDuration * 1.30;
        delay(pauseBetweenNotes);
        // stop the tone playing:
        //noTone(PIN_BUZZER);
  }
}
*/

void OneWireReset (int Pin) // reset.  Should improve to act as a presence pulse
{
    digitalWrite (Pin, LOW);
    pinMode (Pin, OUTPUT);        // bring low for 500 us
    delayMicroseconds (500);
    pinMode (Pin, INPUT);
    delayMicroseconds (500);
}

void OneWireOutByte (int Pin, byte d) // output byte d (least sig bit first).
{
    byte n;

    for (n=8; n!=0; n--)
    {
        if ((d & 0x01) == 1)  // test least sig bit
    {
        digitalWrite (Pin, LOW);
        pinMode (Pin, OUTPUT);
        delayMicroseconds (5);
        pinMode (Pin, INPUT);
        delayMicroseconds (60);
    }
    else
    {
        digitalWrite (Pin, LOW);
        pinMode (Pin, OUTPUT);
        delayMicroseconds (60);
        pinMode (Pin, INPUT);
    }

    d = d>>1; // now the next bit is in the least sig bit position.
    }
}


byte OneWireInByte (int Pin) // read byte, least sig byte first
{
    byte d, n, b;

    for (n=0; n<8; n++)
    {
        digitalWrite (Pin, LOW);
        pinMode (Pin, OUTPUT);
        delayMicroseconds (5);
        pinMode (Pin, INPUT);
        delayMicroseconds (5);
        b = digitalRead (Pin);
        delayMicroseconds (50);
        d = (d >> 1) | (b<<7); // shift d to right and insert b in most sig bit position
    }
    return (d);
}

float getCurrentFridgeTemp()
{
    char temp[6];
    int HighByte, LowByte, TReading, Tc_100, sign, whole, fract;

    OneWireReset (PIN_FT_TEMP);
    OneWireOutByte (PIN_FT_TEMP, 0xcc);
    OneWireOutByte (PIN_FT_TEMP, 0x44); // perform temperature conversion, strong pullup for one sec

    OneWireReset (PIN_FT_TEMP);
    OneWireOutByte (PIN_FT_TEMP, 0xcc);
    OneWireOutByte (PIN_FT_TEMP, 0xbe);

    LowByte = OneWireInByte (PIN_FT_TEMP);
    HighByte = OneWireInByte (PIN_FT_TEMP);
    TReading = (HighByte << 8) + LowByte;
    sign = TReading & 0x8000;  // test most sig bit

    if (sign) // negative
    {
        TReading = (TReading ^ 0xffff) + 1; // 2's comp
    }
    Tc_100 = (6 * TReading) + TReading / 4;    // multiply by (100 * 0.0625) or 6.25

    whole = Tc_100 / 100;  // separate off the whole and fractional portions
    fract = Tc_100 % 100;

    temp[0] = (whole-(whole/100)*100)/10 +'0' ;
    temp[1] = whole-(whole/10)*10 +'0';
    temp[2] = '.';
    temp[3] = fract/10 +'0';
    temp[4] = fract-(fract/10)*10 +'0';
    temp[5] = '\0';

    float vf_currentFridgeAirTemp = atof(temp);

    return vf_currentFridgeAirTemp;
}

uint32_t time_t(){
    // assemble time elements into time_t 

    #define LEAP_YEAR(Y)     ( ((1970+Y)>0) && !((1970+Y)%4) && ( ((1970+Y)%100) || !((1970+Y)%400) ) )
    #define SECS_PER_DAY 86400
    #define SECS_PER_HOUR 3600
    #define SECS_PER_MIN 60

    static const uint8_t monthDays[]={31,28,31,30,31,30,31,31,30,31,30,31}; // API starts months from 1, this array starts from 0

    RTCTime time;
    RTCDate date;
    rtc.readTime(&time);
    rtc.readDate(&date);

    int i;
    uint32_t seconds;

    // seconds from 1970 till 1 jan 00:00:00 of the given year
    seconds = (date.year-1970)*(SECS_PER_DAY * 365);

    for (i = 1971; i < date.year; i++) {
        if (LEAP_YEAR(i)) {
            seconds += SECS_PER_DAY;   // add extra days for leap years
            
        }
    }
    // add days for this year, months start from 1
    for (i = 1; i < date.month; i++) {
        if ( (i == 2) && LEAP_YEAR(date.year)) { 
            seconds += SECS_PER_DAY * 29;
        } else {
            seconds += SECS_PER_DAY * monthDays[i];  //monthDay array starts from 0
        }
    }

    seconds+= (date.day-1) * SECS_PER_DAY;  
    seconds+= time.hour * SECS_PER_HOUR;  
    seconds+= time.minute * SECS_PER_MIN; 
    seconds+= time.second;                

    return seconds; 
}