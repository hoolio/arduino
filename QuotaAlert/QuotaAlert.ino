// (c) Copyright 2010-2012 MCQN Ltd.
// Released under Apache License, version 2.0
//
// Simple example to show how to use the HttpClient library
// Get's the web page given at http://<kHostname><kPath> and
// outputs the content to the serial port

#include <SPI.h>
#include <HttpClient.h>
#include <Ethernet.h>
#include <EthernetClient.h>

// pin assignments
// RGB LED Outputs
int C_RedLED = 3;   // Red LED,
int C_GreenLED = 6;  // Green LED,
int C_BlueLED = 5;  // Blue LED,

// Name of the server we want to connect to
const char kHostname[] = "10.1.1.3";
// Path to download (this is the bit after the hostname in the URL
// that you want to download
const char kPath[] = "/";

byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };

// Number of milliseconds to wait without receiving any data before we give up
const int kNetworkTimeout = 30 * 1000;
// Number of milliseconds to wait if no data is available before trying again
const int kNetworkDelay = 1000;

void setup()
{
  // start serial port:
  Serial.begin(9600);

  // sets the pins as output
  pinMode(C_RedLED, OUTPUT);
  pinMode(C_GreenLED, OUTPUT);
  pinMode(C_BlueLED, OUTPUT);

  // start the Ethernet connection:
  Serial.println("Attempting to configure Ethernet using DHCP");
  digitalWrite(C_RedLED, HIGH);    
  digitalWrite(C_GreenLED, LOW);

  if (Ethernet.begin(mac) == 0) {
    Serial.println("..failed to configure Ethernet using DHCP");
    //Ethernet.begin(mac, ip); // <- DHCP failed, so try using a fixed IP address:
    while (1) {}; // loop forever.
  } else {
    // networking working OK, green light ON.
    Serial.print("..networking up OK; ");
    digitalWrite(C_GreenLED, HIGH);
    digitalWrite(C_RedLED, LOW);
    printIPAddress();
    Serial.println();
    delay(4000);
  }
}

void loop()
{
  int err = 0;
  String result;

  EthernetClient c;
  HttpClient http(c);

  err = http.get(kHostname, kPath);
  if (err == 0)
  {
    Serial.print("Connected to "); Serial.print(kHostname); Serial.println(" OK ");

    err = http.responseStatusCode();
    if (err >= 0)
    {
      Serial.print(" ..received status code: ");
      Serial.println(err);

      // Usually you'd check that the response code is 200 or a
      // similar "success" code (200-299) before carrying on,
      // but we'll print out whatever response we get

      err = http.skipResponseHeaders();
      if (err >= 0)
      {
        int bodyLen = http.contentLength();
        Serial.print(" ..content length is: ");
        Serial.print(bodyLen);

        // this is commented out as we are collecting
        //  the output in the string called result
        //Serial.println("..body returned the following:");
        //Serial.println();

        // Now we've got to the body, so we can print it out
        unsigned long timeoutStart = millis();
        char c;
        // Whilst we haven't timed out & haven't reached the end of the body
        while ( (http.connected() || http.available()) &&
                ((millis() - timeoutStart) < kNetworkTimeout) )
        {
          if (http.available())
          {
            c = http.read();
            // Print out this character
            //Serial.print(c);
            result = result + c;

            bodyLen--;
            // We read something, reset the timeout counter
            timeoutStart = millis();
          }
          else
          {
            // We haven't got any data, so let's pause to allow some to
            // arrive
            delay(kNetworkDelay);
          }
        }
      }
      else
      {
        Serial.print(" ..failed to skip response headers: ");
        Serial.println(err);
      }
    }
    else
    {
      Serial.print(" ..getting response failed: ");
      Serial.println(err);
    }
  }
  else
  {
    Serial.print(" ..connect failed: ");
    Serial.println(err);
  }
  http.stop();

  // output what we have
  Serial.println();
  Serial.print(" ..captured result as: "); Serial.print(result);

  // set the RGB LEDs accordingly
  lightRGBled(result);

  // wait a bit before we check again
  Serial.println(" ..sleeping 3 seconds..");

  delay(3000);
}

void printIPAddress()
{
  Serial.print("My IP address: ");
  for (byte thisByte = 0; thisByte < 4; thisByte++) {
    // print the value of each byte of the IP address:
    Serial.print(Ethernet.localIP()[thisByte], DEC);
    Serial.print(".");
  }

  Serial.println();
}

void lightRGBled(String result)
{
  int result_int = result.toInt();
  Serial.print(" ..calculating colors for result_int of "); Serial.println(result_int);

  // we are assuming result_int is a percentage.  we're not checking that.

  // calculate red and green values.
  //   a result of 100 would mean 100 green
  //   a result of 0 would mean 100 red.
  //   .. so we need to work out how much of each for intervening values.

  //
  int redVal = 100 - result_int;
  int greenVal = result_int;

  // Write current values to LED pins
  analogWrite(C_RedLED, redVal);
  analogWrite(C_GreenLED, greenVal);
  analogWrite(C_BlueLED, 0);
}

