/* == Hondameter ==
 * 
 * A thing to monitor and display various metrics of a motorcycle
 * during operation.
 * 
 * Currently implimented
 * x) IRTemp for ambient and direct (IR) temperature of engine
 * x) Real time clock module for runtime and clock
 * x) LCD Display for output
 * 
 * TO DO
 * x) Replace the IRTemp library with that quicker one from wherever
*/

// include the libraries
#include <Wire.h> // ??
#include <LiquidCrystal.h> // LCD
#include "IRTemp.h" // IRTEMP
#include <SoftI2C.h> // RTC
#include <DS3232RTC.h> // RTC

// pin assignments
char PIN_RTC_SDA  = A4;
char PIN_RTC_SCL  = A5;
int PIN_IRTEMP_DATA    = 10;
int PIN_IRTEMP_CLOCK   = 2;  // Must be either pin 2 or pin 3
int PIN_IRTEMP_ACQUIRE = 11;
LiquidCrystal lcd( 8, 9, 4, 5, 6, 7 );

// Setup Real Time Clock (RTC)
SoftI2C i2c(PIN_RTC_SDA, PIN_RTC_SCL); // assign pins to SDA and SCL
DS3232RTC rtc(i2c);
RTCTime time;

// setup InfraRed Temp Module (IRTemp)
//bool SCALE=false;  // Celcius: false, Farenheit: true
IRTemp irTemp(PIN_IRTEMP_ACQUIRE, PIN_IRTEMP_CLOCK, PIN_IRTEMP_DATA);

// other globlal debug foo 
int tickCount = 0;

void setup()
{  
  lcd.begin(16, 2);
  
  lcd.setCursor( 0, 0 );  lcd.print(" = Hondameter = ");
  lcd.setCursor( 0, 1 );  lcd.print(" =  starting  = ");  
  
  delay(100); 
  //Serial.begin(9600);  
}

void loop()
{
  // read ir and ambient temps from the IRTemp module
  int irTemperature = irTemp.getIRTemperature(false);
  int ambientTemperature = irTemp.getAmbientTemperature(false); 
  // read onboard temp from the RTC module
  //int t1= rtc.readTemperature(); // retrieve the value from the DS3232
  //int RTCTemp = t1 / 4; // temperature in Celsius stored in temp  
  
  // get the time
  rtc.readTime(&time);
  
  // increment tick count (fake runtime)
  tickCount++;  
  
  // output clocks and temp to LCD
  clearLCD();
  lcd.setCursor( 2, 0 );  
  if ( time.hour < 10 ) { lcd.print("0"); } lcd.print(time.hour); lcd.print(":"); 
  if ( time.minute < 10 ) { lcd.print("0"); } lcd.print(time.minute); lcd.print(":"); 
  if ( time.second < 10 ) { lcd.print("0"); } lcd.print(time.second);
  // output 
  lcd.setCursor( 12, 0 );  lcd.print(tickCount);
    
    // output the temps on the second line.
  lcd.setCursor( 0, 1 );  
  lcd.print(" Eng:"); lcd.print(irTemperature);
  lcd.print("  Air:"); lcd.print(ambientTemperature);   
  
  //delay(300);
}

// write nothing all over the LCD to clear it.
void clearLCD(){
  lcd.setCursor(0, 0); lcd.print("                ");
  lcd.setCursor(0, 1); lcd.print("                ");
}
