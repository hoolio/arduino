 /*
  Cosm sensor client
 
 This sketch connects an analog sensor to Cosm (http://www.cosm.com)
 using a Wiznet Ethernet shield. You can use the Arduino Ethernet shield, or
 the Adafruit Ethernet shield, either one will work, as long as it's got
 a Wiznet Ethernet module on board.
 
 This example has been updated to use version 2.0 of the Cosm.com API. 
 To make it work, create a feed with a datastream, and give it the ID
 sensor1. Or change the code below to match your feed.
 
  Circuit:
 * Analog sensor attached to analog in 0
 * Ethernet shield attached to pins 10, 11, 12, 13
 
 created 15 March 2010
 updated 16 Mar 2012
 by Tom Igoe with input from Usman Haque and Joe Saavedra
 
http://arduino.cc/en/Tutorial/PachubeClient
 This code is in the public domain.
 
 */

#include <SPI.h>
#include <Dhcp.h>
#include <Dns.h>
#include <Ethernet.h>
#include <EthernetClient.h>
#include <EthernetServer.h>
#include <EthernetUdp.h>

#define APIKEY         "0frJeBL-tlfOv_oKXTjYq9WnFQiSAKx2QzJ0SHgwbE9Laz0g" // your cosm api key
#define FEEDID         70212 // your feed ID
#define USERAGENT      "Cosm Arduino Example (70212)" // user agent is the project name

// assign a MAC address for the ethernet controller.
// Newer Ethernet shields have a MAC address printed on a sticker on the shield
// fill in your address here:
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};

// fill in an available IP address on your network here,
// for manual configuration:
IPAddress ip(10,1,1,20);
// initialize the library instance:
EthernetClient client;

// if you don't want to use DNS (and reduce your sketch size)
// use the numeric IP instead of the name for the server:
IPAddress server(216,52,233,121);      // numeric IP for api.cosm.com
//char server[] = "api.cosm.com";   // name address for cosm API

unsigned long lastConnectionTime = 0;          // last time you connected to the server, in milliseconds
boolean lastConnected = false;                 // state of the connection last time through the main loop
const unsigned long postingInterval = 10*1000; //delay between updates to Cosm.com

// pin assignments
const int C_LDRPin = A2;
const int C_UNUSED = A3; // <- spare!!
const int C_BlackOutdoorThermisterPin = A4;
const int C_RedIndoorThermisterPin = A5;

const int C_RedLED = 7; 
const int C_OrangeLED = 8; 
const int C_GreenLED = 9; 

void setup() {
  pinMode(C_RedLED, OUTPUT);      
  pinMode(C_OrangeLED, OUTPUT);
  pinMode(C_GreenLED, OUTPUT);
  
  for (int i=0; i<5;i++){
     digitalWrite(C_GreenLED, HIGH);  delay(50); digitalWrite(C_GreenLED, LOW); 
     digitalWrite(C_OrangeLED, HIGH); delay(50); digitalWrite(C_OrangeLED, LOW);
     digitalWrite(C_RedLED, HIGH);    delay(50); digitalWrite(C_RedLED, LOW);
  }
  
  // start serial port:
  Serial.begin(9600);
 
 // start the Ethernet connection:
 Serial.println("Attempting to configure Ethernet using DHCP");
 digitalWrite(C_OrangeLED, HIGH);

 if (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");
    Ethernet.begin(mac, ip); // <- DHCP failed, so try using a fixed IP address:
    digitalWrite(C_RedLED, HIGH);
    digitalWrite(C_OrangeLED, LOW);
    while(1){}; // loop forever.
  } else {
    // networking working OK, green light ON.
    Serial.println("Networking working.");
    digitalWrite(C_GreenLED, HIGH);
    digitalWrite(C_OrangeLED, LOW);
  }
}

void loop() {
  // view this feed at https://cosm.com/feeds/70212
  
  // read the analog sensor(s):
  int V_IndoorTemp = Thermistor(analogRead(C_RedIndoorThermisterPin));
  int V_OutdoorTemp = Thermistor(analogRead(C_BlackOutdoorThermisterPin));
  int V_LightLevel = Thermistor(analogRead(C_LDRPin)); 

  // convert the data to a String to send it:
  String dataString = "Inside,"; dataString += V_IndoorTemp;
  // you can append multiple readings to this String if your
  // Cosm feed is set up to handle multiple values:
  dataString += "\nOutside,";  dataString += V_OutdoorTemp;
  dataString += "\nLight,";  dataString += V_LightLevel;
  
  // if there's incoming data from the net connection.
  // send it out the serial port.  This is for debugging
  // purposes only:
  if (client.available()) {
    char c = client.read();
    Serial.print(c);
  }

  // if there's no net connection, but there was one last time
  // through the loop, then stop the client:
  if (!client.connected() && lastConnected) {
    Serial.println();
    Serial.println("disconnecting.");
    client.stop();
    digitalWrite(C_OrangeLED, LOW);
  }

  // if you're not connected, and ten seconds have passed since
  // your last connection, then connect again and send data:
  if(!client.connected() && (millis() - lastConnectionTime > postingInterval)) {
    sendData(dataString);
    digitalWrite(C_OrangeLED, HIGH);
  }
  
  // store the state of the connection for next time through
  // the loop:
  lastConnected = client.connected();
}

// this method makes a HTTP connection to the server:
void sendData(String thisData) {
  // if there's a successful connection:
  if (client.connect(server, 80)) {
    Serial.println("connecting...");
    // send the HTTP PUT request:
    client.print("PUT /v2/feeds/");
    client.print(FEEDID);
    client.println(".csv HTTP/1.1");
    client.println("Host: api.cosm.com");
    client.print("X-ApiKey: ");
    client.println(APIKEY);
    client.print("User-Agent: ");
    client.println(USERAGENT);
    client.print("Content-Length: ");
    client.println(thisData.length());

    // last pieces of the HTTP PUT request:
    client.println("Content-Type: text/csv");
    client.println("Connection: close");
    client.println();

    // here's the actual content of the PUT request:
    client.println(thisData);
  }
  else {
    // if you couldn't make a connection:
    Serial.println("connection failed");
    Serial.println();
    Serial.println("disconnecting.");
    client.stop();
  }
  // note the time that the connection was made or attempted:
  lastConnectionTime = millis();
}


// This method calculates the number of digits in the
// sensor reading.  Since each digit of the ASCII decimal
// representation is a byte, the number of digits equals
// the number of bytes:

int getLength(int someValue) {
  // there's at least one byte:
  int digits = 1;
  // continually divide the value by ten, 
  // adding one to the digit count for each
  // time you divide, until you're at 0:
  int dividend = someValue /10;
  while (dividend > 0) {
    dividend = dividend /10;
    digits++;
  }
  // return the number of digits:
  return digits;
}

float Thermistor(int RawADC) {
  //float vcc = 4.91;         // only used for display purposes, if used set to the measured Vcc.
  float pad = 9950;         // balance/pad resistor value, set this to the measured resistance of your pad resistor
  float thermr = 10000;      // thermistor nominal resistance
  long Resistance;  
  float Temp;  // Dual-Purpose variable to save space.

  Resistance=((1024 * pad / RawADC) - pad); 
  Temp = log(Resistance); // Saving the Log(resistance) so not to calculate  it 4 times later
  Temp = 1 / (0.001129148 + (0.000234125 * Temp) + (0.0000000876741 * Temp * Temp * Temp));
  Temp = Temp - 273.15;  // Convert Kelvin to Celsius                      

  //Temp = Temp - 20; // Hoolios unforgivably bad conversion factor when using the 4.k thermistor
  Temp = Temp - 3; // Hoolios unforgivably bad conversion factor when using the 10k thermistor.

  return Temp;   // Return the Temperature
}

    
