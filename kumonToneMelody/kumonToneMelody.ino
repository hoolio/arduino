/*
  Melody

 Plays a melody

 circuit:
 * 8-ohm speaker on digital pin 8

 created 21 Jan 2010
 modified 30 Aug 2011
 by Tom Igoe

This example code is in the public domain.

 http://arduino.cc/en/Tutorial/Tone

 */
 #include "pitches.h"

// // notes in the melody:
// int melody[] = {
//   NOTE_C4, NOTE_G3,NOTE_G3, NOTE_A3, NOTE_G3,0, NOTE_B3, NOTE_C4};
//
// // note durations: 4 = quarter note, 8 = eighth note, etc.:
// int noteDurations[] = {
//   4, 8, 8, 4,4,4,4,4 };

#define n5 NOTE_C4
#define n6 NOTE_D4
#define n7 NOTE_E4
#define n8 NOTE_F4
#define n9 NOTE_G4
#define n10 NOTE_A4
#define n11 NOTE_B4
#define n12 NOTE_C5

// notes in the melody:
//int melody[] = {n5, n6, n7, n8, n9, n10, n11, n12};
int melody[] = {n7, n7, n8, n9, n8, n8, n10, n9, n9, n10, n11,
                n7, n8, n9, n9, n10, n8, n10, n10, n11, n10, n11};

// note durations: 4 = quarter note, 8 = eighth note, etc.:
int constNoteDuration = 5;

void setup() {
  // iterate over the notes of the melody:
  for (int thisNote = 0; thisNote < 22; thisNote++) {

    // to calculate the note duration, take one second
    // divided by the note type.
    //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
    int noteDuration = 1000/constNoteDuration;
    tone(8, melody[thisNote],noteDuration);

    // to distinguish the notes, set a minimum time between them.
    // the note's duration + 30% seems to work well:
    int pauseBetweenNotes = noteDuration * 1.30;
    delay(pauseBetweenNotes);
    // stop the tone playing:
    noTone(8);
  }
}

void loop() {
  // no need to repeat the melody.
}
