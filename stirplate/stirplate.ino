int activityLED = 5;
int enA = 10;
int in1 = 9;
int in2 = 8;
int speedPin = A0;
int frequencyPin = A2; 

// speed settings, set for H on Antec fan
int pwmFrequency = 0; // valid range is 0 -> 4, maps to 1,8,64,256,1024
int minSpeed = 60;
int maxSpeed = 255; // max limit is 255
int setSpeed = 0; // from pot on A0

//int rampUpStartSpeed = minSpeed + 30;
//int rampUpStepDelay = 25;
//int rampUpEndSpeed = maxSpeed - 10;

int stictionStartSpeed = maxSpeed;
int stictionStartDelay = 4000;
int rampDownStartSpeed = maxSpeed;
int rampDownStepDelay = 15;
int rampDownEndSpeed = 0; // to be set to set speed

void setup()
{
  Serial.println("setup()");
  // set all the motor control pins to outputs
  pinMode(enA, OUTPUT); // PWM to motor controller
  pinMode(activityLED, OUTPUT);
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  pinMode(13, OUTPUT);
  pinMode(speedPin, INPUT);
  Serial.begin(9600);

  // allow user to reposition vesel
  delay(1500);
  
  // stictionStart
  // flat out
  modulateSpeed(stictionStartSpeed,pwmFrequency);
  // wait for liquid to catch up
  delay(stictionStartDelay);
  // slow to set speed
  setSpeed = map(analogRead(speedPin), 1023, 0, minSpeed, maxSpeed);
  rampDownEndSpeed = setSpeed;
  ramp_down(rampDownStartSpeed,rampDownStepDelay,rampDownEndSpeed); 

  Serial.println("loop()");
}
void ramp_down(int rampDownStartSpeed, int rampDownStepDelay, int rampDownEndSpeed){
  // turn on motors
  Serial.print("ramp_down("); Serial.print(rampDownStartSpeed);
  Serial.print(","); Serial.print(rampDownStepDelay);
  Serial.print(","); Serial.print(rampDownEndSpeed); Serial.println(")");
 
  // rampup
  motor_on();
  for (int i = rampDownStartSpeed; i >= rampDownEndSpeed; i--){
    modulateSpeed(i,pwmFrequency);
    delay(rampDownStepDelay);   
  }
}

void ramp_up(int rampStartSpeed, int ramp_rate, int rampEndSpeed){
  // turn on motors
  Serial.print("ramp_up("); Serial.print(rampStartSpeed);
  Serial.print(","); Serial.print(ramp_rate);
  Serial.print(","); Serial.print(rampEndSpeed); Serial.println(")");
 
  // rampup
  motor_on();
  for (int i = rampStartSpeed; i <= rampEndSpeed; i++){
    modulateSpeed(i,pwmFrequency);
    delay(ramp_rate);   
  }
}
void motor_off(){
  // turn off motors
  Serial.println("motor_off()");
  analogWrite(enA, 0);
  digitalWrite(13, LOW); 
  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH); 
  delay(5000);
}

void motor_on(){
  //Serial.println("motor_on()");
  digitalWrite(in1, HIGH);
  digitalWrite(in2, LOW); 
  digitalWrite(13, HIGH);
}

void modulateSpeed(int setSpeed,int pwmFrequency){
  motor_on();
  Serial.print("..modulateSpeed(");
  Serial.print(setSpeed);
  Serial.print(",");
  Serial.print(pwmFrequency);
  Serial.println(")");
  
  analogWrite(enA, setSpeed);
  int lightIntensity = map(setSpeed, minSpeed, maxSpeed, 1, 255);
  analogWrite(activityLED, lightIntensity);
  setPwmFrequency(enA, pwmFrequency);
}

void setPwmFrequency(int pin, int divisor) {
  // http://playground.arduino.cc/Code/PwmFrequency
  // The base frequency for pins 3, 9, 10, and 11 is 31250 Hz.
  // The divisors available on pins 5, 6, 9 and 10 are: 1, 8, 64, 256, and 1024.
  // eg: Set pin 10's PWM frequency to 31 Hz (31250/1024 = 31)
  //Serial.print("setPwmFrequency("); Serial.print(pin);
  //Serial.print(","); Serial.print(divisor); 
  //Serial.println(")");

  byte mode;
  if(pin == 5 || pin == 6 || pin == 9 || pin == 10) {
    switch(divisor) {
      case 0: mode = 0x01; break;
      case 1: mode = 0x02; break;
      case 2: mode = 0x03; break;
      case 3: mode = 0x04; break;
      case 4: mode = 0x05; break;
      //case 1: mode = 0x01; break;
      //case 8: mode = 0x02; break;
      //case 64: mode = 0x03; break;
      //case 256: mode = 0x04; break;
      //case 1024: mode = 0x05; break;
      default: return;
    }
    if(pin == 5 || pin == 6) {
      TCCR0B = TCCR0B & 0b11111000 | mode;
    } else {
      TCCR1B = TCCR1B & 0b11111000 | mode;
    }
  } else if(pin == 3 || pin == 11) {
    switch(divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 32: mode = 0x03; break;
      case 64: mode = 0x04; break;
      case 128: mode = 0x05; break;
      case 256: mode = 0x06; break;
      case 1024: mode = 0x07; break;
      default: return;
    }
    TCCR2B = TCCR2B & 0b11111000 | mode;
  }
}
void loop(){
  // get speed from pot and map to acceptable range
  setSpeed = analogRead(speedPin);
  setSpeed = map(setSpeed, 1023, 0, minSpeed, maxSpeed);
  
  //pwmFrequency = analogRead(frequencyPin);
  //pwmFrequency = map(pwmFrequency, 0, 1023, 0, 4);

  modulateSpeed(setSpeed,pwmFrequency);

  delay(100);

  //motor_off(); 
}
