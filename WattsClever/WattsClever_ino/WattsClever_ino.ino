/*
  Example for different sending methods
  
  http://code.google.com/p/rc-switch/
*/

#include <RCSwitch.h>

RCSwitch mySwitch = RCSwitch();
int C_Button=2;
int C_OutLED=13;

void setup() {

  Serial.begin(9600);
  
  // Transmitter is connected to Arduino Pin #10  
  mySwitch.enableTransmit(10);

  // Optional set pulse length.
  //mySwitch.setPulseLength(320);
  mySwitch.setPulseLength(400);
  
  // Optional set protocol (default is 1, will work for most outlets)
  //mySwitch.setProtocol(2);
  
  // Optional set number of transmission repetitions.
  //mySwitch.setRepeatTransmit(15);
 
  pinMode(C_Button, INPUT);     
  pinMode(C_OutLED, OUTPUT);  
  
  Serial.begin(9600);
  Serial.println("ready-ish..");
}

void loop() {
  int buttonState = digitalRead(C_Button);
  
  if (buttonState == HIGH) {
    // these functions include a delay()    
    triStateCodeAll(); 
    //triStateCode1(); // on maybe??
    //triStateCode2(); // off maybe??
    
    //TypeA_WithDIPSwitches();
    //binaryCode();
    //decimalCode();

    Serial.println("ready-ish again.."); 
  } else {
    digitalWrite(C_OutLED, LOW);
  }  
}

void triStateCodeAll(){
    Serial.println("sending using tri-state code");
    digitalWrite(C_OutLED, HIGH); 
    mySwitch.sendTriState("00000FFF0F0F");
    delay(1000);  
    mySwitch.sendTriState("00000FFF0FF0");
    delay(1000);  
    digitalWrite(C_OutLED, LOW);
}

void triStateCode1(){
    Serial.println("sending using tri-state code");
    digitalWrite(C_OutLED, HIGH); 
    mySwitch.sendTriState("00000FFF0F0F");
    delay(1000);   
    digitalWrite(C_OutLED, LOW);
}

void triStateCode2(){
    Serial.println("sending using tri-state code");
    digitalWrite(C_OutLED, HIGH);   
    mySwitch.sendTriState("00000FFF0FF0");
    delay(1000);  
    digitalWrite(C_OutLED, LOW);
}

void TypeA_WithDIPSwitches(){
    Serial.println("sending TypeA_WithDIPSwitches");
    digitalWrite(C_OutLED, HIGH); 
    mySwitch.switchOn("11111", 4); 
    delay(1000);  
    mySwitch.switchOff("11111", 4);
    delay(1000);
    digitalWrite(C_OutLED, LOW);
}

void binaryCode(){
    Serial.println("sending using binary code");
    digitalWrite(C_OutLED, HIGH); 
    mySwitch.send("000000000001010100010001");
    delay(1000);  
    mySwitch.send("000000000001010100010100");
    delay(1000);
    digitalWrite(C_OutLED, LOW);
}

void decimalCode(){
    Serial.println("sending using decimal code");
    digitalWrite(C_OutLED, HIGH); 
    mySwitch.send(5393, 24);
    delay(1000);  
    mySwitch.send(5396, 24);
    delay(1000);  
    digitalWrite(C_OutLED, LOW);
}
