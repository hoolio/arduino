// Homy Control
 
#include <ctype.h>
#include <Ports.h>
#include <RF12.h>
 
#define Tck 350
 
long devIds[16] = {28202, 28202, 28202, 28202, 912938, 912938, 912938, 912938, 416811, 416811, 416811, 416811};
int chanIds[16] = {    0,     4,     2,     1,      0,      4,      2,      1,      0,      4,      2,      1};
 
static void sendHigh()
{
  rf12_onOff(1);
  delayMicroseconds(Tck+150);
  rf12_onOff(0);
  delayMicroseconds(3*Tck-200);
}
 
static void sendLow()
{
  rf12_onOff(1);
  delayMicroseconds(3*Tck+150);
  rf12_onOff(0);
  delayMicroseconds(Tck-200);
}
 
static void sendBit(boolean data)
{
  if (data)
    sendHigh();
  else
    sendLow();
}
 
static void sendPreamble()
{
  rf12_onOff(1);
  delayMicroseconds(Tck+150);
  rf12_onOff(0);
  delayMicroseconds(31*Tck-200);
}
 
static void sendPacket(long C, int D)
{
    sendPreamble();
    for(int b = 19; b > -1; b--) sendBit((C >> b) & 0b1);
    for(int b = 3; b > -1; b--) sendBit((D >> b) & 0b1);
}
 
static void turn(long deviceId, int channel, boolean on)
{
  int count = 5;
  while (count--)
    sendPacket(deviceId, (on ? 0b0000 : 0b1000) | (channel & 0b0111));
}
 
static void train(long deviceId, int channel)
{
  int count = 100;
  while (count--)
      sendPacket(deviceId, 0b1000 | (channel & 0b0111));
}
 
static void led(byte on) {
    pinMode(9, OUTPUT);
    digitalWrite(9, !on);
}
 
char getc()
{
  while (Serial.available()==0) {}
  return Serial.read();
}
 
int device(char c)
{
  c = toupper(c);
  if (c>='0' && c<='9')
    return (int)(c-48);
  if (c>='A' && c<='F')
    return (int)(c-55);
 
  return -1;
}
 
void setup() {
  Serial.begin(57600);
  rf12_initialize(0, RF12_433MHZ);
  Serial.println("Homy Control");
  Serial.println();
  Serial.println("Syntax :");
  Serial.println(" [0|1|X|Z]<device>");
  Serial.println();
  Serial.println(" where :");
  Serial.println("    0 - turn off");
  Serial.println("    1 - turn on");
  Serial.println("    X - turn all off for device group");
  Serial.println("    Z - hook device");
  Serial.println("    <device> - device code (hex digit)");
  Serial.println();
}
 
void loop() {
 
  Serial.print(">");
 
  char fc = getc();
  int dev;
 
  switch(toupper(fc))
  {
    case 'Z':
        dev = device(getc());
        led(1);
        train(devIds[dev],chanIds[dev]);
        Serial.print("Train ");Serial.print(devIds[dev]);Serial.print(" ");Serial.println(chanIds[dev]);
        led(0);
      break;
 
    case '1':
        dev = device(getc());
        turn(devIds[dev],chanIds[dev],1);
        Serial.print("On ");Serial.print(devIds[dev]);Serial.print(" ");Serial.println(chanIds[dev]);
      break;
 
    case '0':
        dev = device(getc());
        turn(devIds[dev],chanIds[dev],0);
        Serial.print("Off ");Serial.print(devIds[dev]);Serial.print(" ");Serial.println(chanIds[dev]);
      break;
 
    case 'X':
        dev = device(getc());
        turn(devIds[dev],7,1);
        Serial.print("Off ALL ");Serial.println(devIds[dev]);
      break;
 
    default:
        led(1);
        Serial.println("*");
        led(0);
      break;
  }
}