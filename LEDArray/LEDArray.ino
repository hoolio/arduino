/*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.
 
  This example code is in the public domain.
 */
 
// Pin 13 has an LED connected on most Arduino boards.
// give it a name:
int cathodes = 2;
int cathodesIndicator = 9;
int annodes = 5;
int annodesIndicator = 8;

// the setup routine runs once when you press reset:
void setup() {                
  // initialize the digital pin as an output.
  pinMode(cathodes, OUTPUT);  
  pinMode(cathodesIndicator, OUTPUT);    
  pinMode(annodes, OUTPUT);   
  pinMode(annodesIndicator, OUTPUT);   

  //Serial.begin(9600);
}

// the loop routine runs over and over again forever:
void loop() {
  int mydelay=100;
  
  // nothing should happen
  digitalWrite(cathodes, HIGH);
  digitalWrite(cathodesIndicator, HIGH);
  digitalWrite(annodes, LOW);
  digitalWrite(annodesIndicator, LOW);
  //Serial.println("cathodes HIGH, annodes LOW");
  delay(mydelay);       

  digitalWrite(cathodes, LOW);
  digitalWrite(cathodesIndicator, LOW);
  digitalWrite(annodes, HIGH);
  digitalWrite(annodesIndicator, HIGH);
  //Serial.println("cathodes LOW, annodes HIGH");
  delay(mydelay); 
  
  // now sommit should happen
  digitalWrite(cathodes, HIGH);
  digitalWrite(cathodesIndicator, HIGH);
  digitalWrite(annodes, HIGH);
  digitalWrite(annodesIndicator, HIGH);
  //Serial.println("cathodes HIGH, annodes HIGH");
  delay(mydelay); 
}
