// ok, so monitor IR for off/on command and Button for same
// then trigger PC on/off via optocoupler, ie simply C_ShutdownEventLED=HIGH

#include <IRremote.h>

// some global variables for simplicity
int C_RequiredConstantButtonPressDuration = 10; // how long to require the user to hold the IR button for.  1 "count" =~ 100ms
int C_PowerButtonPressDuration = 1000; // how long to hold the PWR_ON signal to the ATX power supply
int C_OverpressProtectionDelayDuration = 3000; // how long to pause to protect against aggressive users

// define the arduino i/o pins etc
//int C_IR_LED = 3; unused
int C_ShutdownEventLED = 7;
int C_IRReceiver = 8;
int C_IRCommsLED = 11;
int C_NafariousButton = 9; 
IRrecv irrecv(C_IRReceiver);
decode_results results;

// define the various signals that come from the MCE remote
#define OFF_CODE1 0xA9EFCC78 // PC on MCE remote
#define OFF_CODE2 0x800F840C // PC on MCE remote
#define OFF_CODE3 0x4B790694 // PC on MCE remote
#define OFF_CODE4 0x800F040C // PC on MCE remote

int V_DepressCount = 1; int V_DepressTime = millis(); int V_LastDepressTime=0;

void setup()
{
  pinMode(C_ShutdownEventLED, OUTPUT);
  pinMode(C_IRCommsLED, OUTPUT);
  pinMode(C_NafariousButton, INPUT);
  
  Serial.begin(9600);
  irrecv.enableIRIn(); // Start the receiver
  
  Serial.println("Waiting for IR remote and/or physical button press...");
}

void loop() {
  //if the physical button is being pressed then simply trigger the shutdown
  char PhysicalButtonState = digitalRead(C_NafariousButton);
  if (PhysicalButtonState == HIGH) {     
    ShutdownOrStartPC();
  }
  
  // if theres something coming from the remote, lets decode it
  if (irrecv.decode(&results)) {   
    digitalWrite(C_IRCommsLED, HIGH);
    
    // if the remote was the MCE PC ON/OFF button:
    if ((results.value == OFF_CODE1) || (results.value == OFF_CODE2) || (results.value == OFF_CODE3) || (results.value == OFF_CODE4)) {
      
      // lets record when this button press happened, so we can compare it later
      V_LastDepressTime = V_DepressTime;
      V_DepressTime = millis();
      
      // if this next press came within ms of the last one assume it's part of some loong button press event
      // next we want to compare the time of this depress against the time of the last one 
      int V_DepressTimeDelta = V_DepressTime - V_LastDepressTime;
      
      if (V_DepressTimeDelta < 200){ // 
        V_DepressCount++;

        Serial.print(results.value, HEX); Serial.print(" = Off ");
        Serial.print(" count ");Serial.print(V_DepressCount);
        Serial.print(" delta ");Serial.println(V_DepressTimeDelta);
        
        // ok so if this long depress event lasts longer than X then actually trigger the DO SOMETHING
        if (V_DepressCount >= C_RequiredConstantButtonPressDuration) { 
          V_DepressCount=1; //so we dont trigger upon the on/off button event
          ShutdownOrStartPC();
        } 
      // else ie V_DepressTimeDelta > 200 they did press the same button as before, but released it
      // so we dont count it as part of a single event.  also we restart the timer, and the counter
      } else {
        Serial.print(results.value, HEX); Serial.print(" = Off  count 1"); 
        Serial.print(" delta ");Serial.println(V_DepressTimeDelta);
        V_DepressCount=1;        
      }
      
    // else if the button wasn't the MCE ON/OFF one
    } else {
       Serial.println(results.value, HEX);
    }
    irrecv.resume(); // Receive the next value
    digitalWrite(C_IRCommsLED, LOW);
  }
}

void ShutdownOrStartPC(){
  Serial.println("OK, trigger(ing) shutdown/startup");
  digitalWrite(C_ShutdownEventLED, HIGH);
  Serial.print(".. pressing power button for "); Serial.print(C_PowerButtonPressDuration); Serial.println(" ms ..");
  delay(C_PowerButtonPressDuration); // how long to press the power button for
  digitalWrite(C_ShutdownEventLED, LOW);
  Serial.print(".. delaying "); Serial.print(C_OverpressProtectionDelayDuration); Serial.println(" ms to protect against overpress ..");
  delay(C_OverpressProtectionDelayDuration);
  Serial.println(".. now listening again ..");
}
