/*
  Button
 
 Turns on and off a light emitting diode(LED) connected to digital  
 pin 13, when pressing a pushbutton attached to pin 2. 
 
 
 The circuit:
 * LED attached from pin 13 to ground 
 * pushbutton attached to pin 2 from +5V
 * 10K resistor attached to pin 2 from ground
 
 * Note: on most Arduinos there is already an LED on the board
 attached to pin 13.
 
 
 created 2005
 by DojoDave <http://www.0j0.org>
 modified 30 Aug 2011
 by Tom Igoe
 
 This example code is in the public domain.
 
 http://www.arduino.cc/en/Tutorial/Button
 */

// constants won't change. They're used here to 
// set pin numbers:
const int FireButtonPin = 17; //A3
const int JoystickUpPin = 16; //A2 
const int JoystickDownPin = 7; //A1
const int JoystickLeftPin = 13;
const int JoystickRightPin = 15;
const int SpeakerPin = 14; // A0 in digital mode

int V_Frequency = 0;
int V_Multiplier = 8;
int V_Counter = 30;

void setup() {
  // initialize the joystick connected pins as an inputs:
  pinMode(FireButtonPin, INPUT);     
  pinMode(JoystickUpPin, INPUT);
  pinMode(JoystickDownPin, INPUT);
  pinMode(JoystickLeftPin, INPUT);
  pinMode(JoystickRightPin, INPUT);

  Serial.begin(9600);
}

void loop(){
  // read the state of the pushbutton value:
  int FireButtonState = digitalRead(FireButtonPin); FireButtonState = map(FireButtonState,0,1,1,0); 
  int JoystickUpState = digitalRead(JoystickUpPin); JoystickUpState = map(JoystickUpState,0,1,1,0);
  int JoystickDownState = digitalRead(JoystickDownPin); JoystickDownState = map(JoystickDownState,0,1,1,0);
  int JoystickLeftState = digitalRead(JoystickLeftPin); JoystickLeftState = map(JoystickLeftState,0,1,1,0);
  int JoystickRightState = digitalRead(JoystickRightPin);  JoystickRightState = map(JoystickRightState,0,1,1,0);

  Serial.print("Fire: "); Serial.print(FireButtonState);
  Serial.print(" Up: "); Serial.print(JoystickUpState);
  Serial.print(" Down: "); Serial.print(JoystickDownState);
  Serial.print(" Left: "); Serial.print(JoystickLeftState);
  Serial.print(" Right: "); Serial.println(JoystickRightState); 
}
