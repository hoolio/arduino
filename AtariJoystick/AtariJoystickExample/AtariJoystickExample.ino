/*
 Interfacing Atari Joystick to Arduino:
 
 Returns output of Atari 2600 9 pin joystick to Arduino serial console.
 
       +---------> Right
       | +-------> Left
       | | +-----> Down
       | | | +---> Up 
       | | | | 
   _____________
 5 \ x o o o o / 1
    \ x o x o / 
   9 `~~~~~~~' 6
        |   |
        |   +----> Button
        +--------> Ground
 
 pinout via http://www.epanorama.net/documents/joystick/ataristick.html
 
 The circuit:
 * 5v through 10k resistor to each of the 5 joystick output pins
 * Arduino ground to Joystick ground
 * 5 data wires from joystick inputs to the arduino pins in the format:
    5V -> 10K resistor > Data to Arduino > Joystick Input
    
 created 2012 by Julius Roberts <http://hooliowobbits.blogspot.com.au/>
 
 This example code is in the public domain.
 */

// constants won't change. They're used here to set pin numbers:
const int FireButtonPin = 2;     
const int JoystickUpPin = 3;
const int JoystickDownPin = 4;
const int JoystickLeftPin = 5;
const int JoystickRightPin = 6;

void setup() {
  // initialize the Joystick pins as inputs:
  pinMode(FireButtonPin, INPUT);     
  pinMode(JoystickUpPin, INPUT);
  pinMode(JoystickDownPin, INPUT);
  pinMode(JoystickLeftPin, INPUT);
  pinMode(JoystickRightPin, INPUT);

  Serial.begin(9600);
}

void loop(){
  // read the state of the pushbutton value.  Because we're using a
  // pullup resistor, the value for each of the variables will normally 
  // be 1, and 0 when pressed.  This is the opposite of normal 
  // programming logic where 1 = true.  So we invert the variable using
  //  the map function  
  int FireButtonState = digitalRead(FireButtonPin); 
  FireButtonState = map(FireButtonState,0,1,1,0); 
  int JoystickUpState = digitalRead(JoystickUpPin); 
  JoystickUpState = map(JoystickUpState,0,1,1,0);
  int JoystickDownState = digitalRead(JoystickDownPin); 
  JoystickDownState = map(JoystickDownState,0,1,1,0);
  int JoystickLeftState = digitalRead(JoystickLeftPin); 
  JoystickLeftState = map(JoystickLeftState,0,1,1,0);
  int JoystickRightState = digitalRead(JoystickRightPin);  
  JoystickRightState = map(JoystickRightState,0,1,1,0);

  // output the setting to the serial console
  Serial.print("Fire: "); Serial.print(FireButtonState);
  Serial.print(" Up: "); Serial.print(JoystickUpState);
  Serial.print(" Down: "); Serial.print(JoystickDownState);
  Serial.print(" Left: "); Serial.print(JoystickLeftState);
  Serial.print(" Right: "); Serial.println(JoystickRightState);
}
