 /*
  Initially attaches to my test shield and does simple i/o to sensors/lights etc.
  then will talk to the 4d screen to do some magic foo..  
 */
 
// pin assignments
// lights
const int C_RedLED = 7; 
const int C_OrangeLED = 8; 
const int C_GreenLED = 9; 
// sensors
const int C_PotPin = A1;
const int C_RedThermisterPin = A5;
const int C_LDRPin = A2;

void setup() {
  //lights
  pinMode(C_RedLED, OUTPUT);      
  pinMode(C_OrangeLED, OUTPUT);
  pinMode(C_GreenLED, OUTPUT);
  // sensors
  pinMode(C_RedThermisterPin, OUTPUT);
  pinMode(C_PotPin, OUTPUT);
  
  // fancy cycling LED test
  for (int i=0; i<5;i++){
     digitalWrite(C_GreenLED, HIGH);  delay(50); digitalWrite(C_GreenLED, LOW); 
     digitalWrite(C_OrangeLED, HIGH); delay(50); digitalWrite(C_OrangeLED, LOW);
     digitalWrite(C_RedLED, HIGH);    delay(50); digitalWrite(C_RedLED, LOW);
  }
  
  // start serial port:
  Serial.begin(9600);
} 

void loop() {
	// read analog sensor(s):
	int V_Temp = Thermistor(analogRead(C_RedThermisterPin));
 	int V_LightLevel = analogRead(C_LDRPin); 
	int V_Pot = analogRead(C_PotPin); 

	// output to serial
	Serial.print(" V_Temp: "); Serial.print(V_Temp);	
	Serial.print(" V_LightLevel: "); Serial.print(V_LightLevel);	
	Serial.print(" V_Pot: "); Serial.print(V_Pot);	
	Serial.println("");	
		
	//blink a light and wait a sec
	digitalWrite(C_GreenLED, HIGH);
	delay(200);
	digitalWrite(C_GreenLED, LOW);
	delay(1000);
}    

float Thermistor(int RawADC) {
  //float vcc = 4.91;         // only used for display purposes, if used set to the measured Vcc.
  float pad = 9950;         // balance/pad resistor value, set this to the measured resistance of your pad resistor
  float thermr = 10000;      // thermistor nominal resistance
  long Resistance;  
  float Temp;  // Dual-Purpose variable to save space.

  Resistance=((1024 * pad / RawADC) - pad); 
  Temp = log(Resistance); // Saving the Log(resistance) so not to calculate  it 4 times later
  Temp = 1 / (0.001129148 + (0.000234125 * Temp) + (0.0000000876741 * Temp * Temp * Temp));
  Temp = Temp - 273.15;  // Convert Kelvin to Celsius                      

  Temp = Temp - 20; // Hoolios unforgivably bad conversion factor when using the 4.k thermistor
  //Temp = Temp - 3; // Hoolios unforgivably bad conversion factor when using the 10k thermistor.

  return Temp;   // Return the Temperature
}

    

