/*
  DHCP-based IP printer
 
 This sketch uses the DHCP extensions to the Ethernet library
 to get an IP address via DHCP and print the address obtained.
 using an Arduino Wiznet Ethernet shield. 
 
 Circuit: * Ethernet shield attached to pins 10, 11, 12, 13
 
 created 12 April 2011 By Tom Igoe
*/

#include <SPI.h>
#include <Ethernet.h>

// Enter a MAC address for your controller below.
// Newer Ethernet shields have a MAC address printed on a sticker on the shield
byte mac[] = { 0x00, 0xAA, 0xBB, 0xCC, 0xDE, 0x02 };
byte ip[]	= { 10, 1, 1, 222 };
//byte gateway[] = { 10, 1, 1, 2 };
//byte subnet[]  = { 255, 255, 255, 0 };
//Server server(80);

void setup() {  
  // Initialize the Ethernet client library
  //delay(5000); //to overcome some bug with the ethernet chipset
  //EthernetClient client;
   
  // start the serial library:
  Serial.begin(9600);

  // test serial output

  // start the Ethernet connection:
  Serial.println("Starting the Ethernet connection");
  if (Ethernet.begin(ip) == 0) {
    Serial.println("..failed to configure Ethernet");
    delay(100000); // no point in carrying on
  } 
  else {
    Serial.print("..my ip address: ");
    for (byte thisByte = 0; thisByte < 4; thisByte++) {
      // print the value of each byte of the IP address:
      Serial.print(Ethernet.localIP()[thisByte], DEC);
      Serial.print("."); 
    }
    Serial.println();
  }
}

void loop() {

}


